<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrview
 *
 * @author scriptmedia
 */
if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');

class show extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('basemodel');
    }

    function index($xAwal = 0, $xSearch = '') {
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
//        $data['maincontent'] = $this->maincontent(215);
//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header();
//        $data['slider'] = $this->slider();
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();
        $data['table']= $this->setDetailFormmember(0);
        $buttoninfo = ' <button onclick="location.href=\''. base_url().'index.php/showinfo\';" class="col btn col-lg-2 informasi pt-3 mb-3 ml-3">
                <div style="float:left;margin-right:10px;"><img src="'. base_url() .'resource/scriptmedia/images/info.png"></div>
                <h5>Informasi<br>Penting</h5>
            </button>';
        //$data['buttoninfo']= $buttoninfo;
        $data['runningtext']=  $this->getlistrunningtext(0,'');
        //$data['tableinfo']=  $this->getlistinfopenting(0);
        $this->load->view('scriptmedia/index', $data);
    }
function setDetailFormmember($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrmember/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelevent');
        $this->load->model('modeljenisorganisasi');
        $event = $this->modelevent->getDetaileventisaktif();
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';


        $xBufResult .= setForm2('nama', 'Nama', form_input(getArrayObj('ednama', '', ''), '', 'class="col-md-6 form-control d-inline" placeholder="nama" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm2('email', 'Email', form_input(getArrayObj('edemail', '', ''), '', 'class="col-md-6 form-control d-inline"  placeholder="email" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm2('telepon', 'Telepon', form_input(getArrayObj('edtelepon', '', ''), '', 'class="col-md-6 form-control d-inline"  placeholder="telepon" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm2('alamat', 'Alamat', form_input(getArrayObj('edalamat', '', ''), '', 'class="col-md-6 form-control d-inline"  placeholder="alamat" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm2('idevent', 'Event', form_dropdown('edidevent', $event, '', 'class="col-md-4 form-control d-inline" placeholder="idevent" disabled')) . '<div class="spacer"></div>';
        $xBufResult .= setForm2('idjenisorganisasi', 'Organisasi', form_dropdown('edidjenisorganisasi', $this->modeljenisorganisasi->getArrayListjenisorganisasi(), '', 'class="form-control col-md-4 d-inline"  placeholder="idjenisorganisasi" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanmember();"') . '<div class="spacer"></div><div id="tabledatamember">' . $this->getlistmember(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistmember($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $no=1;
        $xbufResult1 = tbaddrow(tbaddcellhead('No', '', 'data-field="idx" data-sortable="true" width=10%') .
//                tbaddcellhead('idjenisorganisasi', '', 'data-field="idjenisorganisasi" data-sortable="true" width=10%') .
                tbaddcellhead('Nama', '', 'data-field="nama" data-sortable="true" width=10%') .
                tbaddcellhead('Email', '', 'data-field="email" data-sortable="true" width=10%') .
                tbaddcellhead('Telepon', '', 'data-field="telepon" data-sortable="true" width=10%') .
                tbaddcellhead('Alamat', '', 'data-field="alamat" data-sortable="true" width=10%') .
                tbaddcellhead('Event', '', 'data-field="idevent" data-sortable="true" width=10%'), '', TRUE);
        $this->load->model('modelmember');
        $this->load->model('modelevent');
        $xQuery = $this->modelmember->getListmember($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $event = $this->modelevent->getDetailevent($row->idevent);
            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditmember(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapusmember(\'' . $row->idx . '\');" style="border:none;">';
            $xbufResult .= tbaddrow(tbaddcell($no++) .
//                    tbaddcell($row->idjenisorganisasi) .
                    tbaddcell($row->nama) .
                    tbaddcell($row->email) .
                    tbaddcell($row->telepon) .
                    tbaddcell(@$row->alamat) .
                    tbaddcell(@$event->Judul));
        }
        $xInput = form_input(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchmember(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchmember(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchmember(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">'. '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' . '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"></div></div>';
    }
       function simpanmember() {
        $this->load->helper('json');
        $xidx = '0';
       
        $xidjenisorganisasi = $_POST['edidjenisorganisasi'];
        $xnama = $_POST['ednama'];
        $xemail = $_POST['edemail'];
        $xtelepon = $_POST['edtelepon'];
        $xalamat = $_POST['edalamat'];
        $xidevent = $_POST['edidevent'];

        $this->load->model('modelmember');
        $idpegawai = $this->session->userdata('idpegawai');
        $xStr = $this->modelmember->setInsertmember($xidx, $xidjenisorganisasi, $xnama, $xemail, $xtelepon, $xalamat, $xidevent);
        echo json_encode(null);
    }
    function searchmember() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatamember'] = $this->getlistmember($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }
    
    private function getlistrunningtext($xAwal, $xSearch='') {
       
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelrunningtext');
        $xQuery = $this->modelrunningtext->getListrunningtext($xAwal, $xLimit, $xSearch);
//        $xbufResult = '<thead>' . '' . '</thead>';
//        $xbufResult = '<tbody>';
        $xbufResult = '';
        $no = $xAwal+1;
        $xacara='';
        foreach ($xQuery->result() as $row) {
              $xbufResult .= ' <i class="fa fa-calendar" aria-hidden="true"></i> '.$row->runningtext .'';
        }

         return '<marquee>' . $xbufResult . '</marquee>';
   }
public function searchinfopenting() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatainfopenting'] = $this->getlistinfopenting($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }
public function searchisdinas() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['marquee'] = $this->getlistisdinas($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

}

?>

function dosearchanggota(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctranggota/searchanggota/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledataanggota").html(json.tabledataanggota);
                $("#edSearch").val(xSearch);
                $("#edHalaman").html(json.halaman);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}


function doeditanggota(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctranggota/editrecanggota/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#ednoktp").val(json.noktp);
                $("#ednamalengkap").val(json.namalengkap);
                $("#edjeniskelamin").val(json.jeniskelamin);
                $("#edidpendidikan").val(json.idpendidikan);
                $("#edidpekerjaan").val(json.idpekerjaan);
                $("#edidjabatan").val(json.idjabatan);
                $("#edalamat").val(json.alamat);
                $("#edidkelurahan").val(json.idkelurahan);
                $("#edidkecamatan").val(json.idkecamatan);
                $("#edidkabupaten").val(json.idkabupaten);
                $("#edidprovinsi").val(json.idprovinsi);
                $("#ednotelpon").val(json.notelpon);
                $("#edusername").val(json.username);
                $("#edpassword").val(json.password);
                $("#ediddapil").val(json.iddapil);
                $("#edidtps").val(json.idtps);
                $("#edidpartai").val(json.idpartai);
                $("#edidtahun").val(json.idtahun);
                $("#edisaktif").val(json.isaktif);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearanggota() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#ednoktp").val("");
        $("#ednamalengkap").val("");
        $("#edjeniskelamin").val("");
        $("#edidpendidikan").val("");
        $("#edidpekerjaan").val("");
        $("#edidjabatan").val("");
        $("#edalamat").val("");
        $("#edidkelurahan").val("");
        $("#edidkecamatan").val("");
        $("#edidkabupaten").val("");
        $("#edidprovinsi").val("");
        $("#ednotelpon").val("");
        $("#edusername").val("");
        $("#edpassword").val("");
        $("#ediddapil").val("");
        $("#edidtps").val("");
        $("#edidpartai").val("");
        $("#edidtahun").val("");
        $("#edisaktif").val("");

    });
}

function dosimpananggota() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctranggota/simpananggota/",
            data: "edidx=" + $("#edidx").val() + "&ednoktp=" + $("#ednoktp").val() + "&ednamalengkap=" + $("#ednamalengkap").val() + "&edjeniskelamin=" + $("#edjeniskelamin").val() + "&edidpendidikan=" + $("#edidpendidikan").val() + "&edidpekerjaan=" + $("#edidpekerjaan").val() + "&edidjabatan=" + $("#edidjabatan").val() + "&edalamat=" + $("#edalamat").val() + "&edidkelurahan=" + $("#edidkelurahan").val() + "&edidkecamatan=" + $("#edidkecamatan").val() + "&edidkabupaten=" + $("#edidkabupaten").val() + "&edidprovinsi=" + $("#edidprovinsi").val() + "&ednotelpon=" + $("#ednotelpon").val() + "&edusername=" + $("#edusername").val() + "&edpassword=" + $("#edpassword").val() + "&ediddapil=" + $("#ediddapil").val() + "&edidtps=" + $("#edidtps").val() + "&edidpartai=" + $("#edidpartai").val() + "&edidtahun=" + $("#edidtahun").val() + "&edisaktif=" + $("#edisaktif").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function (msg) {
                toastr.info('Loding.....', '', {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "onclick": null
                });
            },
            success: function (msg) {
                doClearanggota();
                dosearchanggota('-99');
                toastr.clear();
                toastr.success('Data berhasil disimpan');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapusanggota(edidx) {
    if (confirm("Anda yakin Akan menghapus data ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctranggota/deletetableanggota/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearanggota();
                    dosearchanggota('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchanggota(0);
function queryParams() {
    return {
        type: 'owner',
        sort: 'idx',
        direction: 'desc',
        per_page: 1000,
        page: 1
    };
}

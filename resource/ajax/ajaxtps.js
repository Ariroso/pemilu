function dosearchtps(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtps/searchtps/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatatps").html(json.tabledatatps);
                $("#edSearch").val(xSearch);
                $("#edHalaman").html(json.halaman);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}


function doedittps(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtps/editrectps/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                kabupaten(json.idkabupaten);
                kecamatan(json.idkecamatan);

                $("#edidx").val(json.idx);
                $("#edtps").val(json.tps);
                $("#edurut").val(json.urut);
                $("#edidketua").val(json.idketua);
                $("#edketerangan").val(json.keterangan);
                $("#edidkabupaten").val(json.idkabupaten);
                $("#edidprovinsi").val(json.idprovinsi);
                $("#ediddapil").val(json.iddapil);
                $("#edidtahun").val(json.idtahun);
                selectafteredit(json.idkecamatan, json.idkelurahan);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}
function selectafteredit(xkecamatan, xkelurahan) {
    $(document).ready(function () {
        $("#edidkecamatan").val(xkecamatan);
        $("#edidkelurahan").val(xkelurahan);
    });
}
function doCleartps() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edtps").val("");
        $("#edurut").val("");
        $("#edidketua").val("");
        $("#edketerangan").val("");
        $("#edidkecamatan").val("");
        $("#edidkelurahan").val("");
        $("#edidkabupaten").val("");
        $("#edidprovinsi").val("");
        $("#ediddapil").val("");
        $("#edidtahun").val("");

    });
}

function dosimpantps() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrtps/simpantps/",
            data: "edidx=" + $("#edidx").val() + "&edtps=" + $("#edtps").val() + "&edurut=" + $("#edurut").val() + "&edidketua=" + $("#edidketua").val() + "&edketerangan=" + $("#edketerangan").val() + "&edidkecamatan=" + $("#edidkecamatan").val() + "&edidkelurahan=" + $("#edidkelurahan").val() + "&edidkabupaten=" + $("#edidkabupaten").val() + "&edidprovinsi=" + $("#edidprovinsi").val() + "&ediddapil=" + $("#ediddapil").val() + "&edidtahun=" + $("#edidtahun").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function (msg) {
                toastr.info('Loding.....', '', {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "onclick": null
                });
            },
            success: function (msg) {
                doCleartps();
                dosearchtps('-99');
                toastr.clear();
                toastr.success('Data berhasil disimpan');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapustps(edidx) {
    if (confirm("Anda yakin Akan menghapus data ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrtps/deletetabletps/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doCleartps();
                    dosearchtps('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchtps(0);
function queryParams() {
    return {
        type: 'owner',
        sort: 'idx',
        direction: 'desc',
        per_page: 1000,
        page: 1
    };
}
  
$(document).ready(function () {
    $("#edurl").myupload();

});

function dosearchsetting(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsetting/searchsetting/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatasetting").html(json.tabledatasetting);
                $("#edSearch").val(xSearch);
                $("#edHalaman").html(json.halaman);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}


function doeditsetting(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsetting/editrecsetting/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edNama").val(json.Nama);
                $("#edAlamat").val(json.Alamat);
                $("#edemail").val(json.email);
                $("#edportemail").val(json.portemail);
                $("#edTelpon").val(json.Telpon);
                $("#edWhatsapp").val(json.Whatsapp);
                $("#edidkecamatan").val(json.idkecamatan);
                $("#edBio").val(json.Bio);
                $("#edlogo").val(json.logo);
                $("#edfb").val(json.fb);
                $("#edig").val(json.ig);
                $("#edtw").val(json.tw);
                $("#edG").val(json.G);
                $("#edyoutube").val(json.youtube);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearsetting() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edNama").val("");
        $("#edAlamat").val("");
        $("#edemail").val("");
        $("#edportemail").val("");
        $("#edTelpon").val("");
        $("#edWhatsapp").val("");
        $("#edidkecamatan").val("");
        $("#edBio").val("");
        $("#edlogo").val("");
        $("#edfb").val("");
        $("#edig").val("");
        $("#edtw").val("");
        $("#edG").val("");
        $("#edyoutube").val("");

    });
}

function dosimpansetting() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsetting/simpansetting/",
            data: "edidx=" + $("#edidx").val() + "&edNama=" + $("#edNama").val() + "&edAlamat=" + $("#edAlamat").val() + "&edemail=" + $("#edemail").val() + "&edportemail=" + $("#edportemail").val() + "&edTelpon=" + $("#edTelpon").val() + "&edWhatsapp=" + $("#edWhatsapp").val() + "&edidkecamatan=" + $("#edidkecamatan").val() + "&edBio=" + $("#edBio").val() + "&edlogo=" + $("#edlogo").val() + "&edfb=" + $("#edfb").val() + "&edig=" + $("#edig").val() + "&edtw=" + $("#edtw").val() + "&edG=" + $("#edG").val() + "&edyoutube=" + $("#edyoutube").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function (msg) {
                toastr.info('Loding.....', '', {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "onclick": null
                });
            },
            success: function (msg) {
                doClearsetting();
                dosearchsetting('-99');
                toastr.clear();
                toastr.success('Data berhasil disimpan');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapussetting(edidx) {
    if (confirm("Anda yakin Akan menghapus data ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrsetting/deletetablesetting/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearsetting();
                    dosearchsetting('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchsetting(0);
function queryParams() {
    return {
        type: 'owner',
        sort: 'idx',
        direction: 'desc',
        per_page: 1000,
        page: 1
    };
}
  
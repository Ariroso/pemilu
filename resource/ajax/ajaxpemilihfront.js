function dosearchpemilih(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrpemilihfront/searchpemilih/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatapemilih").html(json.tabledatapemilih);
                $("#edSearch").val(xSearch);
                $("#edHalaman").html(json.halaman);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}


function doeditpemilih(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrpemilihfront/editrecpemilih/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#ednokk").val(json.nokk);
                $("#ednik").val(json.nik);
                $("#ednama").val(json.nama);
                $("#edtempatlahir").val(json.tempatlahir);
                $("#edtgllahir").val(json.tgllahir);
                $("#edperkawinan").val(json.perkawinan);
                $("#edjeniskelamin").val(json.jeniskelamin);
                $("#edidpendidikan").val(json.idpendidikan);
                $("#edidpekerjaan").val(json.idpekerjaan);
                $("#edidjabatan").val(json.idjabatan);
                $("#edalamat").val(json.alamat);
                $("#edrt").val(json.rt);
                $("#edrw").val(json.rw);
                $("#eddisabilitas").val(json.disabilitas);
                $("#edketerangan").val(json.keterangan);
                $("#ediddapil").val(json.iddapil);
                $("#edidtps").val(json.idtps);
                $("#edidtahun").val(json.idtahun);
                $("#edidanggota").val(json.idanggota);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearpemilih() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#ednokk").val("");
        $("#ednik").val("");
        $("#ednama").val("");
        $("#edtempatlahir").val("");
        $("#edtgllahir").val("");
        $("#edperkawinan").val("");
        $("#edjeniskelamin").val("");
        $("#edidpendidikan").val("");
        $("#edidpekerjaan").val("");
        $("#edidjabatan").val("");
        $("#edalamat").val("");
        $("#edrt").val("");
        $("#edrw").val("");
        $("#eddisabilitas").val("");
        $("#edketerangan").val("");
        $("#ediddapil").val("");
        $("#edidtps").val(0);
//        $("#edidtps").att("value",0);
        $("#edidtahun").val("");
        $("#edidanggota").val("");

    });
}

function dosimpanpemilihfront() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrpemilihfront/simpanpemilihfront/",
            data: "edidx=" + $("#edidx").val() + "&ednokk=" + $("#ednokk").val() + "&ednik=" + $("#ednik").val() + "&ednama=" + $("#ednama").val() + "&edtempatlahir=" + $("#edtempatlahir").val() + "&edtgllahir=" + $("#edtgllahir").val() + "&edperkawinan=" + $("#edperkawinan").val() + "&edjeniskelamin=" + $("#edjeniskelamin").val() + "&edidpendidikan=" + $("#edidpendidikan").val() + "&edidpekerjaan=" + $("#edidpekerjaan").val() + "&edidjabatan=" + $("#edidjabatan").val() + "&edalamat=" + $("#edalamat").val() + "&edrt=" + $("#edrt").val() + "&edrw=" + $("#edrw").val() + "&eddisabilitas=" + $("#eddisabilitas").val() + "&edketerangan=" + $("#edketerangan").val() + "&ediddapil=" + $("#ediddapil").val() + "&edidtps=" + $("#edidtps").val() + "&edidtahun=" + $("#edidtahun").val() + "&edidanggota=" + $("#edidanggota").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function (msg) {
                toastr.info('Loding.....', '', {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "onclick": null
                });
            },
            success: function (msg) {
                doClearpemilih();
//                dosearchpemilih('-99');
                toastr.clear();
                toastr.success('Data berhasil disimpan');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapuspemilih(edidx) {
    if (confirm("Anda yakin Akan menghapus data ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrpemilihfront/deletetablepemilih/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearpemilih();
                    dosearchpemilih('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


//dosearchpemilih(0); 
function queryParams() {
    return {
        type: 'owner',
        sort: 'idx',
        direction: 'desc',
        per_page: 1000,
        page: 1
    };
}
 
function strpad(val) {
    return (!isNaN(val) && val.toString().length == 1) ? "0" + val : val;
}

function settanggal() {
    $(document).ready(function () {


        var currentTimeAndDate = new Date();
        var Date30 = new Date();
        var date = new Date();
        Date30.setDate(Date30.getDate() - 30);



        var dd = date.getDate();
        var mm = date.getMonth();
        var yy = date.getYear();

        var hh = date.getHours();
        var mnt = date.getMinutes();

        var dd30 = Date30.getDate();
        var mm30 = Date30.getMonth();
        var yy30 = Date30.getYear();

        yy = (yy < 1000) ? yy + 1900 : yy;
        yy30 = (yy30 < 1000) ? yy30 + 1900 : yy30;


        $(document).on("focus", ".tanggal", function () {
            $(".tanggal").datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1920:2020',
                dateFormat: 'dd-mm-yy'
            });
        });
        if($(".tanggal").val() == ''){
        $(".tanggal").val(strpad(dd) + "-" + strpad(mm + 1) + "-" + yy);
        }

    });
}
settanggal();

function Tpsbydapil() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrpemilihfront/tpsbydapil/",
            data: "ediddapil=" + $('#ediddapil').val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tps").html(json.combotps);
//                $("#kota").html(json.combokota);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Koneksi internet terputus" + xmlHttpRequest.responseText);
            }
        });
    });
}

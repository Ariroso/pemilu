function dosearchimgslide(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimgslide/searchimgslide/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledataimgslide").html(json.tabledataimgslide);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}

function doeditimgslide(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimgslide/editrecimgslide/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                //$("#edurl").val(json.url);
                $("#edurl").val(json.url).trigger('change');
                $("#edketerangan").val(json.keterangan);
                $("#edlink").val(json.link);
//                $("#edtglinsert").val(json.tglinsert);
//                $("#edtglupdate").val(json.tglupdate);
//                $("#edidpegawai").val(json.idpegawai);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearimgslide() {
    $(document).ready(function () {
        $("#edidx").val("0");
//        $("#edurl").val("");
        $("#edketerangan").val("");
        $("#edurl").val("").trigger('change');
        $("#edlink").val("");
//        $("#edtglinsert").val("");
//        $("#edtglupdate").val("");
//        $("#edidpegawai").val("");
    });
}

function dosimpanimgslide() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrimgslide/simpanimgslide/",
            data: "edidx=" + $("#edidx").val() + "&edurl=" + $("#edurl").val() + "&edketerangan=" + $("#edketerangan").val() + "&edtglinsert=" + $("#edtglinsert").val() + "&edtglupdate=" + $("#edtglupdate").val() + "&edidpegawai=" + $("#edidpegawai").val()+ "&edlink=" + $("#edlink").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (msg) {
                doClearimgslide();
                dosearchimgslide('-99');
                alert("Data Berhasil Disimpan.... ");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapusimgslide(edidx, edurl) {
    if (confirm("Anda yakin Akan menghapus data " + edurl + "?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrimgslide/deletetableimgslide/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearimgslide();
                    dosearchimgslide('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


dosearchimgslide(0);

       $(document).ready(function () {
                $("#edurl").myupload();
               
            });



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Base url :  src="http://localhost/tebu-digital/resource/js/jquery.js"
 * site URL : http://localhost/tebu-digital/index.php/voxus/setView
 */


function dosearch(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }

    if (typeof (xSearch) === "undefined") {
        xSearch = "";
    }

    $(document).ready(function () {
        $.ajax({
            //url: "' . site_url('admin/search/') . '",
            url: getBaseURL() + "index.php/webadmindo/search/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch + "&xIdMenu=" + $("#edidmenu").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatatranslete").html(json.tabledata);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " error on search ";
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("This Is " + errorMsg);
            }
        });
    });
}

function doedit(edidtranslete) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/webadmindo/editrec/",
            data: "edidtranslete=" + edidtranslete,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidtranslete").val(json.idtranslete);
                $("#edUrut").val(json.urut);
                $("#edJudul").val(json.judul);
                tinyMCE.activeEditor.setContent(json.isi);
                //alert(json.gambar1);
                $('#previewg1').attr({
                    src: getBaseURL() + "resource/uploaded/project/" + json.gambar1,
                    alt: ""
                });
                //var a_gal = $(\'#preview\').attr({
                $('#preview1').attr({
                    href: getBaseURL() + "resource/uploaded/project/" + json.gambar1,
                    class: "thickbox",
                    alt: "",
                    title: ""
                });

                //tinyMCE.get(\'text\').getContent
                //     $("#edidbahasa").val(json.idbahasa);
                //     $("#edidfield").val(json.idfield);
                //     $("#edidmenu").val(json.idmenu);
                //    $("#edidkomponen").val(json.idkomponen);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseTex);
            }
        });
    });
}

function getUrutTranslete() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/webadmindo/getUrutTranslete/",
            data: "edidmenu=" + $("#edidmenu").val() + "&edidkomponen=" + $("#edidkomponen").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edUrut").val(json.urut);

                tinyMCE.activeEditor.setContent(json.isi);

                //tinyMCE.get(\'text\').getContent
                //     $("#edidbahasa").val(json.idbahasa);
                //     $("#edidfield").val(json.idfield);
                //     $("#edidmenu").val(json.idmenu);
                //    $("#edidkomponen").val(json.idkomponen);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = "error ";
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga" + errorMsg);
            }
        });
    });
}

function doClear() {

    $(document).ready(function () {

        $("#edidtranslete").val("0");
        $("#edJudul").val("");
        $("#edisi").val("");
        tinyMCE.activeEditor.setContent("");
        getUrutTranslete();
        $('#previewg1').attr({
            src: getBaseURL() + "resource/uploaded/white.png",
            alt: ""
        });
        //var a_gal = $(\'#preview\').attr({
        $('#preview1').attr({
            src: getBaseURL() + "resource/uploaded/white.png",
            class: "thickbox",
            alt: "",
            title: ""
        });

        // $("#edidbahasa").val("");
        // $("#edidfield").val("");
        // $("#edidmenu").val("");
        // $("#edidkomponen").val("");
    });
}

function dosimpanimage(idx) {
    $(document).ready(function () {
        //alert("edgambar="+$("#edgambar"+idx).val()+"&edidmenu="+$("#edidmenu").val()+"&idxgambar="+idx);
        $.ajax({
            url: getBaseURL() + "index.php/webadmindo/simpanimage/",
            data: "edgambar=" + $("#edgambar" + idx).val() + "&edidmenu=" + $("#edidmenu").val() + "&idxgambar=" + idx + "&edidtranslete=" + $("#edidtranslete").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                //   doClear();
                // dosearch('-99');
                // tinyMCE.activeEditor.setContent(json.edisi);
                //  alert(tinyMCE.activeEditor.getContent());

                //alert("Image Di simpan");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " dosimpanimage";
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga " + errorMsg);
            }
        });
    });
}


function dosimpan() {
    $(document).ready(function () {

        $.ajax({
            url: getBaseURL() + "index.php/webadmindo/simpan/",
            data: "edidtranslete=" + $("#edidtranslete").val() +
                    "&edisi=" + encodeURIComponent(tinyMCE.activeEditor.getContent()) +
                    "&edJudul=" + $("#edJudul").val() + "&edUrut=" + $("#edUrut").val() +
                    "&edidmenu=" + $("#edidmenu").val() + "&edidkomponen=" + $("#edidkomponen").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                //   doClear();
                dosearch('-99');
                // tinyMCE.activeEditor.setContent(json.edisi);
                //  alert(tinyMCE.activeEditor.getContent());

                alert("Data Sudah Di simpan");
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " ";
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga " + errorMsg);
            }
        });
    });
}


function dohapus(edidtranslete) {
    if (confirm("Anda yakin Akan menghapus data ini?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/webadmindo/deletetable/",
                data: "edidtranslete=" + edidtranslete,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClear();
                    dosearch('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    start = xmlHttpRequest.responseText.search("<title>") + 7;
                    end = xmlHttpRequest.responseText.search("</title>");
                    errorMsg = " error" + xmlHttpRequest.responseText;
                    if (start > 0 && end > 0)
                        alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                    else
                        alert("Error juga " + errorMsg);
                }
            });
        });
    }
}

function dologin() {
    $(document).ready(function () {
        // alert(getBaseURL());
        $.ajax({
            url: getBaseURL() + "index.php/webadmindo/dologin/",
            data: "edUser=" + $("#edUser").val() + "&edPassword=" + $("#edPassword").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {

                if (json.data) {
                    document.location = json.location;
                } else
                {
                    alert("Login Anda Salah Silahkan di ulangi");
                }
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                start = xmlHttpRequest.responseText.search("<title>") + 7;
                end = xmlHttpRequest.responseText.search("</title>");
                errorMsg = " " + xmlHttpRequest.responseText;
                if (start > 0 && end > 0)
                    alert("Rangerti " + errorMsg + "  [" + xmlHttpRequest.responseText.substring(start, end) + "]");
                else
                    alert("Error juga " + errorMsg);
            }
        });
    });
}

function dogetpropinsi(idpropinsi) {
    $(document).ready(function () {
        document.location = getBaseURL() + "index.php/ctrlapperkabupaten/index/" + idpropinsi;
    });
}
function provinsi() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrkabupaten/kabupatenbyprovinsi/",
            data: "edidprovinsi=" + $("#edidprovinsi").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            async: false,
            success: function (json) {
                $("#kabupaten").html("");
                $("#kabupaten").html(json.combokabupaten).trigger('change');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });

}

function kabupaten(xidkabupaten='') {
    if(xidkabupaten===''){
        xidkabupaten = $('#edidkabupaten').val();
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrkecamatan/kecamatanbykabupaten/",
            data: "edidkabupaten=" + xidkabupaten,
            cache: false,
            dataType: 'json',
            type: 'POST',
            async: false,
            success: function (json) {
                $("#kecamatan").html("");
                $("#kecamatan").html(json.combokecamatan);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });

}
function kecamatan(xidkecmatan='') {
     if(xidkecmatan===''){
        xidkecmatan = $('#edidkecamatan').val();
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrkelurahan/kelurahanbykecamatan/",
            data: "edidkecamatan=" + xidkecmatan,
            cache: false,
            dataType: 'json',
            type: 'POST',
            async: false,
            success: function (json) {
                $("#kelurahan").html("");
                $("#kelurahan").html(json.combokelurahan);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });

}
function pad5(number) {
    return (number < 10000 ? '0000' : '') + number;

}

function getvaluemncheck() {
    var xVal = '';
    $('#idjurusan').each(function () {
        $('[name=edidjurusan]').each(function (index) {
            xVal += "&" + $(this).attr('id') + "=" + $(this).val();

        });

    });
    return xVal;
}

function strpad(val) {
    return (!isNaN(val) && val.toString().length == 1) ? "0" + val : val;
}

function settanggal() {

    $(document).ready(function () {


        var currentTimeAndDate = new Date();
        var Date30 = new Date();
        var date = new Date();
        Date30.setDate(Date30.getDate() - 30);



        var dd = date.getDate();
        var mm = date.getMonth();
        var yy = date.getYear();

        var hh = date.getHours();
        var mnt = date.getMinutes();

        var dd30 = Date30.getDate();
        var mm30 = Date30.getMonth();
        var yy30 = Date30.getYear();

        yy = (yy < 1000) ? yy + 1900 : yy;
        yy30 = (yy30 < 1000) ? yy30 + 1900 : yy30;


        $(document).on("focus", ".tanggal", function () {
            $(".tanggal").datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2020',
                dateFormat: 'dd-mm-yy'
            });
        });
        if ($(".tanggal").val() == '') {
            $(".tanggal").val(strpad(dd) + "-" + strpad(mm + 1) + "-" + yy);
        }

    });
}
settanggal();
function autocomplatecalon() {
    $(document).ready(function () {
        $("#ednamacalon").autocomplete({
            multiple: true,
            minLength: 2,
            multipleSeparator: ",",
            source: function (request, response) {
                $.ajax({
                    url: getBaseURL() + "index.php/ctrcalon/getautocomplatecalon/",
//                    term: extractLast( request.term ),
                    data: "ednamacalon=" + $("#ednamacalon").val(),
                    dataType: "json",
                    cache: false,
                    type: "POST",
                    success: function (json) {
                        response($.map(json.data, function (item) {

                            return {
                                label: item.label,
                                value: item.label,
                                idx: item.value
                            }
                        }))
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("Error juga " + xmlHttpRequest.responseText);
                    }
                });
            },
//            search: function () {
//                // custom minLength
//                var term = extractLast(this.value);
//                if (term.length < 2) {
//                    return false;
//                }
//            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
//                var terms = split(this.value);
//                // remove the current input
//                terms.pop();
//                // add the selected item
//                terms.push(ui.item.value);
//                // add placeholder to get the comma-and-space at the end
//                terms.push("");
//                this.value = terms.join(", ");
//                return false;
                $('#edidcalon').val(ui.item.value);
                $('#ednamacalon').val(ui.item.label);
            },
            change: function (event, ui) {
            }
        });
    });
}
autocomplatecalon();
function autocomplateanggota() {
    $(document).ready(function () {
        $("#ednamaanggota").autocomplete({
            multiple: true,
            minLength: 2,
            multipleSeparator: ",",
            source: function (request, response) {
                $.ajax({
                    url: getBaseURL() + "index.php/ctranggota/getautocomplateanggota/",
//                    term: extractLast( request.term ),
                    data: "ednamaanggota=" + $("#ednamaanggota").val(),
                    dataType: "json",
                    cache: false,
                    type: "POST",
                    success: function (json) {
                        response($.map(json.data, function (item) {

                            return {
                                label: item.label,
                                value: item.label,
                                idx: item.value
                            }
                        }))
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        alert("Error juga " + xmlHttpRequest.responseText);
                    }
                });
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                $('#edidanggota').val(ui.item.value);
                $('#edidketua').val(ui.item.value);
                $('#ednamaanggota').val(ui.item.label);
            },
            change: function (event, ui) {
            }
        });
    });
}
autocomplateanggota();
function dosearchsettahunajar(xAwal) {
    xSearch = "";
    try
    {
        if ($("#edSearch").val() != "") {
            xSearch = $("#edSearch").val();
        }
    } catch (err) {
        xSearch = "";
    }
    if (typeof (xSearch) == "undefined") {
        xSearch = "";
    }
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsettahunajar/searchsettahunajar/",
            data: "xAwal=" + xAwal + "&xSearch=" + xSearch,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#tabledatasettahunajar").html(json.tabledatasettahunajar);
                $("#edSearch").val(xSearch);
                $("#edHalaman").html(json.halaman);
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga" + xmlHttpRequest.responseText);
            }
        });
    });
}


function doeditsettahunajar(edidx) {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsettahunajar/editrecsettahunajar/",
            data: "edidx=" + edidx,
            cache: false,
            dataType: 'json',
            type: 'POST',
            success: function (json) {
                $("#edidx").val(json.idx);
                $("#edtahunajar").val(json.tahunajar);
                $("#edgelombang").val(json.gelombang);

            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function doClearsettahunajar() {
    $(document).ready(function () {
        $("#edidx").val("0");
        $("#edtahunajar").val("");
        $("#edgelombang").val("");

    });
}

function dosimpansettahunajar() {
    $(document).ready(function () {
        $.ajax({
            url: getBaseURL() + "index.php/ctrsettahunajar/simpansettahunajar/",
            data: "edidx=" + $("#edidx").val() + "&edtahunajar=" + $("#edtahunajar").val() + "&edgelombang=" + $("#edgelombang").val()+ "&edkategoridaftar=" + $("#edkategoridaftar").val(),
            cache: false,
            dataType: 'json',
            type: 'POST',
            beforeSend: function (msg) {
                toastr.info('Loding.....', '', {
                    "progressBar": true,
                    "positionClass": "toast-top-center",
                    "onclick": null
                });
            },
            success: function (msg) {
//                doClearsettahunajar();
//                dosearchsettahunajar('-99');
                toastr.clear();
                toastr.success('Data berhasil disimpan');
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                alert("Error juga " + xmlHttpRequest.responseText);
            }
        });
    });
}

function dohapussettahunajar(edidx) {
    if (confirm("Anda yakin Akan menghapus data ?"))
    {
        $(document).ready(function () {
            $.ajax({
                url: getBaseURL() + "index.php/ctrsettahunajar/deletetablesettahunajar/",
                data: "edidx=" + edidx,
                cache: false,
                dataType: 'json',
                type: 'POST',
                success: function (json) {
                    doClearsettahunajar();
                    dosearchsettahunajar('-99');
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    alert("Error juga " + xmlHttpRequest.responseText);
                }
            });
        });
    }
}


//dosearchsettahunajar(0);
function queryParams() {
    return {
        type: 'owner',
        sort: 'idx',
        direction: 'desc',
        per_page: 1000,
        page: 1
    };
}
  
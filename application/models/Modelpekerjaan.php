<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : pekerjaan
 * di Buat oleh Diar PHP Generator */

class Modelpekerjaan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListpekerjaan() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",pekerjaan" .
                " FROM pekerjaan   order by idx DESC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->pekerjaan;
        }
        return $xBuffResul;
    }

    function getListpekerjaan($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",pekerjaan" .
                " FROM pekerjaan $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailpekerjaan($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",pekerjaan" .
                " FROM pekerjaan  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexpekerjaan() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",pekerjaan" .
                " FROM pekerjaan order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertpekerjaan($xidx, $xpekerjaan) {
        $xStr = " INSERT INTO pekerjaan( " .
                "idx" .
                ",pekerjaan" .
                ") VALUES('" . $xidx . "','" . $xpekerjaan . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatepekerjaan($xidx, $xpekerjaan) {
        $xStr = " UPDATE pekerjaan SET " .
                "idx='" . $xidx . "'" .
                ",pekerjaan='" . $xpekerjaan . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletepekerjaan($xidx) {
        $xStr = " DELETE FROM pekerjaan WHERE pekerjaan.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletepekerjaan($xidx);
    }

    function setInsertLogDeletepekerjaan($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'pekerjaan',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : pendidikan
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelpendidikan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListpendidikan() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "Kd_jenjang," .
                "Nm_jenjang," .
                "jenjang_ing" .
                " FROM pendidikan   order by Kd_jenjang ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul['0'] = '-';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->Kd_jenjang] = $row->Nm_jenjang;
        }
        return $xBuffResul;
    }

    function getListpendidikan($xAwal, $xLimit, $xSearch='') {
        if (!empty($xSearch)) {
            $xSearch = "Where Nm_jenjang like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "Kd_jenjang," .
                "Nm_jenjang," .
                "jenjang_ing" .
                " FROM pendidikan $xSearch order by Kd_jenjang DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailpendidikan($xKd_jenjang) {
        $xStr = "SELECT " .
                "Kd_jenjang," .
                "Nm_jenjang," .
                "jenjang_ing" .
                " FROM pendidikan  WHERE Kd_jenjang = '" . $xKd_jenjang . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexpendidikan() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "Kd_jenjang," .
                "Nm_jenjang," .
                "jenjang_ing" .
                " FROM pendidikan order by Kd_jenjang DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertpendidikan($xKd_jenjang, $xNm_jenjang, $xjenjang_ing) {
        $xStr = " INSERT INTO pendidikan( " .
                "Kd_jenjang," .
                "Nm_jenjang," .
                "jenjang_ing) VALUES('" . $xKd_jenjang . "','" . $xNm_jenjang . "','" . $xjenjang_ing . "')";
        $query = $this->db->query($xStr);
        return $xKd_jenjang;
    }

    Function setUpdatependidikan($xKd_jenjang, $xNm_jenjang, $xjenjang_ing) {
        $xStr = " UPDATE pendidikan SET " .
                "Kd_jenjang='" . $xKd_jenjang . "'," .
                "Nm_jenjang='" . $xNm_jenjang . "'," .
                "jenjang_ing='" . $xjenjang_ing . "' WHERE Kd_jenjang = '" . $xKd_jenjang . "'";
        $query = $this->db->query($xStr);
        return $xKd_jenjang;
    }

    function setDeletependidikan($xKd_jenjang) {
        $xStr = " DELETE FROM pendidikan WHERE pendidikan.Kd_jenjang = '" . $xKd_jenjang . "'";

        $query = $this->db->query($xStr);
    }

}

?>
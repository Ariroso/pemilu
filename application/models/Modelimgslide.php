<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : imgslide
 * di Buat oleh Diar PHP Generator
 * Update List untuk grid karena program generatorku lom sempurna ya hehehehehe */

class modelimgslide extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListimgslide() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx," .
                "url," .
                "keterangan," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai," .
                "link" .
                " FROM imgslide   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->url;
        }
        return $xBuffResul;
    }

    function getListimgslide($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where url like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx," .
                "url," .
                "keterangan," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai," .
                "link" .
                " FROM imgslide $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailimgslide($xidx) {
        $xStr = "SELECT " .
                "idx," .
                "url," .
                "keterangan," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai," .
                "link" .
                " FROM imgslide  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndeximgslide() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx," .
                "url," .
                "keterangan," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai," .
                "link" .
                " FROM imgslide order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertimgslide($xidx, $xurl, $xketerangan, $xtglinsert, $xtglupdate, $xidpegawai,$xlink) {
        $xStr = " INSERT INTO imgslide( " .
                "idx," .
                "url," .
                "keterangan," .
                "tglinsert," .
                "tglupdate," .
                "idpegawai," .
                "link) VALUES('" . $xidx . "','" . $xurl . "','" . $xketerangan . "',NOW(),NOW(),'" . $xidpegawai . "','" . $xlink . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateimgslide($xidx, $xurl, $xketerangan, $xtglinsert, $xtglupdate, $xidpegawai,$xlink) {
        $xStr = " UPDATE imgslide SET " .
                "idx='" . $xidx . "'," .
                "url='" . $xurl . "'," .
                "keterangan='" . $xketerangan . "'," .
                //"tglinsert='" . $xtglinsert . "'," .
                "tglupdate=NOW()," .
                "idpegawai='" . $xidpegawai . "',".
                "link='" . $xlink . "' WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteimgslide($xidx) {
        $xStr = " DELETE FROM imgslide WHERE imgslide.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteimgslide($xidx);
    }

    function setInsertLogDeleteimgslide($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'imgslide',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

?>
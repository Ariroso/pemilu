<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : partai
 * di Buat oleh Diar PHP Generator */

class Modelpartai extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListpartai() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",partai" .
                " FROM partai   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->partai;
        }
        return $xBuffResul;
    }

    function getListpartai($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",partai" .
                " FROM partai $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailpartai($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",partai" .
                " FROM partai  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexpartai() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",partai" .
                " FROM partai order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertpartai($xidx, $xpartai) {
        $xStr = " INSERT INTO partai( " .
                "idx" .
                ",partai" .
                ") VALUES('" . $xidx . "','" . $xpartai . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatepartai($xidx, $xpartai) {
        $xStr = " UPDATE partai SET " .
                "idx='" . $xidx . "'" .
                ",partai='" . $xpartai . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletepartai($xidx) {
        $xStr = " DELETE FROM partai WHERE partai.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletepartai($xidx);
    }

    function setInsertLogDeletepartai($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'partai',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

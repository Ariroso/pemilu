<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : dapil
 * di Buat oleh Diar PHP Generator */

class Modeldapil extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListdapil() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",dapil" .
                " FROM dapil   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->dapil;
        }
        return $xBuffResul;
    }

    function getListdapil($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",dapil" .
                " FROM dapil $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetaildapil($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",dapil" .
                " FROM dapil  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexdapil() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",dapil" .
                " FROM dapil order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertdapil($xidx, $xdapil) {
        $xStr = " INSERT INTO dapil( " .
                "idx" .
                ",dapil" .
                ") VALUES('" . $xidx . "','" . $xdapil . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatedapil($xidx, $xdapil) {
        $xStr = " UPDATE dapil SET " .
                "idx='" . $xidx . "'" .
                ",dapil='" . $xdapil . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletedapil($xidx) {
        $xStr = " DELETE FROM dapil WHERE dapil.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletedapil($xidx);
    }

    function setInsertLogDeletedapil($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'dapil',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

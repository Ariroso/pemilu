<?php if(!defined('BASEPATH')) exit('Tidak Diperkenankan mengakses langsung'); 
/* Class  Model : pemilih
 * di Buat oleh Diar PHP Generator*/

  class Modelpemilih extends CI_Model {
  function __construct()
 {
    parent::__construct();
 }


    
function getArrayListpemilih(){ /* spertinya perlu lock table*/ 
 $xBuffResul = array(); 
 $xStr =  "SELECT ".
      "idx".",nokk".
",nik".
",nama".
",tempatlahir".
",tgllahir".
",perkawinan".
",jeniskelamin".
",idpendidikan".
",idpekerjaan".
",idjabatan".
",alamat".
",rt".
",rw".
",disabilitas".
",keterangan".
",iddapil".
",idtps".
",idtahun".
",idanggota".

" FROM pemilih   order by idx ASC "; 
 $query = $this->db->query($xStr); 
 foreach ($query->result() as $row) 
 { 
   $xBuffResul[$row->idx] = $row->idx; 
   } 
return $xBuffResul;
}
    
function getListpemilih($xAwal,$xLimit,$xSearch=''){
if(!empty($xSearch)){ 
     $xSearch = "Where idx like '%".$xSearch."%'" ;
 }   
 $xStr =   "SELECT ".
      "idx".
      ",nokk".
",nik".
",nama".
",tempatlahir".
",tgllahir".
",perkawinan".
",jeniskelamin".
",idpendidikan".
",idpekerjaan".
",idjabatan".
",alamat".
",rt".
",rw".
",disabilitas".
",keterangan".
",iddapil".
",idtps".
",idtahun".
",idanggota".
" FROM pemilih $xSearch order by idx DESC limit ".$xAwal.",".$xLimit;  
 $query = $this->db->query($xStr);
 return $query ;
}

 
function getDetailpemilih($xidx){
 $xStr =   "SELECT ".
      "idx".
   ",nokk".
",nik".
",nama".
",tempatlahir".
",tgllahir".
",perkawinan".
",jeniskelamin".
",idpendidikan".
",idpekerjaan".
",idjabatan".
",alamat".
",rt".
",rw".
",disabilitas".
",keterangan".
",iddapil".
",idtps".
",idtahun".
",idanggota".

" FROM pemilih  WHERE idx = '".$xidx."'";

 $query = $this->db->query($xStr);
$row = $query->row();
 return $row;
}

  
function getLastIndexpemilih(){ /* spertinya perlu lock table*/ 
 $xStr =   "SELECT ".
      "idx".
      ",nokk".
",nik".
",nama".
",tempatlahir".
",tgllahir".
",perkawinan".
",jeniskelamin".
",idpendidikan".
",idpekerjaan".
",idjabatan".
",alamat".
",rt".
",rw".
",disabilitas".
",keterangan".
",iddapil".
",idtps".
",idtahun".
",idanggota".

" FROM pemilih order by idx DESC limit 1 ";
 $query = $this->db->query($xStr);
$row = $query->row();
 return $row;
}


  
 Function setInsertpemilih($xidx,$xnokk,$xnik,$xnama,$xtempatlahir,$xtgllahir,$xperkawinan,$xjeniskelamin,$xidpendidikan,$xidpekerjaan,$xidjabatan,$xalamat,$xrt,$xrw,$xdisabilitas,$xketerangan,$xiddapil,$xidtps,$xidtahun,$xidanggota)
{
  $xStr =  " INSERT INTO pemilih( ".
              "idx".
              ",nokk".
",nik".
",nama".
",tempatlahir".
",tgllahir".
",perkawinan".
",jeniskelamin".
",idpendidikan".
",idpekerjaan".
",idjabatan".
",alamat".
",rt".
",rw".
",disabilitas".
",keterangan".
",iddapil".
",idtps".
",idtahun".
",idanggota".
") VALUES('".$xidx."','".$xnokk."','".$xnik."','".$xnama."','".$xtempatlahir."','".$xtgllahir."','".$xperkawinan."','".$xjeniskelamin."','".$xidpendidikan."','".$xidpekerjaan."','".$xidjabatan."','".$xalamat."','".$xrt."','".$xrw."','".$xdisabilitas."','".$xketerangan."','".$xiddapil."','".$xidtps."','".$xidtahun."','".$xidanggota."')";
$query = $this->db->query($xStr);
 return $xidx;
}

Function setUpdatepemilih($xidx,$xnokk,$xnik,$xnama,$xtempatlahir,$xtgllahir,$xperkawinan,$xjeniskelamin,$xidpendidikan,$xidpekerjaan,$xidjabatan,$xalamat,$xrt,$xrw,$xdisabilitas,$xketerangan,$xiddapil,$xidtps,$xidtahun,$xidanggota)
{
  $xStr =  " UPDATE pemilih SET ".
             "idx='".$xidx."'".
              ",nokk='".$xnokk."'".
 ",nik='".$xnik."'".
 ",nama='".$xnama."'".
 ",tempatlahir='".$xtempatlahir."'".
 ",tgllahir='".$xtgllahir."'".
 ",perkawinan='".$xperkawinan."'".
 ",jeniskelamin='".$xjeniskelamin."'".
 ",idpendidikan='".$xidpendidikan."'".
 ",idpekerjaan='".$xidpekerjaan."'".
 ",idjabatan='".$xidjabatan."'".
 ",alamat='".$xalamat."'".
 ",rt='".$xrt."'".
 ",rw='".$xrw."'".
 ",disabilitas='".$xdisabilitas."'".
 ",keterangan='".$xketerangan."'".
 ",iddapil='".$xiddapil."'".
 ",idtps='".$xidtps."'".
 ",idtahun='".$xidtahun."'".
 ",idanggota='".$xidanggota."'".
 " WHERE idx = '".$xidx."'";
 $query = $this->db->query($xStr);
 return $xidx;
}

function setDeletepemilih($xidx)
{
 $xStr =  " DELETE FROM pemilih WHERE pemilih.idx = '".$xidx."'";

 $query = $this->db->query($xStr);
 $this->setInsertLogDeletepemilih($xidx);
}

function setInsertLogDeletepemilih($xidx)
{
 $xidpegawai = $this->session->userdata('idpegawai');    $xStr="insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'pemilih',now(),$xidpegawai)"; 
    $query = $this->db->query($xStr);
}

}

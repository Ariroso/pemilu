<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : tps
 * di Buat oleh Diar PHP Generator */

class Modeltps extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListtps() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                " FROM tps   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->tps;
        }
        return $xBuffResul;
    }
    function getArrayListtpsbydapil($iddapil) { 
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                " FROM tps where iddapil = $iddapil order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul[0]='-TPS-';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->tps;
        }
        return $xBuffResul;
    }

    function getListtps($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                " FROM tps $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailtps($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                " FROM tps  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndextps() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                " FROM tps order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInserttps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun) {
        $xStr = " INSERT INTO tps( " .
                "idx" .
                ",tps" .
                ",urut" .
                ",idketua" .
                ",keterangan" .
                ",idkecamatan" .
                ",idkelurahan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",iddapil" .
                ",idtahun" .
                ") VALUES('" . $xidx . "','" . $xtps . "','" . $xurut . "','" . $xidketua . "','" . $xketerangan . "','" . $xidkecamatan . "','" . $xidkelurahan . "','" . $xidkabupaten . "','" . $xidprovinsi . "','" . $xiddapil . "','" . $xidtahun . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatetps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun) {
        $xStr = " UPDATE tps SET " .
                "idx='" . $xidx . "'" .
                ",tps='" . $xtps . "'" .
                ",urut='" . $xurut . "'" .
                ",idketua='" . $xidketua . "'" .
                ",keterangan='" . $xketerangan . "'" .
                ",idkecamatan='" . $xidkecamatan . "'" .
                ",idkelurahan='" . $xidkelurahan . "'" .
                ",idkabupaten='" . $xidkabupaten . "'" .
                ",idprovinsi='" . $xidprovinsi . "'" .
                ",iddapil='" . $xiddapil . "'" .
                ",idtahun='" . $xidtahun . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletetps($xidx) {
        $xStr = " DELETE FROM tps WHERE tps.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletetps($xidx);
    }

    function setInsertLogDeletetps($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'tps',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : jabatan
 * di Buat oleh Diar PHP Generator */

class Modeljabatan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListjabatan() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",jabatan" .
                ",keterangan" .
                " FROM jabatan   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->jabatan;
        }
        return $xBuffResul;
    }

    function getListjabatan($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",jabatan" .
                ",keterangan" .
                " FROM jabatan $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailjabatan($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",jabatan" .
                ",keterangan" .
                " FROM jabatan  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexjabatan() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",jabatan" .
                ",keterangan" .
                " FROM jabatan order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertjabatan($xidx, $xjabatan, $xketerangan) {
        $xStr = " INSERT INTO jabatan( " .
                "idx" .
                ",jabatan" .
                ",keterangan" .
                ") VALUES('" . $xidx . "','" . $xjabatan . "','" . $xketerangan . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatejabatan($xidx, $xjabatan, $xketerangan) {
        $xStr = " UPDATE jabatan SET " .
                "idx='" . $xidx . "'" .
                ",jabatan='" . $xjabatan . "'" .
                ",keterangan='" . $xketerangan . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletejabatan($xidx) {
        $xStr = " DELETE FROM jabatan WHERE jabatan.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletejabatan($xidx);
    }

    function setInsertLogDeletejabatan($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'jabatan',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

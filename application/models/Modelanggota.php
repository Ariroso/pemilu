<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : anggota
 * di Buat oleh Diar PHP Generator */

class Modelanggota extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListanggota() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                " FROM anggota   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->idx;
        }
        return $xBuffResul;
    }

    function getListanggota($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                " FROM anggota $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailanggota($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                " FROM anggota  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getlogin($username, $password) {
        $xStr = "SELECT " .
                "idx" .
                ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                " FROM anggota  WHERE username = '" . addslashes($username) . "' AND password='" . addslashes($password) . "' AND isaktif='Y' ";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexanggota() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                " FROM anggota order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif) {
        $xStr = " INSERT INTO anggota( " .
                "idx" .
                ",noktp" .
                ",namalengkap" .
                ",jeniskelamin" .
                ",idpendidikan" .
                ",idpekerjaan" .
                ",idjabatan" .
                ",alamat" .
                ",idkelurahan" .
                ",idkecamatan" .
                ",idkabupaten" .
                ",idprovinsi" .
                ",notelpon" .
                ",username" .
                ",password" .
                ",iddapil" .
                ",idtps" .
                ",idpartai" .
                ",idtahun" .
                ",isaktif" .
                ") VALUES('" . $xidx . "','" . $xnoktp . "','" . $xnamalengkap . "','" . $xjeniskelamin . "','" . $xidpendidikan . "','" . $xidpekerjaan . "','" . $xidjabatan . "','" . $xalamat . "','" . $xidkelurahan . "','" . $xidkecamatan . "','" . $xidkabupaten . "','" . $xidprovinsi . "','" . $xnotelpon . "','" . $xusername . "','" . $xpassword . "','" . $xiddapil . "','" . $xidtps . "','" . $xidpartai . "','" . $xidtahun . "','" . $xisaktif . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdateanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif) {
        $xStr = " UPDATE anggota SET " .
                "idx='" . $xidx . "'" .
                ",noktp='" . $xnoktp . "'" .
                ",namalengkap='" . $xnamalengkap . "'" .
                ",jeniskelamin='" . $xjeniskelamin . "'" .
                ",idpendidikan='" . $xidpendidikan . "'" .
                ",idpekerjaan='" . $xidpekerjaan . "'" .
                ",idjabatan='" . $xidjabatan . "'" .
                ",alamat='" . $xalamat . "'" .
                ",idkelurahan='" . $xidkelurahan . "'" .
                ",idkecamatan='" . $xidkecamatan . "'" .
                ",idkabupaten='" . $xidkabupaten . "'" .
                ",idprovinsi='" . $xidprovinsi . "'" .
                ",notelpon='" . $xnotelpon . "'" .
                ",username='" . $xusername . "'" .
                ",password='" . $xpassword . "'" .
                ",iddapil='" . $xiddapil . "'" .
                ",idtps='" . $xidtps . "'" .
                ",idpartai='" . $xidpartai . "'" .
                ",idtahun='" . $xidtahun . "'" .
                ",isaktif='" . $xisaktif . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeleteanggota($xidx) {
        $xStr = " DELETE FROM anggota WHERE anggota.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeleteanggota($xidx);
    }

    function setInsertLogDeleteanggota($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'anggota',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

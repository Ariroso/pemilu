<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : tahun
 * di Buat oleh Diar PHP Generator */

class Modeltahun extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListtahun() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",tahun" .
                " FROM tahun   order by idx ASC ";
        $query = $this->db->query($xStr);
        $xBuffResul[0] = '-';
        foreach ($query->result() as $row) {
            $xBuffResul[$row->tahun] = $row->tahun;
        }
        return $xBuffResul;
    }

    function getListtahun($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",tahun" .
                " FROM tahun $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailtahun($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",tahun" .
                " FROM tahun  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getDetailtahunnow() {
        $xStr = "SELECT " .
                "idx" .
                ",tahun" .
                " FROM tahun  WHERE tahun = YEAR(CURDATE())";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndextahun() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",tahun" .
                " FROM tahun order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInserttahun($xidx, $xtahun) {
        $xStr = " INSERT INTO tahun( " .
                "idx" .
                ",tahun" .
                ") VALUES('" . $xidx . "','" . $xtahun . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatetahun($xidx, $xtahun) {
        $xStr = " UPDATE tahun SET " .
                "idx='" . $xidx . "'" .
                ",tahun='" . $xtahun . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletetahun($xidx) {
        $xStr = " DELETE FROM tahun WHERE tahun.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletetahun($xidx);
    }

    function setInsertLogDeletetahun($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'tahun',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

    function setmanualtahun($xtahunajar, $xgelombang = '') {
        $xStr = " SELECT * FROM tahun";

        $query = $this->db->query($xStr);
        $row = $query->row();
        $this->session->set_userdata('idtahun', $row->idx);
        $this->session->set_userdata('tahun', $row->tahun);
        return TRUE;
    }

}

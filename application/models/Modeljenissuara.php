<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Model : jenissuara
 * di Buat oleh Diar PHP Generator */

class Modeljenissuara extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getArrayListjenissuara() { /* spertinya perlu lock table */
        $xBuffResul = array();
        $xStr = "SELECT " .
                "idx" . ",jenissuara" .
                " FROM jenissuara   order by idx ASC ";
        $query = $this->db->query($xStr);
        foreach ($query->result() as $row) {
            $xBuffResul[$row->idx] = $row->jenissuara;
        }
        return $xBuffResul;
    }

    function getListjenissuara($xAwal, $xLimit, $xSearch = '') {
        if (!empty($xSearch)) {
            $xSearch = "Where idx like '%" . $xSearch . "%'";
        }
        $xStr = "SELECT " .
                "idx" .
                ",jenissuara" .
                " FROM jenissuara $xSearch order by idx DESC limit " . $xAwal . "," . $xLimit;
        $query = $this->db->query($xStr);
        return $query;
    }

    function getDetailjenissuara($xidx) {
        $xStr = "SELECT " .
                "idx" .
                ",jenissuara" .
                " FROM jenissuara  WHERE idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    function getLastIndexjenissuara() { /* spertinya perlu lock table */
        $xStr = "SELECT " .
                "idx" .
                ",jenissuara" .
                " FROM jenissuara order by idx DESC limit 1 ";
        $query = $this->db->query($xStr);
        $row = $query->row();
        return $row;
    }

    Function setInsertjenissuara($xidx, $xjenissuara) {
        $xStr = " INSERT INTO jenissuara( " .
                "idx" .
                ",jenissuara" .
                ") VALUES('" . $xidx . "','" . $xjenissuara . "')";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    Function setUpdatejenissuara($xidx, $xjenissuara) {
        $xStr = " UPDATE jenissuara SET " .
                "idx='" . $xidx . "'" .
                ",jenissuara='" . $xjenissuara . "'" .
                " WHERE idx = '" . $xidx . "'";
        $query = $this->db->query($xStr);
        return $xidx;
    }

    function setDeletejenissuara($xidx) {
        $xStr = " DELETE FROM jenissuara WHERE jenissuara.idx = '" . $xidx . "'";

        $query = $this->db->query($xStr);
        $this->setInsertLogDeletejenissuara($xidx);
    }

    function setInsertLogDeletejenissuara($xidx) {
        $xidpegawai = $this->session->userdata('idpegawai');
        $xStr = "insert into logdelrecord(idxhapus,nmtable,tgllog,ideksekusi) values($xidx,'jenissuara',now(),$xidpegawai)";
        $query = $this->db->query($xStr);
    }

}

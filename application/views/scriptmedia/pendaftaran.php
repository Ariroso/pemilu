<div class="container daftar py-5 px-0">
    <div class="row mb-4">
        <div class="col text-center">
            <h3>Registrasi Awal</h3>
            <p class="text-muted">Silahkan isi formulir Registrasi Awal berikut ini:</p>
            <hr>
        </div>
    </div>

    <form id="confirm">

        <div class="form-group row">
            <label for="nama" class="col-sm-4 col-form-label">Nama Peserta Didik</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="ednama" name="ednama" placeholder="Nama lengkap sesuai ijazah SD &amp; SMP">
            </div>
        </div>

        <div class="form-group row">
            <label for="nama" class="col-sm-4 col-form-label">Tanggal Lahir</label>
            <div class="col-sm-8">
                <input type="text" class="form-control tanggal" name="edtanggallahir" id="edtanggallahir" placeholder="tanggallahir">
                  <small><b>Format : DD-MM-YYYY</b> </small>
            </div>
        </div>

        <div class="form-group row">
            <label for="nama" class="col-sm-4 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline align-items-center">
                    <input type="radio" id="edjeniskelamin1" name="edjeniskelamin" value="L" class="custom-control-input">
                    <label class="custom-control-label" for="edjeniskelamin1">Laki-laki</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline align-items-center">
                    <input type="radio" id="edjeniskelamin2" name="edjeniskelamin" value="P" class="custom-control-input">
                    <label class="custom-control-label" for="edjeniskelamin2">Perempuan</label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="nama" class="col-sm-4 col-form-label">Alamat Email</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="edemail" id="edemail" placeholder="demo@gmail.com (Email aktif untuk notifikasi pembayaran dll.)" >
            </div>
        </div>
        <div class="form-group row">
            <!--<label for="nama" class="col-sm-4 col-form-label">Nomor WhatsApp</label>-->
            <label for="nama" class="col-sm-4 col-form-label">Nomor Handphone</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="ednohp" id="ednohp" placeholder="(Nomor HP aktif untuk notifikasi pembayaran dll.) Format : 62856xxxxx ">
                  <small><b>Format : 6280000000000</b> </small>
            </div>
        </div>
        <!--        <div class="form-group row">
                    <label for="nama" class="col-sm-4 col-form-label">Pas Foto 4x6</label>
                    <div class="col-sm-8 custom-file">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input"  id="customFile">
                            <label class="custom-file-label" for="customFile">Pilih Foto</label>
                        </div>
                    </div>
                </div>-->

        <div class="form-group row mt-5">
            <div class="col text-center ">
                <button type="button" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanpendaftaranawal();">Daftar</button>
            </div>
        </div>

    </form>
</div>
<script>
    if ($("#ednohp").val().trim().length === 0) {
        $("#ednohp").val(62);
    }
</script>
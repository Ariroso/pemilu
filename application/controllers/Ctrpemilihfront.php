<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : asalsekolah   *  By Diar */;

class ctrpemilihfront extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        $this->load->model('basemodel');
    }

    public function index() {
        $xtanggal = date('Y-m-d'); // Hasil: 20-01-2017 05:32:15
        $this->load->model('modelanggota');
        $idanggota = $this->session->userdata('idanggota');

        if (empty($idanggota)) {
            redirect(site_url(), '');
        }
        $row = $this->modelanggota->getDetailanggota($idanggota);
        $data['maincontent'] = $this->createformpemilihfront($idanggota, $row->namalengkap, $row->password, $row->username);

        $data['header'] = $this->basemodel->header(
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/ui/jquery-ui.js"> </script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxpemilihfront.js"> </script>');
        $data['slider'] = '';
        $data['footer'] = $this->basemodel->footer();
        $this->load->view('scriptmedia/index', $data);
    }

    function slider() {
        $this->load->model('modelimgslide');
        $this->load->model('basemodel');
        $qslider = $this->modelimgslide->getListimgslide(0, 10);
        $data = [];
        foreach ($qslider->result() as $row) {
            $data['qslider'][] = array(
                'image' => (!empty($row->url)) ? $this->basemodel->showimage($row->url, 'slide') : '',
                'keterangan' => $row->keterangan,
                'link' => $row->link
            );
        }
        return $this->load->view("scriptmedia/slider", $data, TRUE);
    }

    /*
     * Form Core Start
     */

    function createformpemilihfront($idanggota, $nama, $password, $user) {
        $this->load->helper('form');
        $this->load->helper('html');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctranggota/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelpendidikan');
        $this->load->model('modelpekerjaan');
        $this->load->model('modeljabatan');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modeltahun');
        $iddapil=$this->session->userdata('iddapil');
        $idtps=$this->session->userdata('idtps');
        echo $iddapil;
        echo $idtps;
        if(!empty($idtps))
        {
            $tps = setFormfrontend('idtps', 'TPS', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), $idtps, ' id="edidtps" placeholder="idtps" disabled="disabled"'));
        }else{
            $tps = setFormfrontend('idtps', 'TPS', form_dropdown_('edidtps', $this->modeltps->getArrayListtpsbydapil($iddapil), '', ' id="edidtps" placeholder="idtps" '));
        }

        $xBufResult = '';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidanggota" id="edidanggota" value="' . $idanggota . '" />';

        $xBufResult .= setFormfrontend('nokk', 'No KK', form_input_frontend_(getArrayObj('ednokk', '', ''), '', ' placeholder="No KK" '));

        $xBufResult .= setFormfrontend('nik', 'NIK', form_input_frontend_(getArrayObj('ednik', '', ''), '', ' placeholder="NIK" '));

        $xBufResult .= setFormfrontend('nama', 'Nama', form_input_frontend_(getArrayObj('ednama', '', ''), '', ' placeholder="Nama" '));

        $xBufResult .= setFormfrontend('tempatlahir', 'Tempat Lahir', form_input_frontend_(getArrayObj('edtempatlahir', '', ''), '', ' placeholder="Tempat Lahir" '));

        $xBufResult .= setFormfrontend('tgllahir', 'Tanggal Lahir', form_input_frontend_(getArrayObj('edtgllahir', '', ''), '', ' class="form-control tanggal" placeholder="tgllahir" '));

        $xBufResult .= setFormfrontend('perkawinan', 'Perkawinan', form_dropdown_('edperkawinan', getArrayperkawinan(), '', ' id="edperkawinan" placeholder="Perkawinan" '));

        $xBufResult .= setFormfrontend('jeniskelamin', 'Jenis Kelamin', form_dropdown_('edjeniskelamin', getArraygender(), '', ' id="edjeniskelamin" placeholder="Jenis Kelamin" '));

        $xBufResult .= setFormfrontend('idpendidikan', 'Pendidikan', form_dropdown_('edidpendidikan', $this->modelpendidikan->getArraylistpendidikan(), '', ' id="edidpendidikan" placeholder="Pendidikan" '));

        $xBufResult .= setFormfrontend('idpekerjaan', 'Pekerjaan', form_dropdown_('edidpekerjaan', $this->modelpekerjaan->getArraylistpekerjaan(), '', ' id="edidpekerjaan" placeholder="Pekerjaan" '));

        $xBufResult .= setFormfrontend('idjabatan', 'Jabatan', form_dropdown_('edidjabatan', $this->modeljabatan->getArraylistjabatan(), '', ' placeholder="Jabatan" '));

        $xBufResult .= setFormfrontend('alamat', 'Alamat', form_input_frontend_(getArrayObj('edalamat', '', ''), '', ' placeholder="Alamat" '));

        $xBufResult .= setFormfrontend('rt', 'RT', form_input_frontend_(getArrayObj('edrt', '', '200'), '', ' placeholder="RT" '));

        $xBufResult .= setFormfrontend('rw', 'RT', form_input_frontend_(getArrayObj('edrw', '', '200'), '', ' placeholder="RW" '));

        $xBufResult .= setFormfrontend('disabilitas', 'Disabilitas', form_dropdown_('eddisabilitas', getArrayYaTidak(), '', ' id="eddisabilitas" placeholder="Disabilitas" '));

        $xBufResult .= setFormfrontend('keterangan', 'Keterangan ', form_textarea(getArrayObj('edketerangan', '', '700'), '', ' placeholder="Keterangan" '));

        $xBufResult .= setFormfrontend('iddapil', 'Dapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), $iddapil, ' id="ediddapil" placeholder="iddapil" disabled="disabled"'));

        $xBufResult .= $tps;

//        $xBufResult .= '<div id="tps">' . setFormfrontend('idtps', 'TPS', form_dropdown('edidtps', array('-TPS-'), '', ' class="form-control" id="edidtps" required="required" disabled="disabled"'), '') . '</div>';
//        $xBufResult .= setForm('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idtahun', 'Tahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" '));

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanpemilihfront();" class="btn btn-success col-md-2 offset-md-4"') . '<div class="spacer"></div>';

        return $xBufResult;
    }

    function number_pad($number, $n) {
        return str_pad((int) $number, $n, "0", STR_PAD_LEFT);
    }

    /*
     * Form add  End
     */

    function tpsbydapil() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xiddapil = $_POST['ediddapil'];
        $this->load->model('modeltps');
//        $this->load->model('modelprovinsi');
        $query = $this->modeltps->getArrayListtpsbydapil((int) $xiddapil);
        $xBufResult = '';
        if (!empty($query)) {
//            $xBufResult = setFormfrontend('edkabupaten', 'Kabupaten*', form_dropdown('edidkabupaten', $query, '', ' class="form-control" id="edidkabupaten" onchange="kabupatenselectbysiswa()" required="required" required="required"'));
            $xBufResult .= setFormfrontend('idtps', 'TPS', form_dropdown_('edidtps', $query, '', ' id="edidtps" placeholder="idtps" '));
        } else {
            $xBufResult .= setFormfrontend('idtps', 'TPS', form_dropdown('edidtps', array('-TPS-'), '', ' class="form-control" id="edidtps" required="required"'), '');
        }
        $this->json_data['combotps'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function simpanpemilihfront() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xnokk = $_POST['ednokk'];
        $xnik = $_POST['ednik'];
        $xnama = $_POST['ednama'];
        $xtempatlahir = $_POST['edtempatlahir'];
        $xtgllahir = $_POST['edtgllahir'];
        $xperkawinan = $_POST['edperkawinan'];
        $xjeniskelamin = $_POST['edjeniskelamin'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xalamat = $_POST['edalamat'];
        $xrt = $_POST['edrt'];
        $xrw = $_POST['edrw'];
        $xdisabilitas = $_POST['eddisabilitas'];
        $xketerangan = $_POST['edketerangan'];
        $xiddapil = $_POST['ediddapil'];
        $xidtps = $_POST['edidtps'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->model('modelpemilih');
        $xidpegawai = $this->session->userdata('idanggota');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelpemilih->setUpdatepemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, datetomysql($xtgllahir), $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
            } else {
                $xStr = $this->modelpemilih->setInsertpemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, datetomysql($xtgllahir), $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
            }
        }
        echo json_encode(null);
    }

}

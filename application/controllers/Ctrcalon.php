<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : calon   *  By Diar */

class Ctrcalon extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformcalon('0', $xAwal);
    }

    function createformcalon($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxadmin.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxcalon.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormcalon($xidx), '', '', $xAddJs, '', 'calon');
    }

    function setDetailFormcalon($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrcalon/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelpendidikan');
        $this->load->model('modelpekerjaan');
        $this->load->model('modeljabatan');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modeltahun');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('nama', 'nama', form_input_(getArrayObj('ednama', '', '200'), '', ' placeholder="nama" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('nourut', 'nourut', form_input_(getArrayObj('ednourut', '', '200'), '', ' placeholder="nourut" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('alamat', 'alamat', form_input_(getArrayObj('edalamat', '', '400'), '', ' placeholder="alamat" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpendidikan', 'idpendidikan', form_dropdown_('edidpendidikan', $this->modelpendidikan->getArrayListpendidikan(), '', ' id="edidpendidikan" placeholder="idpendidikan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpekerjaan', 'idpekerjaan', form_dropdown_('edidpekerjaan', $this->modelpekerjaan->getArrayListpekerjaan(), '', ' id="edidpekerjaan" placeholder="idpekerjaan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idjabatan', 'idjabatan', form_dropdown_('edidjabatan', $this->modeljabatan->getArrayListjabatan(), '', ' id="edidjabatan" placeholder="idjabatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('tempatlahir', 'tempatlahir', form_input_(getArrayObj('edtempatlahir', '', '200'), '', ' placeholder="tempatlahir" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('tgllahir', 'tgllahir', form_input_(getArrayObj('edtgllahir', '', '200'), '', ' class="form-control tanggal" placeholder="tgllahir" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('edidprovinsi', 'Provinsi', form_dropdown_('edidprovinsi', $this->modelprovinsi->getArraylistprovinsi(), '', ' class="form-control" id="edidprovinsi" onchange="provinsi();" required="required"'), '');
        $kabupaten = setForm('edkabupaten', 'kabupaten', form_dropdown_('edidkabupaten', $this->modelkabupaten->getArraylistkabupaten(), '', 'id="edidkabupaten" onchange="kabupaten()"')) . '<div class="spacer"></div>';
        $kecamatan = setForm('edkecamatan', 'kecamatan', form_dropdown_('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), '', 'id="edidkecamatan" onchange="kelurahan()"')) . '<div class="spacer"></div>';
        $kelurahan = setForm('edidkelurahan', 'kelurahan', form_dropdown_('edidkelurahan', $this->modelkelurahan->getArraylistkelurahan(), '', 'id="edidkelurahan" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div id="kabupaten">' . $kabupaten . '</div>';

        $xBufResult .= '<div id="kecamatan">' . $kecamatan . '</div>';

        $xBufResult .= '<div id="kelurahan">' . $kelurahan . '</div>';

        $xBufResult .= setForm('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('keterangan', 'keterangan', form_input_(getArrayObj('edketerangan', '', '200'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';


        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpancalon();"') . form_button('btNew', 'new', 'onclick="doClearcalon();"') . '<div class="spacer"></div><div id="tabledatacalon">' . $this->getlistcalon(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistcalon($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('nama', '', 'data-field="nama" data-sortable="true" width=10%') .
                tbaddcellhead('nourut', '', 'data-field="nourut" data-sortable="true" width=10%') .
                tbaddcellhead('alamat', '', 'data-field="alamat" data-sortable="true" width=10%') .
                tbaddcellhead('idpendidikan', '', 'data-field="idpendidikan" data-sortable="true" width=10%') .
                tbaddcellhead('idpekerjaan', '', 'data-field="idpekerjaan" data-sortable="true" width=10%') .
                tbaddcellhead('idjabatan', '', 'data-field="idjabatan" data-sortable="true" width=10%') .
                tbaddcellhead('tempatlahir', '', 'data-field="tempatlahir" data-sortable="true" width=10%') .
                tbaddcellhead('tgllahir', '', 'data-field="tgllahir" data-sortable="true" width=10%') .
                tbaddcellhead('idkelurahan', '', 'data-field="idkelurahan" data-sortable="true" width=10%') .
                tbaddcellhead('idkecamatan', '', 'data-field="idkecamatan" data-sortable="true" width=10%') .
                tbaddcellhead('idkabupaten', '', 'data-field="idkabupaten" data-sortable="true" width=10%') .
                tbaddcellhead('idprovinsi', '', 'data-field="idprovinsi" data-sortable="true" width=10%') .
                tbaddcellhead('iddapil', '', 'data-field="iddapil" data-sortable="true" width=10%') .
                tbaddcellhead('idpartai', '', 'data-field="idpartai" data-sortable="true" width=10%') .
                tbaddcellhead('keterangan', '', 'data-field="keterangan" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modelcalon');
        $xQuery = $this->modelcalon->getListcalon($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditcalon(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapuscalon(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->nama) .
                    tbaddcell($row->nourut) .
                    tbaddcell($row->alamat) .
                    tbaddcell($row->idpendidikan) .
                    tbaddcell($row->idpekerjaan) .
                    tbaddcell($row->idjabatan) .
                    tbaddcell($row->tempatlahir) .
                    tbaddcell($row->tgllahir) .
                    tbaddcell($row->idkelurahan) .
                    tbaddcell($row->idkecamatan) .
                    tbaddcell($row->idkabupaten) .
                    tbaddcell($row->idprovinsi) .
                    tbaddcell($row->iddapil) .
                    tbaddcell($row->idpartai) .
                    tbaddcell($row->keterangan) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchcalon(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchcalon(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchcalon(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistcalonAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['nama'] = "";
        $this->json_data['nourut'] = "";
        $this->json_data['alamat'] = "";
        $this->json_data['idpendidikan'] = "";
        $this->json_data['idpekerjaan'] = "";
        $this->json_data['idjabatan'] = "";
        $this->json_data['tempatlahir'] = "";
        $this->json_data['tgllahir'] = "";
        $this->json_data['idkelurahan'] = "";
        $this->json_data['idkecamatan'] = "";
        $this->json_data['idkabupaten'] = "";
        $this->json_data['idprovinsi'] = "";
        $this->json_data['iddapil'] = "";
        $this->json_data['idpartai'] = "";
        $this->json_data['keterangan'] = "";
        $this->json_data['idtahun'] = "";

        $response = array();
        $this->load->model('modelcalon');
        $xQuery = $this->modelcalon->getListcalon($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['nama'] = $row->nama;
            $this->json_data['nourut'] = $row->nourut;
            $this->json_data['alamat'] = $row->alamat;
            $this->json_data['idpendidikan'] = $row->idpendidikan;
            $this->json_data['idpekerjaan'] = $row->idpekerjaan;
            $this->json_data['idjabatan'] = $row->idjabatan;
            $this->json_data['tempatlahir'] = $row->tempatlahir;
            $this->json_data['tgllahir'] = $row->tgllahir;
            $this->json_data['idkelurahan'] = $row->idkelurahan;
            $this->json_data['idkecamatan'] = $row->idkecamatan;
            $this->json_data['idkabupaten'] = $row->idkabupaten;
            $this->json_data['idprovinsi'] = $row->idprovinsi;
            $this->json_data['iddapil'] = $row->iddapil;
            $this->json_data['idpartai'] = $row->idpartai;
            $this->json_data['keterangan'] = $row->keterangan;
            $this->json_data['idtahun'] = $row->idtahun;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpancalonAndroid() {
        $xidx = $_POST['edidx'];
        $xnama = $_POST['ednama'];
        $xnourut = $_POST['ednourut'];
        $xalamat = $_POST['edalamat'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xtempatlahir = $_POST['edtempatlahir'];
        $xtgllahir = $_POST['edtgllahir'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xiddapil = $_POST['ediddapil'];
        $xidpartai = $_POST['edidpartai'];
        $xketerangan = $_POST['edketerangan'];
        $xidtahun = $_POST['edidtahun'];

        $this->load->helper('json');
        $this->load->model('modelcalon');
        $response = array();
        if ($xidx != '0') {
            $this->modelcalon->setUpdatecalon($xidx, $xnama, $xnourut, $xalamat, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xtempatlahir, $xtgllahir, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidpartai, $xketerangan, $xidtahun);
        } else {
            $this->modelcalon->setInsertcalon($xidx, $xnama, $xnourut, $xalamat, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xtempatlahir, $xtgllahir, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidpartai, $xketerangan, $xidtahun);
        }
        $row = $this->modelcalon->getLastIndexcalon();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['nama'] = $row->nama;
        $this->json_data['nourut'] = $row->nourut;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['tempatlahir'] = $row->tempatlahir;
        $this->json_data['tgllahir'] = $row->tgllahir;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idtahun'] = $row->idtahun;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editreccalon() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelcalon');
        $row = $this->modelcalon->getDetailcalon($xIdEdit);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['nama'] = $row->nama;
        $this->json_data['nourut'] = $row->nourut;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['tempatlahir'] = $row->tempatlahir;
        $this->json_data['tgllahir'] = mysqltodate($row->tgllahir);
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idtahun'] = $row->idtahun;

        echo json_encode($this->json_data);
    }

    function deletetablecalon() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelcalon');
        $this->modelcalon->setDeletecalon($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchcalon() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatacalon'] = $this->getlistcalon($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function getautocomplatecalon() {
        $namacalon = $_POST['ednamacalon'];
        $this->load->helper('json');
        $this->load->model('modelcalon');
        $query = $this->modelcalon->getListcalon(0, 10, $namacalon);
        $xdata = array();
        foreach ($query->result() as $row) {
            $xdata[] = array('label' => $row->calon, 'value' => $row->idx);
        }
        $this->json_data['data'] = $xdata;
        echo json_encode($this->json_data);
    }

    function simpancalon() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xnama = $_POST['ednama'];
        $xnourut = $_POST['ednourut'];
        $xalamat = $_POST['edalamat'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xtempatlahir = $_POST['edtempatlahir'];
        $xtgllahir = $_POST['edtgllahir'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xiddapil = $_POST['ediddapil'];
        $xidpartai = $_POST['edidpartai'];
        $xketerangan = $_POST['edketerangan'];
        $xidtahun = $_POST['edidtahun'];

        $this->load->model('modelcalon');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelcalon->setUpdatecalon($xidx, $xnama, $xnourut, $xalamat, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xtempatlahir, datetomysql($xtgllahir), $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidpartai, $xketerangan, $xidtahun);
            } else {
                $xStr = $this->modelcalon->setInsertcalon($xidx, $xnama, $xnourut, $xalamat, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xtempatlahir, datetomysql($xtgllahir), $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidpartai, $xketerangan, $xidtahun);
            }
        }
        echo json_encode(null);
    }

}

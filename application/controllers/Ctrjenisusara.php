<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : jenisusara   *  By Diar */

class Ctrjenisusara extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformjenisusara('0', $xAwal);
    }

    function createformjenisusara($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxjenisusara.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormjenisusara($xidx), '', '', $xAddJs, '', 'jenisusara');
    }

    function setDetailFormjenisusara($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrjenisusara/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('jenisusara', 'jenisusara', form_input_(getArrayObj('edjenisusara', '', '200'), '', ' placeholder="jenisusara" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanjenisusara();"') . form_button('btNew', 'new', 'onclick="doClearjenisusara();"') . '<div class="spacer"></div><div id="tabledatajenisusara">' . $this->getlistjenisusara(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistjenisusara($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('jenisusara', '', 'data-field="jenisusara" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modeljenisusara');
        $xQuery = $this->modeljenisusara->getListjenisusara($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditjenisusara(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapusjenisusara(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->jenisusara) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchjenisusara(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchjenisusara(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchjenisusara(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistjenisusaraAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['jenisusara'] = "";

        $response = array();
        $this->load->model('modeljenisusara');
        $xQuery = $this->modeljenisusara->getListjenisusara($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['jenisusara'] = $row->jenisusara;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpanjenisusaraAndroid() {
        $xidx = $_POST['edidx'];
        $xjenisusara = $_POST['edjenisusara'];

        $this->load->helper('json');
        $this->load->model('modeljenisusara');
        $response = array();
        if ($xidx != '0') {
            $this->modeljenisusara->setUpdatejenisusara($xidx, $xjenisusara);
        } else {
            $this->modeljenisusara->setInsertjenisusara($xidx, $xjenisusara);
        }
        $row = $this->modeljenisusara->getLastIndexjenisusara();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jenisusara'] = $row->jenisusara;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecjenisusara() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeljenisusara');
        $row = $this->modeljenisusara->getDetailjenisusara($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jenisusara'] = $row->jenisusara;

        echo json_encode($this->json_data);
    }

    function deletetablejenisusara() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeljenisusara');
        $this->modeljenisusara->setDeletejenisusara($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchjenisusara() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatajenisusara'] = $this->getlistjenisusara($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpanjenisusara() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xjenisusara = $_POST['edjenisusara'];

        $this->load->model('modeljenisusara');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeljenisusara->setUpdatejenisusara($xidx, $xjenisusara);
            } else {
                $xStr = $this->modeljenisusara->setInsertjenisusara($xidx, $xjenisusara);
            }
        }
        echo json_encode(null);
    }

}

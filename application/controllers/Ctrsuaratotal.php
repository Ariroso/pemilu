<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : suaratotal   *  By Diar */

class Ctrsuaratotal extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformsuaratotal('0', $xAwal);
    }

    function createformsuaratotal($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsuaratotal.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormsuaratotal($xidx), '', '', $xAddJs, '', 'suaratotal');
    }

    function setDetailFormsuaratotal($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrsuaratotal/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modeltps');
        $this->load->model('modeljenissuara');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidanggota" id="edidanggota" value="0" />';

        $xBufResult .= setForm('jumlahsuara', 'jumlahsuara', form_input_(getArrayObj('edjumlahsuara', '', '200'), '', ' placeholder="jumlahsuara" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idjenissuara', 'idjenissuara', form_dropdown_('edidjenissuara', $this->modeljenissuara->getArrayListjenissuara(), '', ' id="edidjenissuara" placeholder="idjenissuara" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('keterangan', 'keterangan', form_textarea(getArrayObj('edketerangan', '', '400'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';

//$xBufResult .= setForm('idtahun','idtahun',form_input_(getArrayObj('edidtahun','','200'),'',' placeholder="idtahun" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idanggota','idanggota',form_input_(getArrayObj('edidanggota','','200'),'',' placeholder="idanggota" ')).'<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpansuaratotal();"') . form_button('btNew', 'new', 'onclick="doClearsuaratotal();"') . '<div class="spacer"></div><div id="tabledatasuaratotal">' . $this->getlistsuaratotal(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistsuaratotal($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('jumlahsuara', '', 'data-field="jumlahsuara" data-sortable="true" width=10%') .
                tbaddcellhead('idjenissuara', '', 'data-field="idjenissuara" data-sortable="true" width=10%') .
                tbaddcellhead('idtps', '', 'data-field="idtps" data-sortable="true" width=10%') .
                tbaddcellhead('keterangan', '', 'data-field="keterangan" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('idanggota', '', 'data-field="idanggota" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modelsuaratotal');
        $xQuery = $this->modelsuaratotal->getListsuaratotal($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditsuaratotal(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapussuaratotal(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->jumlahsuara) .
                    tbaddcell($row->idjenissuara) .
                    tbaddcell($row->idtps) .
                    tbaddcell($row->keterangan) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($row->idanggota) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchsuaratotal(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchsuaratotal(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchsuaratotal(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistsuaratotalAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['jumlahsuara'] = "";
        $this->json_data['idjenissuara'] = "";
        $this->json_data['idtps'] = "";
        $this->json_data['keterangan'] = "";
        $this->json_data['idtahun'] = "";
        $this->json_data['idanggota'] = "";

        $response = array();
        $this->load->model('modelsuaratotal');
        $xQuery = $this->modelsuaratotal->getListsuaratotal($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['jumlahsuara'] = $row->jumlahsuara;
            $this->json_data['idjenissuara'] = $row->idjenissuara;
            $this->json_data['idtps'] = $row->idtps;
            $this->json_data['keterangan'] = $row->keterangan;
            $this->json_data['idtahun'] = $row->idtahun;
            $this->json_data['idanggota'] = $row->idanggota;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpansuaratotalAndroid() {
        $xidx = $_POST['edidx'];
        $xjumlahsuara = $_POST['edjumlahsuara'];
        $xidjenissuara = $_POST['edidjenissuara'];
        $xidtps = $_POST['edidtps'];
        $xketerangan = $_POST['edketerangan'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->helper('json');
        $this->load->model('modelsuaratotal');
        $response = array();
        if ($xidx != '0') {
            $this->modelsuaratotal->setUpdatesuaratotal($xidx, $xjumlahsuara, $xidjenissuara, $xidtps, $xketerangan, $xidtahun, $xidanggota);
        } else {
            $this->modelsuaratotal->setInsertsuaratotal($xidx, $xjumlahsuara, $xidjenissuara, $xidtps, $xketerangan, $xidtahun, $xidanggota);
        }
        $row = $this->modelsuaratotal->getLastIndexsuaratotal();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jumlahsuara'] = $row->jumlahsuara;
        $this->json_data['idjenissuara'] = $row->idjenissuara;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecsuaratotal() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelsuaratotal');
        $row = $this->modelsuaratotal->getDetailsuaratotal($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jumlahsuara'] = $row->jumlahsuara;
        $this->json_data['idjenissuara'] = $row->idjenissuara;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        echo json_encode($this->json_data);
    }

    function deletetablesuaratotal() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelsuaratotal');
        $this->modelsuaratotal->setDeletesuaratotal($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchsuaratotal() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatasuaratotal'] = $this->getlistsuaratotal($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpansuaratotal() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xjumlahsuara = $_POST['edjumlahsuara'];
        $xidjenissuara = $_POST['edidjenissuara'];
        $xidtps = $_POST['edidtps'];
        $xketerangan = $_POST['edketerangan'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->model('modelsuaratotal');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelsuaratotal->setUpdatesuaratotal($xidx, $xjumlahsuara, $xidjenissuara, $xidtps, $xketerangan, $xidtahun, $xidanggota);
            } else {
                $xStr = $this->modelsuaratotal->setInsertsuaratotal($xidx, $xjumlahsuara, $xidjenissuara, $xidtps, $xketerangan, $xidtahun, $xidanggota);
            }
        }
        echo json_encode(null);
    }

}

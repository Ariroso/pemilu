<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : settahunajar   *  By Diar */;

class Ctrsettahunajar extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformsettahunajar('0', $xAwal);
    }

    function createformsettahunajar($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsettahunajar.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormsettahunajar($xidx), '', '', $xAddJs, '', 'Atur Tahun Ajaran');
    }

    function setDetailFormsettahunajar($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div class="spacer"></div><div id="stylized" class="myform">' . form_open_multipart('ctrsettahunajar/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modeltahun');
//        $this->load->model('modelgelombang');
//        $this->load->model('modelkategoridaftar');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('tahunajar', 'Tahun Pemilu', form_dropdown_('edtahunajar', $this->modeltahun->getArrayListtahun(), '', ' id="edtahunajar" placeholder="tahunajar" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('gelombang', 'Gelombang', form_dropdown_('edgelombang', $this->modelgelombang->getArrayListgelombang(), '', ' id="edgelombang" placeholder="gelombang" ')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('kategoridaftar', 'Kategori daftar', form_dropdown_('edkategoridaftar', $this->modelkategoridaftar->getArrayListkategoridaftar(), '', ' id="edkategoridaftar" placeholder="gelombang" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'Simpan', 'class="btn-ursuline" onclick="dosimpansettahunajar();"') . '<div class="spacer"></div>';
        return $xBufResult;
    }

//    function getlistsettahunajar($xAwal, $xSearch) {
//        $xLimit = $this->session->userdata('limit');
//        $this->load->helper('form');
//        $this->load->helper('common');
//        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
//                tbaddcellhead('tahunajar', '', 'data-field="tahunajar" data-sortable="true" width=10%') .
//                tbaddcellhead('gelombang', '', 'data-field="gelombang" data-sortable="true" width=10%') .
//                tbaddcellhead('Edit/Hapus', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
//        $this->load->model('modelsettahunajar');
//        $xQuery = $this->modelsettahunajar->getListsettahunajar($xAwal, $xLimit, $xSearch);
//        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
//        $xbufResult .= '<tbody>';
//        foreach ($xQuery->result() as $row) {
//            $xButtonEdit = '<img src="' . base_url() . 'resource/imgbtn/edit.png" alt="Edit Data" onclick = "doeditsettahunajar(\'' . $row->idx . '\');" style="border:none;width:20px"/>';
//            $xButtonHapus = '<img src="' . base_url() . 'resource/imgbtn/delete_table.png" alt="Hapus Data" onclick = "dohapussettahunajar(\'' . $row->idx . '\');" style="border:none;">';
//            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
//                    tbaddcell($row->tahunajar) .
//                    tbaddcell($row->gelombang) .
//                    tbaddcell($xButtonEdit . '&nbsp/&nbsp' . $xButtonHapus));
//        }
//        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
//        $xButtonSearch = '<span class="input-group-btn">
//                                                <button class="btn btn-default" type="button" onclick = "dosearchsettahunajar(0);"><i class="fa fa-search"></i>
//                                                </button>
//                                            </span>';
//        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchsettahunajar(' . ($xAwal - $xLimit) . ');"/>';
//        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
//        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchsettahunajar(' . ($xAwal + $xLimit) . ');" />';
//        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
//                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';
//
//        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
//        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';
//
//        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
//                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
//                    <div class="modal-dialog modal-lg">
//                    <div   class="modal-content">
//                    <div class="modal-header">
//        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
//        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
//      </div>
//      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
//    }
//
//    function getlistsettahunajarAndroid() {
//        $this->load->helper('json');
//        $xSearch = $_POST['search'];
//        $xAwal = $_POST['start'];
//        $xLimit = $_POST['limit'];
//        $this->load->helper('form');
//        $this->load->helper('common');
//        $this->json_data['idx'] = "";
//        $this->json_data['tahunajar'] = "";
//        $this->json_data['gelombang'] = "";
//
//        $response = array();
//        $this->load->model('modelsettahunajar');
//        $xQuery = $this->modelsettahunajar->getListsettahunajar($xAwal, $xLimit, $xSearch);
//        foreach ($xQuery->result() as $row) {
//            $this->json_data['idx'] = $row->idx;
//            $this->json_data['tahunajar'] = $row->tahunajar;
//            $this->json_data['gelombang'] = $row->gelombang;
//
//            array_push($response, $this->json_data);
//        }
//        if (empty($response)) {
//            array_push($response, $this->json_data);
//        }
//        echo json_encode($response);
//    }
//
//    function simpansettahunajarAndroid() {
//        $xidx = $_POST['edidx'];
//        $xtahunajar = $_POST['edtahunajar'];
//        $xgelombang = $_POST['edgelombang'];
//
//        $this->load->helper('json');
//        $this->load->model('modelsettahunajar');
//        $response = array();
//        if ($xidx != '0') {
//            $this->modelsettahunajar->setUpdatesettahunajar($xidx, $xtahunajar, $xgelombang);
//        } else {
//            $this->modelsettahunajar->setInsertsettahunajar($xidx, $xtahunajar, $xgelombang);
//        }
//        $row = $this->modelsettahunajar->getLastIndexsettahunajar();
//        $this->json_data['idx'] = $row->idx;
//        $this->json_data['tahunajar'] = $row->tahunajar;
//        $this->json_data['gelombang'] = $row->gelombang;
//
//        $response = array();
//        array_push($response, $this->json_data);
//
//        echo json_encode($response);
//    }
//
//    function editrecsettahunajar() {
//        $xIdEdit = $_POST['edidx'];
//        $this->load->model('modelsettahunajar');
//        $row = $this->modelsettahunajar->getDetailsettahunajar($xIdEdit);
//        $this->load->helper('json');
//        $this->json_data['idx'] = $row->idx;
//        $this->json_data['tahunajar'] = $row->tahunajar;
//        $this->json_data['gelombang'] = $row->gelombang;
//
//        echo json_encode($this->json_data);
//    }
//
//    function deletetablesettahunajar() {
//        $edidx = $_POST['edidx'];
//        $this->load->model('modelsettahunajar');
//        $this->modelsettahunajar->setDeletesettahunajar($edidx);
//        $this->load->helper('json');
//        echo json_encode(null);
//    }
//
//    function searchsettahunajar() {
//        $xAwal = $_POST['xAwal'];
//        $xSearch = $_POST['xSearch'];
//        $this->load->helper('json');
//        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
//        $xlimit = $this->session->userdata('limit');
//        $xHal = 1;
//        if ($xAwal <= 0) {
//            $xHal = 1;
//        } else {
//            $xHal = ($xhalaman + 1);
//        }
//        if ($xhalaman < 0) {
//            $xHal = (($xhalaman - 1) * -1);
//        }
//        if (($xAwal + 0) == -99) {
//            $xAwal = $this->session->userdata('awal', $xAwal);
//            $xHal = $this->session->userdata('halaman', $xHal);
//        }
//        if ($xAwal + 0 <= -1) {
//            $xAwal = 0;
//            $this->session->set_userdata('awal', $xAwal);
//        } else {
//            $this->session->set_userdata('awal', $xAwal);
//        }
//        $this->json_data['tabledatasettahunajar'] = $this->getlistsettahunajar($xAwal, $xSearch);
//        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
//        echo json_encode($this->json_data);
//    }
//
    function simpansettahunajar() {
        $this->load->helper('json');

        $xtahunajar = $_POST['edtahunajar'];
        $xgelombang = $_POST['edgelombang'];
//        $xkategoridaftar = $_POST['edkategoridaftar'];

        $this->load->model('modeltahun');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            $this->modeltahun->setmanualtahun($xtahunajar, $xgelombang);
        }
        echo json_encode(null);
    }

}

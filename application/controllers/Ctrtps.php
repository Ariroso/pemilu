<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : tps   *  By Diar */

class Ctrtps extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformtps('0', $xAwal);
    }

    function createformtps($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxadmin.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxtps.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormtps($xidx), '', '', $xAddJs, '', 'tps');
    }

    function setDetailFormtps($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrtps/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeltahun');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidketua" id="edidketua" value="0" />';

        $xBufResult .= setForm('tps', 'tps', form_input_(getArrayObj('edtps', '', '200'), '', ' placeholder="tps" ')) . '<div class="spacer"></div>';


        $xBufResult .= setForm('idketua', 'idketua', form_input_(getArrayObj('ednamaanggota', '', '200'), '', ' placeholder="idketua" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('keterangan', 'keterangan', form_textarea(getArrayObj('edketerangan', '', '400'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('edidprovinsi', 'Provinsi', form_dropdown_('edidprovinsi', $this->modelprovinsi->getArraylistprovinsi(), '', ' class="form-control" id="edidprovinsi" onchange="provinsi();" required="required"'), '');
        $kabupaten = setForm('edkabupaten', 'kabupaten', form_dropdown_('edidkabupaten', $this->modelkabupaten->getArraylistkabupaten(), '', 'id="edidkabupaten" onchange="kabupaten()"')) . '<div class="spacer"></div>';
        $kecamatan = setForm('edkecamatan', 'kecamatan', form_dropdown_('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), '', 'id="edidkecamatan" onchange="kelurahan()"')) . '<div class="spacer"></div>';
        $kelurahan = setForm('edidkelurahan', 'kelurahan', form_dropdown_('edidkelurahan', $this->modelkelurahan->getArraylistkelurahan(), '', 'id="edidkelurahan" ')) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="kabupaten">' . $kabupaten . '</div>';
        $xBufResult .= '<div id="kecamatan">' . $kecamatan . '</div>';
        $xBufResult .= '<div id="kelurahan">' . $kelurahan . '</div>';

        $xBufResult .= setForm('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('urut', 'urut', form_input_(getArrayObj('edurut', '', '200'), '', ' placeholder="urut" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpantps();"') . form_button('btNew', 'new', 'onclick="doCleartps();"') . '<div class="spacer"></div><div id="tabledatatps">' . $this->getlisttps(0, '') . '</div><div class="spacer"></div>';

        return $xBufResult;
    }

    function getlisttps($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('tps', '', 'data-field="tps" data-sortable="true" width=10%') .
                tbaddcellhead('urut', '', 'data-field="urut" data-sortable="true" width=10%') .
                tbaddcellhead('idketua', '', 'data-field="idketua" data-sortable="true" width=10%') .
                tbaddcellhead('keterangan', '', 'data-field="keterangan" data-sortable="true" width=10%') .
                tbaddcellhead('idkecamatan', '', 'data-field="idkecamatan" data-sortable="true" width=10%') .
                tbaddcellhead('idkelurahan', '', 'data-field="idkelurahan" data-sortable="true" width=10%') .
                tbaddcellhead('idkabupaten', '', 'data-field="idkabupaten" data-sortable="true" width=10%') .
                tbaddcellhead('idprovinsi', '', 'data-field="idprovinsi" data-sortable="true" width=10%') .
                tbaddcellhead('iddapil', '', 'data-field="iddapil" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modeltps');
        $xQuery = $this->modeltps->getListtps($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doedittps(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapustps(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->tps) .
                    tbaddcell($row->urut) .
                    tbaddcell($row->idketua) .
                    tbaddcell($row->keterangan) .
                    tbaddcell($row->idkecamatan) .
                    tbaddcell($row->idkelurahan) .
                    tbaddcell($row->idkabupaten) .
                    tbaddcell($row->idprovinsi) .
                    tbaddcell($row->iddapil) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchtps(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchtps(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchtps(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlisttpsAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['tps'] = "";
        $this->json_data['urut'] = "";
        $this->json_data['idketua'] = "";
        $this->json_data['keterangan'] = "";
        $this->json_data['idkecamatan'] = "";
        $this->json_data['idkelurahan'] = "";
        $this->json_data['idkabupaten'] = "";
        $this->json_data['idprovinsi'] = "";
        $this->json_data['iddapil'] = "";
        $this->json_data['idtahun'] = "";

        $response = array();
        $this->load->model('modeltps');
        $xQuery = $this->modeltps->getListtps($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['tps'] = $row->tps;
            $this->json_data['urut'] = $row->urut;
            $this->json_data['idketua'] = $row->idketua;
            $this->json_data['keterangan'] = $row->keterangan;
            $this->json_data['idkecamatan'] = $row->idkecamatan;
            $this->json_data['idkelurahan'] = $row->idkelurahan;
            $this->json_data['idkabupaten'] = $row->idkabupaten;
            $this->json_data['idprovinsi'] = $row->idprovinsi;
            $this->json_data['iddapil'] = $row->iddapil;
            $this->json_data['idtahun'] = $row->idtahun;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpantpsAndroid() {
        $xidx = $_POST['edidx'];
        $xtps = $_POST['edtps'];
        $xurut = $_POST['edurut'];
        $xidketua = $_POST['edidketua'];
        $xketerangan = $_POST['edketerangan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xiddapil = $_POST['ediddapil'];
        $xidtahun = $_POST['edidtahun'];

        $this->load->helper('json');
        $this->load->model('modeltps');
        $response = array();
        if ($xidx != '0') {
            $this->modeltps->setUpdatetps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun);
        } else {
            $this->modeltps->setInserttps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun);
        }
        $row = $this->modeltps->getLastIndextps();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['tps'] = $row->tps;
        $this->json_data['urut'] = $row->urut;
        $this->json_data['idketua'] = $row->idketua;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtahun'] = $row->idtahun;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrectps() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeltps');
        $row = $this->modeltps->getDetailtps($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['tps'] = $row->tps;
        $this->json_data['urut'] = $row->urut;
        $this->json_data['idketua'] = $row->idketua;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtahun'] = $row->idtahun;

        echo json_encode($this->json_data);
    }

    function deletetabletps() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeltps');
        $this->modeltps->setDeletetps($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchtps() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatatps'] = $this->getlisttps($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpantps() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xtps = $_POST['edtps'];
        $xurut = $_POST['edurut'];
        $xidketua = $_POST['edidketua'];
        $xketerangan = $_POST['edketerangan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xiddapil = $_POST['ediddapil'];
        $xidtahun = $_POST['edidtahun'];

        $this->load->model('modeltps');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeltps->setUpdatetps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun);
            } else {
                $xStr = $this->modeltps->setInserttps($xidx, $xtps, $xurut, $xidketua, $xketerangan, $xidkecamatan, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xiddapil, $xidtahun);
            }
        }
        echo json_encode(null);
    }

}

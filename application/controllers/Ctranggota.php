<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : anggota   *  By Diar */

class Ctranggota extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformanggota('0', $xAwal);
    }

    function createformanggota($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxadmin.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxanggota.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormanggota($xidx), '', '', $xAddJs, '', 'anggota');
    }

    function setDetailFormanggota($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctranggota/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelpendidikan');
        $this->load->model('modelpekerjaan');
        $this->load->model('modeljabatan');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modeltahun');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('noktp', 'noktp', form_input_(getArrayObj('ednoktp', '', '200'), '', ' placeholder="noktp" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('namalengkap', 'namalengkap', form_input_(getArrayObj('ednamalengkap', '', '200'), '', ' placeholder="namalengkap" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('jeniskelamin', 'jeniskelamin', form_dropdown_('edjeniskelamin', getArraygender(), '', ' id="edjeniskelamin" placeholder="jeniskelamin" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpendidikan', 'idpendidikan', form_dropdown_('edidpendidikan', $this->modelpendidikan->getArraylistpendidikan(), '', ' id="edidpendidikan" placeholder="idpendidikan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpekerjaan', 'idpekerjaan', form_dropdown_('edidpekerjaan', $this->modelpekerjaan->getArraylistpekerjaan(), '', ' id="edidpekerjaan" placeholder="idpekerjaan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idjabatan', 'idjabatan', form_dropdown_('edidjabatan', $this->modeljabatan->getArraylistjabatan(), '', ' placeholder="idjabatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('alamat', 'alamat', form_input_(getArrayObj('edalamat', '', '400'), '', ' placeholder="alamat" ')) . '<div class="spacer"></div>';

//$xBufResult .= setForm('idkelurahan','idkelurahan',form_input_(getArrayObj('edidkelurahan','','200'),'',' placeholder="idkelurahan" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idkecamatan','idkecamatan',form_input_(getArrayObj('edidkecamatan','','200'),'',' placeholder="idkecamatan" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idkabupaten','idkabupaten',form_input_(getArrayObj('edidkabupaten','','200'),'',' placeholder="idkabupaten" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idprovinsi','idprovinsi',form_input_(getArrayObj('edidprovinsi','','200'),'',' placeholder="idprovinsi" ')).'<div class="spacer"></div>';
        $xBufResult .= setForm('edidprovinsi', 'Provinsi', form_dropdown_('edidprovinsi', $this->modelprovinsi->getArraylistprovinsi(), '', ' class="form-control" id="edidprovinsi" onchange="provinsi();" required="required"'), '');
        $kabupaten = setForm('edkabupaten', 'kabupaten', form_dropdown_('edidkabupaten', $this->modelkabupaten->getArraylistkabupaten(), '', 'id="edidkabupaten" onchange="kabupaten()"')) . '<div class="spacer"></div>';
        $kecamatan = setForm('edkecamatan', 'kecamatan', form_dropdown_('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), '', 'id="edidkecamatan" onchange="kelurahan()"')) . '<div class="spacer"></div>';
        $kelurahan = setForm('edidkelurahan', 'kelurahan', form_dropdown_('edidkelurahan', $this->modelkelurahan->getArraylistkelurahan(), '', 'id="edidkelurahan" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div id="kabupaten">' . $kabupaten . '</div>';

        $xBufResult .= '<div id="kecamatan">' . $kecamatan . '</div>';

        $xBufResult .= '<div id="kelurahan">' . $kelurahan . '</div>';

        $xBufResult .= setForm('notelpon', 'notelpon', form_input_(getArrayObj('ednotelpon', '', '200'), '', ' placeholder="notelpon" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('username', 'username', form_input_(getArrayObj('edusername', '', '200'), '', ' placeholder="username" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('password', 'password', form_input_(getArrayObj('edpassword', '', '200'), '', ' placeholder="password" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('isaktif', 'isaktif', form_dropdown_('edisaktif', getArrayYaTidak(), '', ' id="edisaktif" placeholder="isaktif" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpananggota();"') . form_button('btNew', 'new', 'onclick="doClearanggota();"') . '<div class="spacer"></div><div id="tabledataanggota">' . $this->getlistanggota(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistanggota($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('noktp', '', 'data-field="noktp" data-sortable="true" width=10%') .
                tbaddcellhead('namalengkap', '', 'data-field="namalengkap" data-sortable="true" width=10%') .
                tbaddcellhead('jeniskelamin', '', 'data-field="jeniskelamin" data-sortable="true" width=10%') .
                tbaddcellhead('idpendidikan', '', 'data-field="idpendidikan" data-sortable="true" width=10%') .
                tbaddcellhead('idpekerjaan', '', 'data-field="idpekerjaan" data-sortable="true" width=10%') .
                tbaddcellhead('idjabatan', '', 'data-field="idjabatan" data-sortable="true" width=10%') .
                tbaddcellhead('alamat', '', 'data-field="alamat" data-sortable="true" width=10%') .
                tbaddcellhead('idkelurahan', '', 'data-field="idkelurahan" data-sortable="true" width=10%') .
                tbaddcellhead('idkecamatan', '', 'data-field="idkecamatan" data-sortable="true" width=10%') .
                tbaddcellhead('idkabupaten', '', 'data-field="idkabupaten" data-sortable="true" width=10%') .
                tbaddcellhead('idprovinsi', '', 'data-field="idprovinsi" data-sortable="true" width=10%') .
                tbaddcellhead('notelpon', '', 'data-field="notelpon" data-sortable="true" width=10%') .
                tbaddcellhead('username', '', 'data-field="username" data-sortable="true" width=10%') .
                tbaddcellhead('password', '', 'data-field="password" data-sortable="true" width=10%') .
                tbaddcellhead('iddapil', '', 'data-field="iddapil" data-sortable="true" width=10%') .
                tbaddcellhead('idtps', '', 'data-field="idtps" data-sortable="true" width=10%') .
                tbaddcellhead('idpartai', '', 'data-field="idpartai" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('isaktif', '', 'data-field="isaktif" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modelanggota');
        $xQuery = $this->modelanggota->getListanggota($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditanggota(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fas fa-trash btn" aria-hidden="true" onclick = "dohapusanggota(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->noktp) .
                    tbaddcell($row->namalengkap) .
                    tbaddcell($row->jeniskelamin) .
                    tbaddcell($row->idpendidikan) .
                    tbaddcell($row->idpekerjaan) .
                    tbaddcell($row->idjabatan) .
                    tbaddcell($row->alamat) .
                    tbaddcell($row->idkelurahan) .
                    tbaddcell($row->idkecamatan) .
                    tbaddcell($row->idkabupaten) .
                    tbaddcell($row->idprovinsi) .
                    tbaddcell($row->notelpon) .
                    tbaddcell($row->username) .
                    tbaddcell($row->password) .
                    tbaddcell($row->iddapil) .
                    tbaddcell($row->idtps) .
                    tbaddcell($row->idpartai) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($row->isaktif) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchanggota(0);"><i class="fas fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchanggota(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchanggota(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistanggotaAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['noktp'] = "";
        $this->json_data['namalengkap'] = "";
        $this->json_data['jeniskelamin'] = "";
        $this->json_data['idpendidikan'] = "";
        $this->json_data['idpekerjaan'] = "";
        $this->json_data['idjabatan'] = "";
        $this->json_data['alamat'] = "";
        $this->json_data['idkelurahan'] = "";
        $this->json_data['idkecamatan'] = "";
        $this->json_data['idkabupaten'] = "";
        $this->json_data['idprovinsi'] = "";
        $this->json_data['notelpon'] = "";
        $this->json_data['username'] = "";
        $this->json_data['password'] = "";
        $this->json_data['iddapil'] = "";
        $this->json_data['idtps'] = "";
        $this->json_data['idpartai'] = "";
        $this->json_data['idtahun'] = "";
        $this->json_data['isaktif'] = "";

        $response = array();
        $this->load->model('modelanggota');
        $xQuery = $this->modelanggota->getListanggota($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['noktp'] = $row->noktp;
            $this->json_data['namalengkap'] = $row->namalengkap;
            $this->json_data['jeniskelamin'] = $row->jeniskelamin;
            $this->json_data['idpendidikan'] = $row->idpendidikan;
            $this->json_data['idpekerjaan'] = $row->idpekerjaan;
            $this->json_data['idjabatan'] = $row->idjabatan;
            $this->json_data['alamat'] = $row->alamat;
            $this->json_data['idkelurahan'] = $row->idkelurahan;
            $this->json_data['idkecamatan'] = $row->idkecamatan;
            $this->json_data['idkabupaten'] = $row->idkabupaten;
            $this->json_data['idprovinsi'] = $row->idprovinsi;
            $this->json_data['notelpon'] = $row->notelpon;
            $this->json_data['username'] = $row->username;
            $this->json_data['password'] = $row->password;
            $this->json_data['iddapil'] = $row->iddapil;
            $this->json_data['idtps'] = $row->idtps;
            $this->json_data['idpartai'] = $row->idpartai;
            $this->json_data['idtahun'] = $row->idtahun;
            $this->json_data['isaktif'] = $row->isaktif;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpananggotaAndroid() {
        $xidx = $_POST['edidx'];
        $xnoktp = $_POST['ednoktp'];
        $xnamalengkap = $_POST['ednamalengkap'];
        $xjeniskelamin = $_POST['edjeniskelamin'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xalamat = $_POST['edalamat'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xnotelpon = $_POST['ednotelpon'];
        $xusername = $_POST['edusername'];
        $xpassword = $_POST['edpassword'];
        $xiddapil = $_POST['ediddapil'];
        $xidtps = $_POST['edidtps'];
        $xidpartai = $_POST['edidpartai'];
        $xidtahun = $_POST['edidtahun'];
        $xisaktif = $_POST['edisaktif'];

        $this->load->helper('json');
        $this->load->model('modelanggota');
        $response = array();
        if ($xidx != '0') {
            $this->modelanggota->setUpdateanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif);
        } else {
            $this->modelanggota->setInsertanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif);
        }
        $row = $this->modelanggota->getLastIndexanggota();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['noktp'] = $row->noktp;
        $this->json_data['namalengkap'] = $row->namalengkap;
        $this->json_data['jeniskelamin'] = $row->jeniskelamin;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['notelpon'] = $row->notelpon;
        $this->json_data['username'] = $row->username;
        $this->json_data['password'] = $row->password;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['isaktif'] = $row->isaktif;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecanggota() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelanggota');
        $row = $this->modelanggota->getDetailanggota($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['noktp'] = $row->noktp;
        $this->json_data['namalengkap'] = $row->namalengkap;
        $this->json_data['jeniskelamin'] = $row->jeniskelamin;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['notelpon'] = $row->notelpon;
        $this->json_data['username'] = $row->username;
        $this->json_data['password'] = $row->password;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['isaktif'] = $row->isaktif;

        echo json_encode($this->json_data);
    }

    function deletetableanggota() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelanggota');
        $this->modelanggota->setDeleteanggota($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchanggota() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledataanggota'] = $this->getlistanggota($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function getautocomplateanggota() {
        $namaanggota = $_POST['ednamaanggota'];
        $this->load->helper('json');
        $this->load->model('modelanggota');
        $query = $this->modelanggota->getListanggota(0, 10, $namaanggota);
        $xdata = array();
        foreach ($query->result() as $row) {
            $xdata[] = array('label' => $row->namalengkap, 'value' => $row->idx);
        }
        $this->json_data['data'] = $xdata;
        echo json_encode($this->json_data);
    }

    function simpananggota() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xnoktp = $_POST['ednoktp'];
        $xnamalengkap = $_POST['ednamalengkap'];
        $xjeniskelamin = $_POST['edjeniskelamin'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xalamat = $_POST['edalamat'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xnotelpon = $_POST['ednotelpon'];
        $xusername = $_POST['edusername'];
        $xpassword = $_POST['edpassword'];
        $xiddapil = $_POST['ediddapil'];
        $xidtps = $_POST['edidtps'];
        $xidpartai = $_POST['edidpartai'];
        $xidtahun = $_POST['edidtahun'];
        $xisaktif = $_POST['edisaktif'];

        $this->load->model('modelanggota');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelanggota->setUpdateanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif);
            } else {
                $xStr = $this->modelanggota->setInsertanggota($xidx, $xnoktp, $xnamalengkap, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xidkelurahan, $xidkecamatan, $xidkabupaten, $xidprovinsi, $xnotelpon, $xusername, $xpassword, $xiddapil, $xidtps, $xidpartai, $xidtahun, $xisaktif);
            }
        }
        echo json_encode(null);
    }

}

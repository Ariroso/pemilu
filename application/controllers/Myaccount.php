<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : asalsekolah   *  By Diar */;

class Myaccount extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        $this->load->model('basemodel');
    }

    public function index() {
        $xtanggal = date('Y-m-d'); // Hasil: 20-01-2017 05:32:15
//        $this->load->model('modelitembayar');
//        $this->load->model('modelpendaftaran');
//        $this->load->model('modelgelombang');
        $this->load->model('modelanggota');
        $idanggota = $this->session->userdata('idanggota');
//        $beliform = $this->modelitembayar->getBayarformbyidsiswa($idsiswa, 1);
//        $pendaftaran = $this->modelgelombang->getpendaftaranbuka($xtanggal);

        if (empty($idanggota)) {
            redirect(site_url(), '');
        }
//        $pendaftaran = $this->modelitembayar->getitembayarformbyidsiswa($idsiswa, 1)->num_rows();
//        if ($pendaftaran > 0) {
//            $password = $this->session->userdata('password');
//            $user = $this->session->userdata('user');
//            $nama = $this->session->userdata('nama');
//            $data['maincontent'] = '
//                  <div class=" bg-regina">
//                  </div>
//                  ';
//            if ($beliform <= 0) {
//                $data['maincontent'] .= $this->detailBiaya($idsiswa);
//            } else {
        $row = $this->modelanggota->getDetailanggota($idanggota);
        $data['maincontent'] = $this->createformsiswa($idanggota, $row->namalengkap, $row->password, $row->username);
//            }
        $data['header'] = $this->basemodel->header(
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/ui/jquery-ui.js"> </script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
//                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
//                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
//                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
//                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myupload.js"></script>' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxmyaccount.js"> </script>');
        $data['slider'] = '';
        $data['footer'] = $this->basemodel->footer();
        $this->load->view('scriptmedia/index', $data);
    }

    function slider() {
        $this->load->model('modelimgslide');
        $this->load->model('basemodel');
        $qslider = $this->modelimgslide->getListimgslide(0, 10);
        $data = [];
        foreach ($qslider->result() as $row) {
            $data['qslider'][] = array(
                'image' => (!empty($row->url)) ? $this->basemodel->showimage($row->url, 'slide') : '',
                'keterangan' => $row->keterangan,
                'link' => $row->link
            );
        }
        return $this->load->view("scriptmedia/slider", $data, TRUE);
    }

    /*
     * Form Core Start
     */

    function createformdaftarsiswa($idanggota) {
        $this->load->helper('common');
        $this->load->model('modelgelombang');
        $xidgelombang = $this->session->userdata('idgelombang');
        $tahun = $this->session->userdata('tahunajar');
        $xketerangan = $this->modelgelombang->getDetailgelombang($xidgelombang);
        $xBufResult = '';
        $xBufResult .= '<div id="form">';
        $xBufResult .= '<div class="container daftar py-2 py-lg-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Pendaftaran PPDB Online ' . $tahun . ' Reguler ' . $xketerangan->gelombang . ' DIBUKA</h3>'
                . '<div class="text-muted">Waktu Pendaftaran ' . $xketerangan->gelombang
                . '  dimulai <br> dari tanggal ' . datetomysql($xketerangan->tanggalmulai) . ' sampai tanggal ' . datetomysql($xketerangan->tanggalakhir) . ' '
                . '</div><hr>'
                . '</div>'
                . '</div>';

        $xBufResult .= '<input type="hidden" id="edidsiswa" name="edidsiswa" value="' . $idanggota . '">';
        $xBufResult .= '<div class="form-group row mt-4">'
                . '<div class="col text-center">'
                . '<p>Sudah yakin ingin mendaftar di SMA Regina pacis?<br>Jika sudah yakin silahkan klik tombol daftar dibawah ini:</p><button type="submit" class="btn btn-lg btn-ursuline px-5 my-2" onclick="dosimpanbeliform()">'
                . 'Daftar'
                . '</button>'
                . '</div>'
                . '</div></form>';
        $xBufResult .= '</div></div>';
        return $xBufResult;
    }

    function createformsiswa($idanggota, $nama, $password, $user) {
        $this->load->helper('form');
        $this->load->helper('html');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctranggota/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelpendidikan');
        $this->load->model('modelpekerjaan');
        $this->load->model('modeljabatan');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modeltahun');
        $rowanggota = $this->modelanggota->getDetailanggota($idanggota);
        
        $xBufResult = '';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setFormfrontend('noktp', 'noktp', form_input_(getArrayObj('ednoktp', $rowanggota->noktp, ''), '', ' placeholder="noktp" disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('namalengkap', 'namalengkap', form_input_(getArrayObj('ednamalengkap', $rowanggota->namalengkap, ''), '', ' placeholder="namalengkap" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('jeniskelamin', 'jeniskelamin', form_dropdown_('edjeniskelamin', getArraygender(), $rowanggota->jeniskelamin, ' id="edjeniskelamin" placeholder="jeniskelamin" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idpendidikan', 'idpendidikan', form_dropdown_('edidpendidikan', $this->modelpendidikan->getArraylistpendidikan(), $rowanggota->idpendidikan, ' id="edidpendidikan" placeholder="idpendidikan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idpekerjaan', 'idpekerjaan', form_dropdown_('edidpekerjaan', $this->modelpekerjaan->getArraylistpekerjaan(), $rowanggota->idpekerjaan, ' id="edidpekerjaan" placeholder="idpekerjaan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idjabatan', 'idjabatan', form_dropdown_('edidjabatan', $this->modeljabatan->getArraylistjabatan(), $rowanggota->idjabatan, ' placeholder="idjabatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('alamat', 'alamat', form_input_(getArrayObj('edalamat', $rowanggota->alamat, ''), '', ' placeholder="alamat" ')) . '<div class="spacer"></div>';

//$xBufResult .= setForm('idkelurahan','idkelurahan',form_input_(getArrayObj('edidkelurahan','','200'),'',' placeholder="idkelurahan" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idkecamatan','idkecamatan',form_input_(getArrayObj('edidkecamatan','','200'),'',' placeholder="idkecamatan" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idkabupaten','idkabupaten',form_input_(getArrayObj('edidkabupaten','','200'),'',' placeholder="idkabupaten" ')).'<div class="spacer"></div>';
//
//$xBufResult .= setForm('idprovinsi','idprovinsi',form_input_(getArrayObj('edidprovinsi','','200'),'',' placeholder="idprovinsi" ')).'<div class="spacer"></div>';
        $xBufResult .= setFormfrontend('edidprovinsi', 'Provinsi', form_dropdown_('edidprovinsi', $this->modelprovinsi->getArraylistprovinsi(), $rowanggota->idprovinsi, ' class="form-control" id="edidprovinsi" onchange="provinsi2();" required="required"'), '');
        $kabupaten = setFormfrontend('edkabupaten', 'kabupaten', form_dropdown_('edidkabupaten', $this->modelkabupaten->getArraylistkabupaten(), $rowanggota->idkabupaten, 'id="edidkabupaten" onchange="kabupaten()"')) . '<div class="spacer"></div>';
        $kecamatan = setFormfrontend('edkecamatan', 'kecamatan', form_dropdown_('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), $rowanggota->idkecamatan, 'id="edidkecamatan" onchange="kelurahan()"')) . '<div class="spacer"></div>';
        $kelurahan = setFormfrontend('edidkelurahan', 'kelurahan', form_dropdown_('edidkelurahan', $this->modelkelurahan->getArraylistkelurahan(), '', 'id="edidkelurahan" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div id="kabupaten">' . $kabupaten . '</div>';

        $xBufResult .= '<div id="kecamatan">' . $kecamatan . '</div>';

        $xBufResult .= '<div id="kelurahan">' . $kelurahan . '</div>';

        $xBufResult .= setFormfrontend('notelpon', 'notelpon', form_input_(getArrayObj('ednotelpon', '', ''), '', ' placeholder="notelpon" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('username', 'username', form_input_(getArrayObj('edusername', '', ''), '', ' placeholder="username" disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('password', 'password', form_input_frontpassword_(getArrayObj('edpassword', '', ''), '', ' placeholder="password" ')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil"  disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps"  disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai"  disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun"  disabled')) . '<div class="spacer"></div>';

        $xBufResult .= setFormfrontend('isaktif', 'isaktif', form_dropdown_('edisaktif', getArrayYaTidak(), '', ' id="edisaktif" placeholder="isaktif"  disabled')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpananggota();" class="btn btn-success col-md-2 offset-md-4"') . '<div class="spacer"></div>';

        return $xBufResult;
    }

    function formprestasi2() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('json');
        $this->load->helper('html');
        $this->load->helper('common');
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Data Prestasi </h3>'
                . '<p class="text-muted light">'
                . 'Prestasi di bidang akademik dan non-akademik untuk Juara I, II, dan III.<br>Data ini digunakan untuk pengembangan siswa.'
                . '</p><hr><p class="text-muted"><i class="fa fa-info-circle"></i> Isi data kemudian klik tombol <strong>Tambahkan</strong> untuk menyimpan.<br>Klik tombol <strong>Lanjut</strong> untuk melewati formulir ini.</p>'
                . '</div>'
                . '</div>';
        $xBufResult .= form_open_multipart('ctrsiswa/inserttable', array('id' => 'form', 'name' => 'form'));
        $xBufResult .= '<form>';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="" />';
        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $idsiswa . '" />';

        $xBufResult .= setFormfrontend('ednamalomba', 'Nama Lomba', form_input_frontend_(getArrayObj('ednamalomba', '', ''), '', 'placeholder="Nama Lomba" required="required"'));
        $xBufResult .= setFormfrontend('edpenyelenggara', 'Penyelenggara', form_input_frontend_(getArrayObj('edpenyelenggara', '', ''), '', 'placeholder="Penyelenggara Lomba" required="required"'));
        $xBufResult .= setFormfrontend('edtingkat', 'Tingkat', form_input_frontend_(getArrayObj('edtingkat', '', ''), '', 'placeholder="Tingkat Provinsi, Kabupaten, dsb." required="required"'));
        $xBufResult .= setFormfrontend('edprestasi', 'Prestasi', form_input_frontend_(getArrayObj('edprestasi', '', ''), '', 'placeholder="Prestasi yang pernah diraih" required="required"'));
        $xBufResult .= setFormfrontend('sertifikat', 'Sertifikat *<br><span class="small text-muted">Unggah sertifikat/piagam penghargaan</span>', '<div id="uploadarea" style="width:150px;height:150px">' . form_input_frontend_(getArrayObj('edurl', '', '200'), '', 'alt="Unggah" class="form-control" required="required"') . '</div>');
        $xBufResult .= '<div class="form-group row mt-5">'
                . '<div class="col text-center">'
                . '<button type="submit" class="btn btn-ursuline px-5" onclick="dosimpanprestasibysiswa()">'
                . 'Tambahkan'
                . '</button> &nbsp'
                . '</div>'
                . '</div>';
        $xBufResult .= '<div id="tabledatarapot">' .
                $this->getlistprestasi($idsiswa)
                . '</div><div class="spacer"></div>';
        $xBufResult .= '<div class="form-group row">'
                . '<div class="col text-center mt-5">'
                . '<button type="button" class="btn btn-lg btn-ursuline px-5" onclick="formayah()">'
                . 'Lanjut'
                . '</button> &nbsp'
//                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="formpilihjurusan()">'
//                . 'Skip(lewati)'
//                . '</button>'
                . '</div>'
                . '</div>';
        $xBufResult .= '</form>';
        $xBufResult .= '</div>';
        $this->json_data['formprestasi'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function getlistprestasi($xSearch) {
        $idsiswa = $this->session->userdata('idsiswa');
        $xLimit = 10;
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('No', '', 'data-field="idx" data-sortable="true"  align="center"') .
                tbaddcellhead('Nama Lomba', '', 'data-field="lomba" data-sortable="true" align="center"') .
                tbaddcellhead('Penyelengggara', '', 'data-field="penyelenggara" data-sortable="true" align="center"') .
                tbaddcellhead('Tingkat', '', 'data-field="tingkat" data-sortable="true" align="center"') .
                tbaddcellhead('Prestasi', '', 'data-field="prestasi" data-sortable="true"  align="center"') .
                tbaddcellhead('Piagam', '', 'data-field="piagam" data-sortable="true"  align="center"') .
                tbaddcellhead('Aksi', '', 'data-field="aksi" data-sortable="true"  align="center"')
                , '', TRUE);
        $this->load->model('modelprestasi');

//        $xQuery = $this->modelrapot->getListrapotbypendaftaranawal($xAwal, 100, $xSearch, $xgelombang, $xkelas, $xsemester, $idsiswa, $kode);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        $i = 1;
        $query = $this->modelprestasi->getListprestasibysiswa($xSearch);
        // if(!empty($query->result())){

        foreach ($query->result() as $row) {

            $xButtonHapus = '<label class="btn btn-sm btn-danger" onclick = "dohapusprestasi(\'' . $row->idx . '\');"> Hapus</label>';
            $xbufResult .= tbaddrow(tbaddcell($i++) .
                    tbaddcell($row->namalomba) .
                    tbaddcell($row->penyelenggara) .
                    tbaddcell($row->tingkat) .
                    tbaddcell($row->prestasi) .
                    tbaddcell('<img src="' . base_url() . 'resource/uploaded/img/' . $row->foto . '" style="border: solid;width: 70px; height: 80px; align:center;">') .
                    tbaddcell($xButtonHapus)
            );
        }
        //     }

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table"');
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive" style="left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="submit" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function createformayahsiswa() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->helper('common');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modelagama');
        $this->load->model('modelorangtua');
        $this->load->model('modelpekerjaan');
        $rowortuayah = $this->modelorangtua->getListorangtuaayahsiswadetail(0, 10, $idsiswa);
        $kabupaten = setFormfrontend('edidkabupatenayah', 'Kabupaten*', form_dropdown('edidkabupatenayah', array('--Kabupaten--'), '', ' class="form-control" required="required" id="edidkabupatenayah" disabled="disabled"'));
        $kecamatan = setFormfrontend('edidkecamatanayah', 'Kecamatan*', form_dropdown('edidkecamatanayah', array('--Kecamatan--'), '', ' class="form-control" required="required" id="edidkecamatanayah" disabled="disabled"'));
        if (!empty($rowortuayah->idkabupaten)) {
            $kabupaten = setFormfrontend('edidkabupatenayah', 'Kabupaten*', form_dropdown('edidkabupatenayah', $this->modelkabupaten->getListkabupatenbyprovinsi(@$rowortuayah->idprovinsi), $rowortuayah->idkabupaten, ' class="form-control" required="required" id="edidkabupatenayah" disabled="disabled"'));
        }
        if (!empty($rowortuayah->idkabupaten)) {
            $kecamatan = setFormfrontend('edidkecamatanayah', 'Kabupaten*', form_dropdown('edidkecamatanayah', $this->modelkecamatan->getListkecamatanbykabupaten(@$rowortuayah->idkabupaten), $rowortuayah->idkecamatan, ' class="form-control" required="required" id="edidkecamatanayah" disabled="disabled"'));
        }
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Data Ayah </h3>'
                . '<p class="text-muted">'
                . '<span class="light">Silahkan isi formulir Data Ayah berikut ini:</span><br><i class="fa fa-info-circle"></i> Isian data bertanda * wajib di isi'
                . '</p><hr>'
                . '</div>'
                . '</div>';
        $xBufResult .= form_open_multipart('ctrsiswa/inserttable', array('id' => 'form', 'name' => 'form'));
        $xBufResult .= '<form>';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="' . @$rowortuayah->idx . '" />';

        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $idsiswa . '" />';

        $xBufResult .= '<input type="hidden" name="edayahibuayah" id="edayahibuayah" value="A" />';

        $xBufResult .= setFormfrontend('edstatushidupayah', 'Status Hidup *', form_dropdown('edstatushidupayah', getArraystatus(), @$rowortuayah->statushidup, ' class="form-control" id="edstatushidupayah" onchange="cekhiduportuayah()" required="required"'));
        $xBufResult .= '<div id="cekhidupayah">';

        $xBufResult .= setFormfrontend('ednamalengkapayah', 'Nama Ayah *', form_input_frontend_(getArrayObj('ednamalengkapayah', @$rowortuayah->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

        $xBufResult .= setFormfrontend('edtempatlahirayah', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahirayah', @$rowortuayah->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

        $xBufResult .= setFormfrontend('edtanggallahirayah', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahirayah', datetomysql(@$rowortuayah->tanggallahir), ''), '', ' class="tanggal form-control" placeholder=" Tanggal Lahir Format : DD-MM-YYYY" required="required"'));
        if (empty($rowortuayah->idx) || $rowortuayah->statushidup == "Hidup") {
            $xBufResult .= setFormfrontend('edagamaayah', 'Agama *', form_dropdown('edagamaayah', $this->modelagama->getArrayListagama(), @$rowortuayah->agama, ' class="form-control" id="edagamaayah" required="required"'));

            $xBufResult .= setFormfrontend('edkewarganegaraanayah', 'Kewarganegaraan *', form_dropdown('edkewarganegaraanayah', getArraywn(), @$rowortuayah->kewarganegaraan, ' class="form-control" id="edkewarganegaraanayah" required="required" '));

            $xBufResult .= setFormfrontend('edpendidikanterakhir', 'Pendidikan Terakhir *', form_dropdown('edpendidikanterakhirayah', getArrayschool(), @$rowortuayah->pendidikanterakhir, ' class="form-control" id="edpendidikanterakhirayah" required="required"'));

            $xBufResult .= setFormfrontend('edpekerjaanayah', 'Pekerjaan', form_dropdown('edpekerjaanayah', $this->modelpekerjaan->getArrayListpekerjaan(), @$rowortuayah->pekerjaan, ' class="form-control" id="edpekerjaanayah" onchange="kerjalain()" required="required"'));

            $xBufResult .= '<div id="kerjalain"></div>';

            $xBufResult .= setFormfrontend('edpangkatjabatanayah', 'Pangkat/Jabatan', form_input_frontend_(getArrayObj('edpangkatjabatanayah', @$rowortuayah->pangkatjabatan, ''), '', 'placeholder="Pangkat/Jabatan" '));

            $xBufResult .= setFormfrontend('edpenghasilanperbulanayah', 'Penghasilan per-Bulan *', form_dropdown('edpenghasilanperbulanayah', getArraypenghasilan(), @$rowortuayah->penghasilanperbulan, ' class="form-control" id="edpenghasilanperbulanayah" required="required"'));

            $xBufResult .= setFormfrontend('ednamakantorayah', 'Nama Kantor', form_input_frontend_(getArrayObj('ednamakantorayah', @$rowortuayah->namakantor, ''), '', 'placeholder="Nama Kantor" '));

            $xBufResult .= setFormfrontend('edalamatkantorayah', 'Alamat Kantor', form_input_frontend_(getArrayObj('edalamatkantorayah', @$rowortuayah->alamatkantor, ''), '', 'placeholder="Alamat Kantor" '));

            $xBufResult .= setFormfrontend('edtelpkantorayah', 'Nomor Telepon Kantor', form_input_frontend_(getArrayObj('edtelpkantorayah', @$rowortuayah->telpkantor, ''), '', 'placeholder="Nomor Telepon Kantor" '));

            $xBufResult .= setFormfrontend('edalamatrumahayah', 'Alamat Rumah *', form_input_frontend_(getArrayObj('edalamatrumahayah', @$rowortuayah->alamatrumah, ''), '', 'placeholder="Alamat Rumah" required="required"'));

            $xBufResult .= setFormfrontend('edidprovinsiayah', 'Provinsi*', form_dropdown('edidprovinsiayah', $this->modelprovinsi->getArraylistprovinsi(), @$rowortuayah->idprovinsi, ' class="form-control" id="edidprovinsiayah" onchange="provinsiortuayahbysiswa()" required="required"'));

            $xBufResult .= '<div id="kabupatenayah">' . $kabupaten . '</div>';

            $xBufResult .= '<div id="kecamatanayah">' . $kecamatan . '</div>';

            $xBufResult .= setFormfrontend('ednohpayah', 'No HP*', form_input_frontend_(getArrayObj('ednohpayah', @$rowortuayah->nohp, ''), '', 'placeholder=" NO Hp Format : 628xxxxxx" required="required"'));

            $xBufResult .= setFormfrontend('edstatusorangtuaayah', 'Status Ayah *', form_dropdown('edstatusorangtuaayah', getArraystatusortu(), @$rowortuayah->statusorangtua, ' class="form-control" required="required" id="edstatusorangtuaayah"'));

            $xBufResult .= setFormfrontend('edstatusperkawinanayah', 'Status Perkawinan *', form_dropdown('edstatusperkawinanayah', getArrayperkawinan(), @$rowortuayah->statusperkawinan, ' class="form-control" required="required" id="edstatusperkawinanayah"'));
        }
        $xBufResult .= '</div>';
        $xBufResult .= '<div class="form-group row mt-5">'
                . '<div class="col text-center">'
                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanorangtuaayahbysiswa();">'
                . 'Lanjut'
                . '</button>'
                . '</div>'
                . '</div></form>';
        $xBufResult .= '</div>';
        $this->json_data['formayah'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function createformibusiswa() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->helper('common');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modelagama');
        $this->load->model('modelorangtua');
        $this->load->model('modelpekerjaan');
        $rowortuibu = $this->modelorangtua->getListorangtuaibusiswadetail(0, 10, $idsiswa);
        $kabupaten = setFormfrontend('edidkabupatenibu', 'Kabupaten*', form_dropdown('edidkabupatenibu', array('--Kabupaten--'), '', ' class="form-control" required="required" id="edidkabupatenibu" disabled="disabled"'));
        $kecamatan = setFormfrontend('edidkecamatanibu', 'Kecamatan*', form_dropdown('edidkecamatanibu', array('--Kecamatan--'), '', ' class="form-control" required="required" id="edidkecamatanibu" disabled="disabled"'));
        if (!empty($rowortuibu->idkabupaten)) {
            $kabupaten = setFormfrontend('edidkabupatenibu', 'Kabupaten*', form_dropdown('edidkabupatenibu', $this->modelkabupaten->getListkabupatenbyprovinsi(@$rowortuibu->idprovinsi), $rowortuibu->idkabupaten, ' class="form-control" required="required" id="edidkabupatenibu" disabled="disabled"'));
        }
        if (!empty($rowortuibu->idkabupaten)) {
            $kecamatan = setFormfrontend('edidkecamatanibu', 'Kabupaten*', form_dropdown('edidkecamatanibu', $this->modelkecamatan->getListkecamatanbykabupaten(@$rowortuibu->idkabupaten), $rowortuibu->idkecamatan, ' class="form-control" required="required" id="edidkecamatanibu" disabled="disabled"'));
        }
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Data Ibu</h3>'
                . '<p class="text-muted">'
                . '<span class"light">Silahkan isi formulir Data Ibu berikut ini:</span><br><i class="fa fa-info-circle"></i> Isian data bertanda * wajib di isi'
                . '</p><hr>'
                . '</div>'
                . '</div>';
        $xBufResult .= '<form>';
        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $idsiswa . '" />';
        $xBufResult .= '<input type="hidden" name="edayahibuibu" id="edayahibuibu" value="I" />';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="' . @$rowortuibu->idx . '" />';
        $xBufResult .= setFormfrontend('edstatushidupibu', 'Status Hidup*', form_dropdown('edstatushidupibu', getArraystatus(), @$rowortuibu->statushidup, ' class="form-control" id="edstatushidupibu" onchange="cekhiduportuibu()" required="required"'));
        $xBufResult .= '<div id="cekhidupibu">';
        $xBufResult .= setFormfrontend('ednamalengkapibu', 'Nama Ibu *', form_input_frontend_(getArrayObj('ednamalengkapibu', @$rowortuibu->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

        $xBufResult .= setFormfrontend('edtempatlahiribu', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahiribu', @$rowortuibu->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

        $xBufResult .= setFormfrontend('edtanggallahiribu', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahiribu', datetomysql(@$rowortuibu->tanggallahir), ''), '', ' class="tanggal form-control" placeholder=" Tanggal Lahir Format : DD-MM-YYYY" required="required"'));
        if (empty(@$rowortuibu->idx) || @$rowortuibu->statushidup == "Hidup") {
            $xBufResult .= setFormfrontend('edagamaibu', 'Agama *', form_dropdown('edagamaibu', $this->modelagama->getArrayListagama(), @$rowortuibu->agama, ' class="form-control" id="edagamaibu" required="required"'));

            $xBufResult .= setFormfrontend('edkewarganegaraanibu', 'Kewarganegaraan *', form_dropdown('edkewarganegaraanibu', getArraywn(), @$rowortuibu->kewarganegaraan, ' class="form-control" id="edkewarganegaraanibu" required="required"'));

            $xBufResult .= setFormfrontend('edpendidikanterakhir', 'Pendidikan Terakhir *', form_dropdown('edpendidikanterakhiribu', getArrayschool(), @$rowortuibu->pendidikanterakhir, ' class="form-control" id="edpendidikanterakhiribu" required="required"'));

            $xBufResult .= setFormfrontend('edpekerjaanibu', 'Pekerjaan *', form_dropdown('edpekerjaanibu', $this->modelpekerjaan->getArrayListpekerjaan(), @$rowortuibu->pekerjaan, ' class="form-control" id="edpekerjaanibu" onchange="kerjalain()"'));
            $xBufResult .= '<div id="kerjalain"></div>';
            $xBufResult .= setFormfrontend('edpangkatjabatanibu', 'Pangkat/Jabatan', form_input_frontend_(getArrayObj('edpangkatjabatanibu', @$rowortuibu->pangkatjabatan, ''), '', 'placeholder="Pangkat/Jabatan" '));

            $xBufResult .= setFormfrontend('edpenghasilanperbulanibu', 'Penghasilan per-Bulan *', form_dropdown('edpenghasilanperbulanibu', getArraypenghasilan(), @$rowortuibu->penghasilanperbulan, ' class="form-control" id="edpenghasilanperbulanibu" required="required"'));

            $xBufResult .= setFormfrontend('ednamakantoribu', 'Nama Kantor', form_input_frontend_(getArrayObj('ednamakantoribu', @$rowortuibu->namakantor, ''), '', 'placeholder="Nama Kantor" '));

            $xBufResult .= setFormfrontend('edalamatkantoribu', 'Alamat Kantor', form_input_frontend_(getArrayObj('edalamatkantoribu', @$rowortuibu->alamatkantor, ''), '', 'placeholder="Alamat Kantor" '));

            $xBufResult .= setFormfrontend('edtelpkantoribu', 'Nomor Telepon Kantor', form_input_frontend_(getArrayObj('edtelpkantoribu', @$rowortuibu->telpkantor, ''), '', 'placeholder="Nomor Telepon Kantor" '));

            $xBufResult .= setFormfrontend('edalamatrumahibu', 'Alamat Rumah *', form_input_frontend_(getArrayObj('edalamatrumahibu', @$rowortuibu->alamatrumah, ''), '', 'placeholder="Alamat Rumah"  required="required"'));

            $xBufResult .= setFormfrontend('edidprovinsiibu', 'Provinsi*', form_dropdown('edidprovinsiibu', $this->modelprovinsi->getArraylistprovinsi(), @$rowortuibu->idprovinsi, ' class="form-control" id="edidprovinsiibu" onchange="provinsiortuibubysiswa()" required="required"'));

            $xBufResult .= '<div id="kabupatenibu">' . $kabupaten . '</div>';

            $xBufResult .= '<div id="kecamatanibu">' . $kecamatan . '</div>';

            $xBufResult .= setFormfrontend('ednohpibu', 'No HP*', form_input_frontend_(getArrayObj('ednohpibu', @$rowortuibu->nohp, ''), '', 'placeholder=" NO Hp Format : 628xxxxxx" required="required"'));

            $xBufResult .= setFormfrontend('edstatusorangtuaibu', 'Status Ibu *', form_dropdown('edstatusorangtuaibu', getArraystatusortu(), @$rowortuibu->statusorangtua, ' class="form-control" id="edstatusorangtuaibu" required="required"'));

            $xBufResult .= setFormfrontend('edstatusperkawinanibu', 'Status Perkawinan *', form_dropdown('edstatusperkawinanibu', getArrayperkawinan(), @$rowortuibu->statusperkawinan, ' class="form-control" id="edstatusperkawinanibu" required="required"'));
        }
        $xBufResult .= '</div>';
        $xBufResult .= '<div class="form-group row mt-5">'
                . '<div class="col text-center">'
                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanorangtuaibubysiswa()">'
                . 'Lanjut'
                . '</button>'
                . '</div>'
                . '</div></form>';
        $xBufResult .= '</div>';
        $this->json_data['formibu'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function createformwalisiswa() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->helper('common');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modelagama');
        $this->load->model('modelwali');
        $rowwali = $this->modelwali->getListwalisiswadetail($idsiswa);
        $kabupaten = setFormfrontend('edidkabupatenwali', 'Kabupaten*', form_dropdown('edidkabupatenwali', array('--Kabupaten--'), '', ' class="form-control" required="required" id="edidkabupatenwali" disabled="disabled"'));
        $kecamatan = setFormfrontend('edidkecamatanwali', 'Kecamatan*', form_dropdown('edidkecamatanwali', array('--Kecamatan--'), '', ' class="form-control" required="required" id="edidkecamatanwali" disabled="disabled"'));
        if (!empty($rowwali->idkabupaten)) {
            $kabupaten = setFormfrontend('edidkabupatenwali', 'Kabupaten*', form_dropdown('edidkabupatenwali', $this->modelkabupaten->getListkabupatenbyprovinsi(@$rowwali->idprovinsi), $rowwali->idkabupaten, ' class="form-control" required="required" id="edidkabupatenwali" disabled="disabled"'));
        }
        if (!empty($rowwali->idkabupaten)) {
            $kecamatan = setFormfrontend('edidkecamatanwali', 'Kabupaten*', form_dropdown('edidkecamatanwali', $this->modelkecamatan->getListkecamatanbykabupaten(@$rowwali->idkabupaten), $rowwali->idkecamatan, ' class="form-control" required="required" id="edidkecamatanwali" disabled="disabled"'));
        }
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Data Wali</h3>'
                . '<p class="text-muted">'
                . '<span class="light">Silahkan isi formulir Data Wali berikut ini:</span><br><i class="fa fa-info-circle"></i> Klik tombol <strong>Lewati</strong> untuk melewati pengisian data.<br>Klik tombol <strong>Lanjut</strong> untuk menyimpan data.'
                . '</p><hr>'
                . '</div>'
                . '</div>';
        $xBufResult .= form_open_multipart('ctrsiswa/inserttable', array('id' => 'form', 'name' => 'form'));
        $xBufResult .= '<form>';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="' . @$rowwali->idx . '" />';
        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $idsiswa . '" />';

        $xBufResult .= setFormfrontend('ednamalengkapwali', 'Nama Wali', form_input_frontend_(getArrayObj('ednamalengkapwali', @$rowwali->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

        $xBufResult .= setFormfrontend('edtempatlahirwali', 'Tempat Lahir', form_input_frontend_(getArrayObj('edtempatlahirwali', @$rowwali->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

        $xBufResult .= setFormfrontend('edtanggallahirwali', 'Tanggal Lahir', form_input_frontend_(getArrayObj('edtanggallahirwali', datetomysql(@$rowwali->tanggallahir), ''), '', ' class="tanggal form-control" required="required" placeholder=" Tanggal Lahir Format : DD-MM-YYYY" '));

        $xBufResult .= setFormfrontend('edagamawali', 'Agama', form_dropdown('edagamawali', $this->modelagama->getArrayListagama(), @$rowwali->agama, ' class="form-control" id="edagamawali" required="required"'));

        $xBufResult .= setFormfrontend('edkewarganegaraanwali', 'Kewarganegaraan', form_dropdown('edkewarganegaraanwali', getArraywn(), @$rowwali->kewarganegaraan, ' class="form-control" id="edkewarganegaraanwali" required="required"'));

        $xBufResult .= setFormfrontend('edpendidikanterakhir', 'Pendidikan Terakhir', form_dropdown('edpendidikanterakhirwali', getArrayschool(), @$rowwali->pendidikanterakhir, ' class="form-control" id="edpendidikanterakhirwali" required="required"'));

        $xBufResult .= setFormfrontend('edpekerjaanwali', 'Pekerjaan', form_dropdown('edpekerjaanwali', getArrayjob(), @$rowwali->pekerjaan, ' class="form-control" id="edpekerjaanwali" '));

        $xBufResult .= setFormfrontend('edpangkatjabatanwali', 'Pangkat/Jabatan', form_input_frontend_(getArrayObj('edpangkatjabatanwali', @$rowwali->pangkatjabatan, ''), '', 'placeholder="Pangkat/Jabatan" '));

        $xBufResult .= setFormfrontend('edpenghasilanperbulanwali', 'Penghasilan per-Bulan*', form_dropdown('edpenghasilanperbulanwali', getArraypenghasilan(), @$rowwali->penghasilanperbulan, ' class="form-control" id="edpenghasilanperbulanwali" required="required"'));

        $xBufResult .= setFormfrontend('edalamatrumahwali', 'Alamat Rumah ', form_input_frontend_(getArrayObj('edalamatrumahwali', @$rowwali->alamatrumah, ''), '', 'placeholder="Alamat Rumah" required="required"'));

        $xBufResult .= setFormfrontend('edidprovinsiwali', 'Provinsi', form_dropdown('edidprovinsiwali', $this->modelprovinsi->getArraylistprovinsi(), @$rowwali->idprovinsi, ' class="form-control" id="edidprovinsiwali" onchange="provinsiwalibysiswa()" required="required"'));

        $xBufResult .= '<div id="kabupatenwali">' . $kabupaten . '</div>';

        $xBufResult .= '<div id="kecamatanwali">' . $kecamatan . '</div>';

        $xBufResult .= setFormfrontend('ednotelpwali', 'Nomor HP/Telepon', form_input_frontend_(getArrayObj('ednotelpwali', @$rowwali->notelp, ''), '', 'placeholder=" Nomor HP/Telepon Rumah" required="required"'));

        $xBufResult .= setFormfrontend('edhubungandengancalonwali', 'Status Wali', form_dropdown('edhubungandengancalonwali', getArrayhubwali(), @$rowwali->hubungandengancalon, ' class="form-control" id="edhubungandengancalonwali" required="required"'));

        $xBufResult .= setFormfrontend('edstatusperkawinanwali', 'Status Pernikahan', form_dropdown('edstatusperkawinanwali', getArraystatusnikah(), @$rowwali->statusperkawinan, ' class="form-control" id="edstatusperkawinanwali" required="required"'));

        $xBufResult .= '<div class="form-group row mt-5">'
                . '<div class="col text-center">'
                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanwalibysiswa()">'
                . 'Lanjut'
                . '</button> &nbsp'
                . '<button type="button" class="btn btn-lg btn-ursuline px-5" onclick="formpilihjurusan()">'
                . 'Lewati'
                . '</button>'
                . '</div>'
                . '</div></form>';
        $xBufResult .= '</div>';
        $this->json_data['formwali'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function createformjurusansiswa() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->helper('common');
        $idtahunajar = $this->session->userdata('idtahunajar');
        $idgelombang = $this->session->userdata('idgelombang');
        $idkategoridaftar = $this->session->userdata('idkategoridaftar');
        $this->load->model('modeltahunajar');
        $this->load->model('modelgelombang');
        $this->load->model('modelkategoridaftar');
        $this->load->model('modelpilihan');
        $this->load->model('modelagama');
        $this->load->model('modelpendaftaran');
        $rsiswa = $this->modelpendaftaran->getDatailpendaftaranbysiswa($idsiswa);
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Pilihan Jurusan</h3>'
                . '<p class="text-muted">'
                . '<span class="light">Silahkan isi formulir Pilihan Jurusan berikut ini:</span><br><i class="fa fa-info-circle"></i> Penentuan pilihan jurusan bisa berubah sesuai dengan keputusan dari Sekolah'
//                . 'Penentuan Jurusan dapat ber' /// kata-kata keterangan dalam form pemilihan jurusan
                . '</p><hr>'
                . '</div>'
                . '</div>';
        $xBufResult .= form_open_multipart('ctrsiswa/inserttable', array('id' => 'form', 'name' => 'form'));
        $xBufResult .= '<form>';
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="' . @$rsiswa->idx . '" />';
        $xBufResult .= '<input type="hidden" name="edidgelombang" id="edidgelombang" value=" ' . $idgelombang . ' " />';
        $xBufResult .= '<input type="hidden" name="edidtahunajar" id="edidtahunajar" value=" ' . $idtahunajar . ' " />';
        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $idsiswa . '" />';
        $xBufResult .= setFormfrontend('edkoependaftaran', 'Kode Pendaftaran', form_input_frontend_(getArrayObj('edkoependaftaran', @$rsiswa->koependaftaran, ''), '', 'disabled'));
        $pilihan = $this->modelpilihan->getListpilihan(0, 10);
        $xBufResult .= '<div id="idjurusan">';
        $no = 1;
        foreach ($pilihan->result() as $row) {
            $xBufResult .= setFormfrontend('edidjurusan', ' Jurusan Pilihan ' . $no++, $this->listjurusan($row->idx));
        }
        $xBufResult .= '</div>';
        $xBufResult .= '<div class="form-group row mt-5">'
                . '<div class="col text-center">'
                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanpilihjurusanbysiswa()">'
                . 'Selesai'
                . '</button>'
                . '</div>'
                . '</div></form>';
        $xBufResult .= '</div>';
        $this->json_data['formjurusan'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function createformnilairaportsiswa() {
//        $idsiswa = $this->session->userdata('idsiswa');
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('form');
        $this->load->helper('json');
        $this->load->helper('html');
        $this->load->helper('common');
        $this->load->model('modelkelastingkat');
        $this->load->model('modelsemester');
        $this->load->model('modelgelombang');
        $idtahunajar = $this->session->userdata('idtahunajar');
        $idgelombang = $this->session->userdata('idgelombang');
        $idkategoridaftar = $this->session->userdata('idkategoridaftar');
        $this->load->model('modeltahunajar');
        $this->load->model('modelgelombang');
        $this->load->model('modelkategoridaftar');
        $this->load->model('modelpilihan');
        $this->load->model('modelagama');
        $this->load->model('modeltahun');
        $this->load->model('modelrapot');
        $this->load->model('modelpendaftaran');
        $rowdaftar = $this->modelpendaftaran->getDatailpendaftaranbysiswa($idsiswa);
        $rowrapot = $this->modelrapot->getDetailrapotbyidsiswa($idsiswa);
        $lock = '';
        if (@$rowrapot->islock == "Y") {
            $lock = 'disabled';
        }
//        $kode = $rowdaftar->koependaftaran;
        $kode = $_POST['edkoependaftaran'];
        $nama = '';
        if (@$rowdaftar->koependaftaran != '' && $kode == '0' || $kode == 'undefined') {
            $kode = $rowdaftar->koependaftaran;
            $nama = ucwords(strtolower($rowdaftar->namalenkap));
        }
//        $kode12 = $_POST['edidsiswa'];
        $nama = ucwords(strtolower(@$rowdaftar->namalenkap));
        $xBufResult = '';
        $xBufResult .= '<div class="container daftar py-5 px-0">';
        if (count($rowdaftar) > 0) {
            $xBufResult .= '<div class="row mb-4">'
                    . '<div class="col text-center">'
                    . '<h3>Data Nilai Rapor</h3>'
                    . '<p class="text-muted light">'
                    . 'Silahkan isi formulir data nilai rapor pengetahuan berikut ini:'
                    . '</p><hr>'
                    . '</div>'
                    . '</div>';
            $this->load->helper('form');
            $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
            $xBufResult .= setFormfrontend('edkodependaftaran', 'Kode Pendaftaran ', form_input_frontend_(getArrayObj('edkodependaftaran', $kode, ''), '', ' disabled'));
            $xBufResult .= setFormfrontend('ednamasiswa', 'Nama Siswa', form_input_frontend_(getArrayObj('ednamasiswa', $nama, ''), '', ' disabled'));
//        $xBufResult .= setFormfrontend('ednorapot', 'No Raport', form_input_frontend_(getArrayObj('ednorapot', '', ''), '', 'placeholder="No Raport"'));
            $xBufResult .= setFormfrontend('edtahunrapot', 'Tahun Masuk SMP ', form_dropdown('edtahunrapot', $this->modeltahun->getArraylisttahun(), @$rowrapot->tahunrapot, ' class="form-control" id="edtahunrapot" required="required"' . $lock . ''));
//        $xBufResult .= setFormfrontend('edtahunrapot', 'Tahun Masuk SMP', form_dropdown($data, $options));
            $xBufResult .= '<div id="tabledatarapot" class="mt-5">' . $this->getlistrapot(0, '', $kode) . '</div>';

//        return $xBufResult;
        } else {
            $xBufResult .= '<div class="row mb-4">'
                    . '<div class="col text-center">'
                    . '<h3>Mohon Isi Form Pilihan Jurusan Sebelum Input Nilai Rapot</h3>'
                    . '<p class="text-muted">'
                    . ''
                    . '</p><hr>'
                    . '</div>'
                    . '</div>';
        }
        $this->json_data['formrapot'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function getlistrapot($xAwal, $xSearch, $kode, $xgelombang = '', $xkelas = '', $xsemester = '') {
        $idsiswa = $this->session->userdata('idsiswa');
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('No.', 'vertical-align:middle; ', 'data-field="idx" data-sortable="true" rowspan="2" align="center"') .
                tbaddcellhead('Mapel', 'vertical-align:middle', 'data-field="mapel" data-sortable="true" rowspan="2" align="center"') .
                $this->kelastingkat() .
                tbaddcellhead('Total', 'vertical-align:middle', 'data-field="totalnilai" data-sortable="true" rowspan="2" align="center"') .
                tbaddcellhead('Rerata', 'vertical-align:middle', 'data-field="ratarata" data-sortable="true" rowspan="2" align="center"')
                , '', TRUE);
        $xbufResult1 .= tbaddrow(
                $this->kelastingkatsemester()
                , '', TRUE);
        $this->load->model('modelrapot');
        $this->load->model('modelmatapelajaran');
        $this->load->model('modelkelastingkat');
        $this->load->model('modelpendaftaran');
        $this->load->model('modelsemester');

        $xQuery = $this->modelrapot->getListrapotbypendaftaranawal($xAwal, 100, $xSearch, $xgelombang, $xkelas, $xsemester, $idsiswa, $kode);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        $rerata = 0;
        $totalnilai = 0;
        $idpendaftaran = 0;
        $idx = 0;
        $i = 1;
        $query = $this->modelmatapelajaran->getListmatapelajaranrapot(0, 20);
        foreach ($query->result() as $rowmapel) {
            foreach ($xQuery->result() as $row) {
                $idx = $row->idx;
                $idpendaftaran = $row->idpendaftaran;
                $rerata = $row->ratarata;
                $totalnilai = $row->totalnilai;
                $xbufidx = '<input type="hidden" name="edidx" id="edidx_' . $row->idx . '" value="' . $row->idx . '" />';
                $lock = ($row->islock == 'Y') ? 'disabled' : '';
                $xButtonHapus = '<i class="btn btn-lg btn-ursuline px-2 fa-save btn" aria-hidden="true" onclick = "dosimpanrapot(\'' . $row->idx . '\',\'' . $row->idpendaftaran . '\');">save</i>';
                $xbufResult .= tbaddrow(tbaddcell($i++ . $xbufidx) .
                        tbaddcell($rowmapel->mapel) .
                        $this->matapelajarannilai($idpendaftaran, $rowmapel->idx, $lock) .
                        tbaddcell(form_input_(getArrayObj('edtotalnilai_' . $row->idpendaftaran . $rowmapel->idx, $row->totalnilai, '80'), '', 'class="subtotal" disabled')) .
                        tbaddcell(form_input_(getArrayObj('edratarata_' . $row->idpendaftaran . $rowmapel->idx, $row->ratarata, '80'), '', 'class="subrerata" disabled'))
                );
            }
        }

        $xbufResult .= tbaddrow(
                tbaddcell('Total Nilai', '', 'colspan="3') .
                tbaddcell(form_input_(getArrayObj('edtotalnilaiakhir_' . $idpendaftaran, $totalnilai, '80'), '', ' disabled'))
        );
        $xbufResult .= tbaddrow(
                tbaddcell('Rata-rata Nilai', '', 'colspan="3') .
                tbaddcell(form_input_(getArrayObj('edratarataakhir_' . $idpendaftaran, $rerata, '80'), '', ' disabled'))
        );
        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table"');
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    <div class="modal-header">
        <button type="submit" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>
      </div><div class="form-group row">'
                . '<div class="col text-center mt-5">'
                . '<button type="submit" class="btn btn-lg btn-ursuline px-5" onclick = "dosimpanrapot(\'' . $idx . '\',\'' . $idpendaftaran . '\');">'
                . 'Selesai'
                . '</button>'
                . '</div>'
                . '</div></form>
        </div>';
    }

    /*
     * Form Core End
     */



    /*
     * Form additional  Start
     */

    function detailBiaya($idsiswa) {
        $this->load->helper('common');
        $this->load->model('modelgelombang');
        $this->load->model('modelitembayar');
        $this->load->model('modelbank');
        $this->load->model('modelverifikasipembayaran');
        $cekuploadbukti = $this->modelverifikasipembayaran->getDetailverifikasipembayaranbyitembayar($idsiswa, 1);

        $xidgelombang = $this->session->userdata('idgelombang');
        $tahun = $this->session->userdata('tahunajar');
        $xketerangan = $this->modelgelombang->getDetailgelombang($xidgelombang);
        $beliform = $this->modelitembayar->getBayarformbyidsiswadata($idsiswa, 1);
        $xBufResult = '';

        if (!empty($cekuploadbukti)) {
            $xBufResult = $this->notifikasisetelahuploadbukti($idsiswa);
        } else {
            $xBufResult .= '<div id="form">';
            $xBufResult .= '<div class="container daftar py-2 py-lg-4 px-0">';
            $xBufResult .= '<div class="row mb-4">'
                    . '<div class="col text-center">'
                    . '<h3>Konfirmasi Pembelian Formulir Pendaftaran</h3>'
                    . '<p class="text-muted">'
                    . 'Terimakasih Sudah Mendaftar Sebagai Calon Peserta Didik Baru<br>' . $xketerangan->gelombang . ' Tahun Ajaran ' . $tahun . '</p>
                    <img class="my-3" src="' . base_url() . 'resource/scriptmedia/images/logo.png">
                    <h2>Salam Serviam!</h2>
                    </div><div class="w-100"><hr/></div><div class="col mt-2"><p>Silahkan melakukan pembayaran sejumlah:</p>
                    <dl">
                    <dd><b>Rp. ' . number_format(@$beliform->nominal, 2, ",", ".") . '</b></dd>
                    <dd><b>No. Pembayaran : ' . @$beliform->noinvoice . '</b></dd>
                    <dd><i>Bank Pengiriman</i></dd>';

            /*
             * Start Data bank yang digunakan untuk transfer biaya pendaftaran
             */
            $databank = $this->modelbank->getListbank(0, 10, '');
            $nomor = 1;
            if (!empty($databank)) {
                foreach ($databank->result() as $value) {
                    $xBufResult .= '<dd><b> Bank : ' . $value->bank . '</b></dd>'
                            . '<dd><b>No. Rek : ' . $value->norek . '</b></dd>'
                            . '<dd><b>A/n  : ' . $value->namapemilik . '</b></dd></dl>';
                }
            }
            /*
             * End Data bank yang digunakan untuk transfer biaya pendaftaran
             */
            $xBufResult .= '<p class="lead mt-4">Harap mencantumkan Nomor Pembayaran pada waktu transfer sebagai bukti pembayaran.</p><p>Setelah mentransfer, konfirmasikan bukti transfer dengan cara klik tombol <b>Unggah Bukti</b> di bawah ini.<br>Panitia akan segera mengkonfirmasi pendaftaran Anda.<br><br>
                    Salam,<br>Panitia PPDB ' . $tahun . '. </p> '
                    . '<div class="form-group row">'
                    . '<div class="col text-center">'
                    . '<button type="submit" class="btn btn-lg btn-ursuline px-3 mt-3" onclick = "bayarbeliform(\'' . $idsiswa . '\',\'1\',\'' . $beliform->nominal . '\');">'
                    . 'Unggah Bukti Transfer' /// Button Selesai
                    . '</button>'
                    . '</div>'
                    . '<div class="alert alert-success mt-5 mx-3"><p>Formulir akan terbuka setelah Anda melakukan pembayaran dan mengunggah bukti transfer serta telah mendapat konfirmasi dari pihak sekolah.</p>'
                    . '<p>Konfirmasi pembayaran akan dilayani pada jam kerja, 1x24 jam.</p>'
                    . '</div>'
                    . '</div></div>'
                    . '</div><div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div   class="modal-content">
                                <div class="modal-header">
                                    <button type="submit" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
                                </div>
                    <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
//        $xBufResult .= '<form>';
            $xBufResult .= '</form>';

//        $xBufResult .=
            $xBufResult .= '</div></div>';
        }

        return $xBufResult;
    }

    function notifikasisetelahuploadbukti($idsiswa) {
        $this->load->helper('common');
        $this->load->model('modelgelombang');
        $this->load->model('modelitembayar');
        $this->load->model('modelbank');
        $this->load->model('modelverifikasipembayaran');
        $cekuploadbukti = $this->modelverifikasipembayaran->getDetailverifikasipembayaranbyitembayar($idsiswa, 1);
        $bank = $this->modelbank->getDetailbank($cekuploadbukti->idbank);
        $xidgelombang = $this->session->userdata('idgelombang');
        $tahun = $this->session->userdata('tahunajar');
        $xketerangan = $this->modelgelombang->getDetailgelombang($xidgelombang);
        $beliform = $this->modelitembayar->getBayarformbyidsiswadata($idsiswa, 1);
        $xBufResult = '';
        $xBufResult .= '<div id="form">';
        $xBufResult .= '<div class="container daftar py-2 py-lg-5 px-0">';
        $xBufResult .= '<div class="row mb-4">'
                . '<div class="col text-center">'
                . '<h3>Konfirmasi Bukti Pembayaran Pembelian Formulir Pendaftaran</h3>'
                . '<p class="text-muted">'
                . 'Terimakasih Sudah Mendaftar Sebagai Calon Peserta Didik Baru<br>' . $xketerangan->gelombang . ' Tahun Ajaran ' . $tahun . '</p>
                    <img class="my-3" src="' . base_url() . 'resource/scriptmedia/images/logo.png">
                    <h2>Salam Serviam!</h2>
                    </div><div class="w-100"><hr/></div><div class="col mt-2"><p>
                    Anda telah mengirim bukti pembayaran sejumlah:</p>
                    <dl>
                    <dd>Jumlah Dibayar : <b>Rp. ' . number_format(@$beliform->nominal, 2, ",", ".") . '</b></dd>
                    <dd>No. Pembayaran : <b>' . @$beliform->noinvoice . '</b></dd>
                    <dd>Nama Rekening Pengirim : <b>' . $cekuploadbukti->namapemilikrekening . '</b></dd>
                    <dd>Bank Tujuan : <b>' . @$bank->bank . ' -> ' . @$bank->namapemilik . ' -> ' . @$bank->norek . '</b></dd>'
                . '<dd>Bukti Transfer:</dd> '
                . '</dl>'
                . '<div class="col-12 col-lg-4">'
                . '<img class="img-fluid" src="' . base_url() . 'resource/uploaded/img/' . $cekuploadbukti->file . '">'
                . '</div>'
                . '<p class="mt-5"><hr/>Silahkan menunggu konfirmasi pembayaran melalui Email atau SMS dari pihak panitia pada jam kerja sekolah.<br><br>Salam,<br>Panitia PPDB.</p>'
                . '<div class="alert alert-success mt-5 mx-1">Konfirmasi pembayaran akan dilayani pada jam kerja, 1x24 jam.'
                . '<br>Setelah mendapatkan konfirmasi, harap mengisi biodata calon siswa <a href="' . base_url() . 'index.php/Myaccount"><b>disini</b></a>'
                . '</div>'
                . '</div>';
//        $xBufResult .= '<form>';
        $xBufResult .= '</form>';

//        $xBufResult .=
        $xBufResult .= '</div></div>';
        return $xBufResult;
    }

    function formnopip() {
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');
        $pip = $_POST['edpip'];
        $xBufResult = '';
        if ($pip == "Y") {
            $xBufResult = setFormfrontend('ednopip', 'Nomor KIP', form_input_frontend_(getArrayObj('ednopip', '', ''), '', 'placeholder=" Nomor Kartu Indonesia Pintar" required="required"'), '');
        }
        $this->json_data['formnopip'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function cekhiduportuayah() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');

        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modelagama');
        $this->load->model('modelorangtua');
        $this->load->model('modelpekerjaan');
        $rowortuayah = $this->modelorangtua->getListorangtuaayahsiswadetail(0, 10, $idsiswa);
        $kabupaten = setFormfrontend('edidkabupatenayah', 'Kabupaten*', form_dropdown('edidkabupatenayah', array('--Kabupaten--'), '', ' class="form-control" required="required" id="edidkabupatenayah" disabled="disabled"'));
        $kecamatan = setFormfrontend('edidkecamatanayah', 'Kecamatan*', form_dropdown('edidkecamatanayah', array('--Kecamatan--'), '', ' class="form-control" required="required" id="edidkecamatanayah" disabled="disabled"'));
        if (!empty($rowortuayah->idkabupaten)) {
            $kabupaten = setFormfrontend('edidkabupatenayah', 'Kabupaten*', form_dropdown('edidkabupatenayah', $this->modelkabupaten->getListkabupatenbyprovinsi(@$rowortuayah->idprovinsi), $rowortuayah->idkabupaten, ' class="form-control" required="required" id="edidkabupatenayah" disabled="disabled"'));
        }
        if (!empty($rowortuayah->idkabupaten)) {
            $kecamatan = setFormfrontend('edidkecamatanayah', 'Kabupaten*', form_dropdown('edidkecamatanayah', $this->modelkecamatan->getListkecamatanbykabupaten(@$rowortuayah->idkabupaten), $rowortuayah->idkecamatan, ' class="form-control" required="required" id="edidkecamatanayah" disabled="disabled"'));
        }
        $statushidup = $_POST['edstatushidupayah'];
        $xBufResult = '';
        if ($statushidup == "Meninggal") {
            $xBufResult .= setFormfrontend('ednamalengkapayah', 'Nama Ayah *', form_input_frontend_(getArrayObj('ednamalengkapayah', @$rowortuayah->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

            $xBufResult .= setFormfrontend('edtempatlahirayah', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahirayah', @$rowortuayah->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required" '));

            $xBufResult .= setFormfrontend('edtanggallahirayah', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahirayah', datetomysql(@$rowortuayah->tanggallahir), ''), '', ' class="tanggal form-control" required="required" placeholder=" Tanggal Lahir Format : DD-MM-YYYY" '));
        } else {
            $xBufResult .= setFormfrontend('ednamalengkapayah', 'Nama Ayah *', form_input_frontend_(getArrayObj('ednamalengkapayah', @$rowortuayah->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

            $xBufResult .= setFormfrontend('edtempatlahirayah', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahirayah', @$rowortuayah->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

            $xBufResult .= setFormfrontend('edtanggallahirayah', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahirayah', datetomysql(@$rowortuayah->tanggallahir), ''), '', ' class="tanggal form-control" required="required" placeholder=" Tanggal Lahir Format : DD-MM-YYYY" '));

            $xBufResult .= setFormfrontend('edagamaayah', 'Agama *', form_dropdown('edagamaayah', $this->modelagama->getArrayListagama(), @$rowortuayah->agama, ' class="form-control" id="edagamaayah" required="required"'));

            $xBufResult .= setFormfrontend('edkewarganegaraanayah', 'Kewarganegaraan *', form_dropdown('edkewarganegaraanayah', getArraywn(), @$rowortuayah->kewarganegaraan, ' class="form-control" id="edkewarganegaraanayah" required="required"'));

            $xBufResult .= setFormfrontend('edpendidikanterakhir', 'Pendidikan Terakhir *', form_dropdown('edpendidikanterakhirayah', getArrayschool(), @$rowortuayah->pendidikanterakhir, ' class="form-control" id="edpendidikanterakhirayah" required="required"'));

            $xBufResult .= setFormfrontend('edpekerjaanayah', 'Pekerjaan', form_dropdown('edpekerjaanayah', $this->modelpekerjaan->getArrayListpekerjaan(), @$rowortuayah->pekerjaan, ' class="form-control" id="edpekerjaanayah" onchange="kerjalain()" '));

            $xBufResult .= '<div id="kerjalain"></div>';

            $xBufResult .= setFormfrontend('edpangkatjabatanayah', 'Pangkat/Jabatan', form_input_frontend_(getArrayObj('edpangkatjabatanayah', @$rowortuayah->pangkatjabatan, ''), '', 'placeholder="Pangkat/Jabatan" '));

            $xBufResult .= setFormfrontend('edpenghasilanperbulanayah', 'Penghasilan per-Bulan*', form_dropdown('edpenghasilanperbulanayah', getArraypenghasilan(), @$rowortuayah->penghasilanperbulan, ' class="form-control" id="edpenghasilanperbulanayah" required="required"'));

            $xBufResult .= setFormfrontend('ednamakantorayah', 'Nama Kantor', form_input_frontend_(getArrayObj('ednamakantorayah', @$rowortuayah->namakantor, ''), '', 'placeholder="Nama Kantor" '));

            $xBufResult .= setFormfrontend('edalamatkantorayah', 'Alamat Kantor', form_input_frontend_(getArrayObj('edalamatkantorayah', @$rowortuayah->alamatkantor, ''), '', 'placeholder="Alamat Kantor" '));

            $xBufResult .= setFormfrontend('edtelpkantorayah', 'Nomor Telepon Kantor', form_input_frontend_(getArrayObj('edtelpkantorayah', @$rowortuayah->telpkantor, ''), '', 'placeholder="Nomor Telepon Kantor" '));

            $xBufResult .= setFormfrontend('edalamatrumahayah', 'Alamat Rumah *', form_input_frontend_(getArrayObj('edalamatrumahayah', @$rowortuayah->alamatrumah, ''), '', 'placeholder="Alamat Rumah" required="required"'));

            $xBufResult .= setFormfrontend('edidprovinsiayah', 'Provinsi*', form_dropdown('edidprovinsiayah', $this->modelprovinsi->getArraylistprovinsi(), @$rowortuayah->idprovinsi, ' class="form-control" id="edidprovinsiayah" onchange="provinsiortuayahbysiswa()" required="required"'));

            $xBufResult .= '<div id="kabupatenayah">' . $kabupaten . '</div>';

            $xBufResult .= '<div id="kecamatanayah">' . $kecamatan . '</div>';

            $xBufResult .= setFormfrontend('ednohpayah', 'No HP*', form_input_frontend_(getArrayObj('ednohpayah', '', ''), '', 'placeholder=" NO Hp Format : 628xxxxxx" required="required"'));

            $xBufResult .= setFormfrontend('edstatusorangtuaayah', 'Status Ayah *', form_dropdown('edstatusorangtuaayah', getArraystatusortu(), @$rowortuayah->statusorangtua, ' class="form-control" id="edstatusorangtuaayah" required="required"'));

            $xBufResult .= setFormfrontend('edstatusperkawinanayah', 'Status Perkawinan *', form_dropdown('edstatusperkawinanayah', getArrayperkawinan(), @$rowortuayah->statusperkawinan, ' class="form-control" id="edstatusperkawinanayah" required="required"'));
        }
        $this->json_data['cekhidupayah'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function cekhiduportuibu() {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');

        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modelagama');
        $this->load->model('modelorangtua');
        $this->load->model('modelpekerjaan');
        $rowortuibu = $this->modelorangtua->getListorangtuaibusiswadetail(0, 10, $idsiswa);
        $kabupaten = setFormfrontend('edidkabupatenibu', 'Kabupaten*', form_dropdown('edidkabupatenibu', array('--Kabupaten--'), '', ' class="form-control" required="required" id="edidkabupatenibu" disabled="disabled"'));
        $kecamatan = setFormfrontend('edidkecamatanibu', 'Kecamatan*', form_dropdown('edidkecamatanibu', array('--Kecamatan--'), '', ' class="form-control" required="required" id="edidkecamatanibu" disabled="disabled"'));
        if (!empty($rowortuibu->idkabupaten)) {
            $kabupaten = setFormfrontend('edidkabupatenibu', 'Kabupaten*', form_dropdown('edidkabupatenibu', $this->modelkabupaten->getListkabupatenbyprovinsi(@$rowortuibu->idprovinsi), $rowortuibu->idkabupaten, ' class="form-control" required="required" id="edidkabupatenibu" disabled="disabled"'));
        }
        if (!empty($rowortuibu->idkabupaten)) {
            $kecamatan = setFormfrontend('edidkecamatanibu', 'Kabupaten*', form_dropdown('edidkecamatanibu', $this->modelkecamatan->getListkecamatanbykabupaten(@$rowortuibu->idkabupaten), $rowortuibu->idkecamatan, ' class="form-control" required="required" id="edidkecamatanibu" disabled="disabled"'));
        }
        $statushidup = $_POST['edstatushidupibu'];
        $xBufResult = '';
        if ($statushidup == "Meninggal") {
            $xBufResult .= setFormfrontend('ednamalengkapibu', 'Nama Ibu *', form_input_frontend_(getArrayObj('ednamalengkapibu', @$rowortuibu->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

            $xBufResult .= setFormfrontend('edtempatlahiribu', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahiribu', @$rowortuibu->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

            $xBufResult .= setFormfrontend('edtanggallahiribu', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahiribu', datetomysql(@$rowortuibu->tanggallahir), ''), '', ' class="tanggal form-control" required="required"  placeholder=" Tanggal Lahir Format : DD-MM-YYYY" '));
        } else {
            $xBufResult .= setFormfrontend('ednamalengkapibu', 'Nama Ibu *', form_input_frontend_(getArrayObj('ednamalengkapibu', @$rowortuibu->namalengkap, ''), '', 'placeholder="Nama Lengkap" required="required"'));

            $xBufResult .= setFormfrontend('edtempatlahiribu', 'Tempat Lahir *', form_input_frontend_(getArrayObj('edtempatlahiribu', @$rowortuibu->tempatlahir, ''), '', 'placeholder=" Tempat Lahir" required="required"'));

            $xBufResult .= setFormfrontend('edtanggallahiribu', 'Tanggal Lahir *', form_input_frontend_(getArrayObj('edtanggallahiribu', datetomysql(@$rowortuibu->tanggallahir), ''), '', ' class="tanggal form-control" required="required"  placeholder=" Tanggal Lahir Format : DD-MM-YYYY" '));

            $xBufResult .= setFormfrontend('edagamaibu', 'Agama *', form_dropdown('edagamaibu', $this->modelagama->getArrayListagama(), @$rowortuibu->agama, ' class="form-control" id="edagamaibu" required="required"'));

            $xBufResult .= setFormfrontend('edkewarganegaraanibu', 'Kewarganegaraan *', form_dropdown('edkewarganegaraanibu', getArraywn(), @$rowortuibu->kewarganegaraan, ' class="form-control" required="required" id="edkewarganegaraanibu"'));

            $xBufResult .= setFormfrontend('edpendidikanterakhir', 'Pendidikan Terakhir *', form_dropdown('edpendidikanterakhiribu', getArrayschool(), @$rowortuibu->pendidikanterakhir, 'required="required" class="form-control" id="edpendidikanterakhiribu"'));

            $xBufResult .= setFormfrontend('edpekerjaanibu', 'Pekerjaan', form_dropdown('edpekerjaanibu', $this->modelpekerjaan->getArrayListpekerjaan(), @$rowortuibu->pekerjaan, ' class="form-control" id="edpekerjaanibu" onchange="kerjalain()"'));
            $xBufResult .= '<div id="kerjalain"></div>';
            $xBufResult .= setFormfrontend('edpangkatjabatanibu', 'Pangkat/Jabatan', form_input_frontend_(getArrayObj('edpangkatjabatanibu', @$rowortuibu->pangkatjabatan, ''), '', 'placeholder="Pangkat/Jabatan" '));

            $xBufResult .= setFormfrontend('edpenghasilanperbulanibu', 'Penghasilan per-Bulan*', form_dropdown('edpenghasilanperbulanibu', getArraypenghasilan(), @$rowortuibu->penghasilanperbulan, ' class="form-control" required="required" id="edpenghasilanperbulanibu"'));

            $xBufResult .= setFormfrontend('ednamakantoribu', 'Nama Kantor', form_input_frontend_(getArrayObj('ednamakantoribu', @$rowortuibu->namakantor, ''), '', 'placeholder="Nama Kantor" '));

            $xBufResult .= setFormfrontend('edalamatkantoribu', 'Alamat Kantor', form_input_frontend_(getArrayObj('edalamatkantoribu', @$rowortuibu->alamatkantor, ''), '', 'placeholder="Alamat Kantor" '));

            $xBufResult .= setFormfrontend('edtelpkantoribu', 'Nomor Telepon Kantor', form_input_frontend_(getArrayObj('edtelpkantoribu', @$rowortuibu->telpkantor, ''), '', 'placeholder="Nomor Telepon Kantor" '));

            $xBufResult .= setFormfrontend('edalamatrumahibu', 'Alamat Rumah *', form_input_frontend_(getArrayObj('edalamatrumahibu', @$rowortuibu->alamatrumah, ''), '', 'placeholder="Alamat Rumah" required="required"'));

            $xBufResult .= setFormfrontend('edidprovinsiibu', 'Provinsi*', form_dropdown('edidprovinsiibu', $this->modelprovinsi->getArraylistprovinsi(), @$rowortuibu->idprovinsi, ' class="form-control" required="required" id="edidprovinsiibu" onchange="provinsiortuibubysiswa()"'));

            $xBufResult .= '<div id="kabupatenibu">' . $kabupaten . '</div>';

            $xBufResult .= '<div id="kecamatanibu">' . $kecamatan . '</div';

            $xBufResult .= setFormfrontend('ednohpibu', 'No HP*', form_input_frontend_(getArrayObj('ednohpibu', '', ''), '', 'placeholder=" NO Hp Format : 628xxxxxx" required="required"'));

            $xBufResult .= setFormfrontend('edstatusorangtuaibu', 'Status Ibu *', form_dropdown('edstatusorangtuaibu', getArraystatusortu(), @$rowortuibu->statusorangtua, ' class="form-control" id="edstatusorangtuaibu" required="required"'));

            $xBufResult .= setFormfrontend('edstatusperkawinanibu', 'Status Perkawinan *', form_dropdown('edstatusperkawinanibu', getArrayperkawinan(), @$rowortuibu->statusperkawinan, ' class="form-control" id="edstatusperkawinanibu" required="required"'));
        }
        $this->json_data['cekhidupibu'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function bahasalain() {
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');
        $pip = $_POST['edbahasaseharihari'];
        $xBufResult = '';
        if ($pip == "-") {
            $xBufResult = setFormfrontend('edbahasasehariharilain', '<span class="text-muted">Bahasa Yang Dimaksud</span>', form_input_frontend_(getArrayObj('edbahasasehariharilain', '', ''), '', 'placeholder="Tuliskan bahasa yang dimasksud" required="required"'), '');
        }
        $this->json_data['bahasalain'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function sekolahlain() {
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');
        $pip = $_POST['edidasalsekolah'];
        $xBufResult = '';
        if ($pip == "-") {
            $xBufResult = setFormfrontend('edidasalsekolahlain', 'Sekolah yang dimaksud', form_input_frontend_(getArrayObj('edidasalsekolahlain', '', ''), '', 'placeholder=" Isikan Sekolah yang dimaksud" required="required"'), '');
        }
        $this->json_data['sekolahlain'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function deletetableprestasi() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelprestasi');
        $this->modelprestasi->setDeleteprestasibysiswa($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function formkerjalain() {
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');
        $ayah = $_POST['edpekerjaanayah'];
        $ibu = $_POST['edpekerjaanibu'];
        $sttsortuayah = $_POST['edayahibuayah'];
        $sttsortuibu = $_POST['edayahibuibu'];
        $xBufResult = '';
        if ($ayah == "1" && $sttsortuayah == "A") {
            $xBufResult = setFormfrontend('', 'Pekerjaan Yang Dimaksud*', form_input_frontend_(getArrayObj('edpekerjaanayahbaru', '', ''), '', 'placeholder=" Silahkan tulis pekerjaan yang dimaksud" id="edpekerjaanayahbaru"'));
        } elseif ($ibu == "1" && $sttsortuibu == "I") {
            $xBufResult = setFormfrontend('', 'Pekerjaan Yang Dimaksud*', form_input_frontend_(getArrayObj('edpekerjaanibubaru', '', ''), '', 'placeholder=" Silahkan tulis pekerjaan yang dimaksud" id="edpekerjaanibubaru"'));
        }
        $this->json_data['formkerjalain'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function formbaptisandgereja() {
        $this->load->helper('json');
        $this->load->helper('form');
        $this->load->helper('common');
        $this->load->model('modelagama');

        $agama = $_POST['edagama'];
        $xagama = $this->modelagama->getDetailagama($agama);
        $rel = '';
        if (!empty($agama)) {
            $rel = $xagama->Nm_agama;
        }
        $xBufResultgerja = '';
        $xBufResultbaptis = '';
        if ($rel == "Katholik" || $rel == "Kristen") {
            $xBufResultgerja .= setFormfrontend('ednamagereja', 'Nama Gereja', form_input_frontend_(getArrayObj('ednamagereja', '', ''), '', 'placeholder=" Nama Gereja" '));
            $xBufResultbaptis .= setFormfrontend('ednamabaptis', 'Nama Baptis', form_input_frontend_(getArrayObj('ednamabaptis', '', ''), '', 'placeholder=" Nama Baptis" '), 'Bagi Yang Katolik');
        }
        $this->json_data['formbaptis'] = $xBufResultbaptis;
        $this->json_data['formgereja'] = $xBufResultgerja;
        echo json_encode($this->json_data);
    }

    function kelastingkat() {
        $this->load->model('modelkelastingkat');
        $kelas = $this->modelkelastingkat->getListkelastingkatrapotjalurnotest(0, 20);
        $xbufresult = '';
        foreach ($kelas->result() as $row) {
            $xbufresult .= tbaddcellhead($row->tingkatkelas . ' ( ' . $row->kelasangka . ' )', 'text-align:center', 'data-field="' . $row->tingkatkelas . '" data-sortable="true" colspan="2" ');
        }
        return $xbufresult;
    }

    function kelastingkatsemester() {
        $this->load->model('modelkelastingkat');
        $this->load->model('modelsemester');
//        $kelas = $this->modelkelastingkat->getListkelastingkatrapot(0, 20);
        $kelas = $this->modelkelastingkat->getListkelastingkatrapotjalurnotest(0, 20);
        $semester = $this->modelsemester->getListsemesterraport(0, 20);
        $xbufresult = '';
        foreach ($kelas->result() as $row) {
            foreach ($semester->result() as $value) {
                $xbufresult .= tbaddcellhead('Semester ' . $value->semester, '', 'align="center"');
            }
        }
        return $xbufresult;
    }

    function matapelajarannilai($idpendaftaran, $idmapel, $lock) {
        $this->load->model('modelmatapelajaran');
        $this->load->model('modelnilairapot');
        $this->load->model('modelsemester');
        $idgelombang = $this->session->userdata('idgelombang');
//        if($idgelombang==1){
        $kelas = $this->modelkelastingkat->getListkelastingkatrapotjalurnotest(0, 20);
//        }
//        elseif($idgelombang==2){
//            $kelas = $this->modelkelastingkat->getListkelastingkatrapot(0, 20);
//        }
        $semester = $this->modelsemester->getListsemesterraport(0, 20);
        $query = $this->modelmatapelajaran->getListmatapelajaranrapot(0, 20);
        $xbufresult = '';
        foreach ($kelas->result() as $valuekelas) {
            foreach ($semester->result() as $valuesemester) {

                $rownilai = $this->modelnilairapot->getDetailnilairapotbyidmapel($idmapel, $idpendaftaran, $valuekelas->idx, $valuesemester->idx);
                $xbufresult .= '<input type="hidden" name="edidsemester" id="edidsemester_' . $valuesemester->idx . '" class="semester" value="' . $valuesemester->idx . '"/>';
                $xbufresult .= '<input type="hidden" name="edidkelas" id="edidkelas_' . $valuekelas->idx . '" class="kelas" value="' . $valuekelas->idx . '"/>';
                $xbufresult .= tbaddcell(form_input_(getArrayObj('ednilai_' . $idpendaftaran . '_' . $idmapel . $valuekelas->idx . $valuesemester->idx, @$rownilai->nilai, '60'), 0, ' onkeyup="gettotalnilai(\'' . $idpendaftaran . '\','
                                . '\'' . $idmapel . '\',\'' . $valuekelas->idx . '\',\'' . $valuesemester->idx . '\');" '
                                . ' required="required"  class="value nilai' . $idpendaftaran . '' . $idmapel . ' nilaikelas' . $idpendaftaran . '' . $valuekelas->idx . '' . $idmapel . '" ' . $lock));
            }
        }
        return $xbufresult;
    }

    function listjurusan($pilihan) {
        $idsiswa = $this->session->userdata('idsiswa');
        $this->load->model('modeljurusan');
        $this->load->model('modelpendaftaran');
        $rpiljurusan = $this->modelpendaftaran->getDatailpilihjurusansiswa($idsiswa, $pilihan);
        $lock = (@$rpiljurusan->islock == 'Y') ? 'disabled' : '';
        $xBufResult = '';
        $xBufResult .= form_dropdown('edidjurusan', $this->modeljurusan->getArraylistjurusanbypilihan($pilihan), @$rpiljurusan->idjurusan, ' required="required" class="form-control" id="edidjurusan_' . $pilihan . '" ' . $lock);
        return $xBufResult;
    }

    function kabupatenbyprovinsisiswa() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $query = $this->modelkabupaten->getListkabupatenbyprovinsi((int) $xidprovinsi);
        $xBufResult = '';
        if (!empty($query)) {
            $xBufResult = setFormfrontend('edkabupaten', 'Kabupaten*', form_dropdown('edidkabupaten', $query, '', ' class="form-control" id="edidkabupaten" onchange="kabupatenselectbysiswa()" required="required" required="required"'));
        }
        $this->json_data['combokabupaten'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function kecamatanbykabupatensiswa() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $this->load->model('modelkecamatan');
        if (!($xidkabupaten == 'undefined')) {
            $xidkabupatenprovinsi = $xidkabupaten;
        } else {
            $xidkabupatenprovinsi = $xidprovinsi;
        }
        $query = $this->modelkecamatan->getListkecamatanbykabupaten((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkecamatan', 'Kecamatan *', form_dropdown('edidkecamatan', $query, '', ' class="form-control" id="edidkecamatan" onchange="kecamatanselectbysiswa();" required="required" required="required"'));
        $this->json_data['combokecamatan'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function kelurahanbykecamatansiswa() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $this->load->model('modelkelurahan');
        if (!($xidkecamatan == 'undefined')) {
            $xidkabupatenprovinsi = $xidkecamatan;
        } else {
            $xidkabupatenprovinsi = $xidkabupaten;
        }
        $query = $this->modelkelurahan->getListkelurahanbykecamatan((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkelurahan', 'Kelurahan *', form_dropdown('edidkelurahan', $query, '', ' class="form-control" id="edidkelurahan" onchange="kelurahanselectbysiswa();" required="required" required="required"'));
        $this->json_data['combokelurahan'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function kabupatenbyprovinsisekolah() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $query = $this->modelkabupaten->getListkabupatenbyprovinsi((int) $xidprovinsi);
        $xBufResult = '';
        if (!empty($query)) {
            $xBufResult = setFormfrontend('edidkabupatensekolah', 'Kabupaten Asal Sekolah*', form_dropdown('edidkabupatensekolah', $query, '', ' class="form-control" id="edidkabupatensekolah" onchange="kabupatenselectbysekolah()" '));
        }
        $this->json_data['combokabupaten'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function kecamatanbykabupatensekolah() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $this->load->model('modelkecamatan');
        $this->load->model('modelasalsekolah');
        if (!($xidkabupaten == 'undefined')) {
            $xidkabupatenprovinsi = $xidkabupaten;
        } else {
            $xidkabupatenprovinsi = $xidprovinsi;
        }
        $query = $this->modelkecamatan->getListkecamatanbykabupaten((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkecamatansekolah', 'Kecamatan Asal Sekolah*', form_dropdown('edidkecamatansekolah', $query, '', ' class="form-control" id="edidkecamatansekolah" onchange="kecamatanselectbysekolah()"'));
        $this->json_data['combokecamatan'] = $xBufResult;
        $querysekolah = $this->modelasalsekolah->getListasalsekolahbykecamatan('', (int) $xidkabupaten);
        $xBufResult1 = '';
        $xBufResult1 = setFormfrontend('edidasalsekolah', 'Sekolah Asal*', form_dropdown('edidasalsekolah', $querysekolah, '', ' class="form-control" id="edidasalsekolah" required="required" onchange="sekolahlain()"'));
        $this->json_data['comboasalsekolah'] = $xBufResult1;
//        $this->json_data['comboasalsekolah'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function asalsekolahbykecamatansekolah() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $this->load->model('modelasalsekolah');
        if (!($xidkecamatan == 'undefined') && !($xidkabupaten == 'undefined')) {
            $xidkecamatan = $xidkecamatan;
            $xidkabupaten = $xidkabupaten;
        } else {
            $xidkecamatan = '';
            $xidkabupaten = $xidkabupaten;
        }
        $query = $this->modelasalsekolah->getListasalsekolahbykecamatan((int) $xidkecamatan, (int) $xidkabupaten);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidasalsekolah', 'Sekolah Asal*', form_dropdown('edidasalsekolah', $query, '', ' class="form-control" id="edidasalsekolah" required="required" onchange="sekolahlain()"'));
        $this->json_data['comboasalsekolah'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function kabupatenbyprovinsiayah() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $query = $this->modelkabupaten->getListkabupatenbyprovinsi((int) $xidprovinsi);
        $xBufResult = '';
        if (!empty($query)) {
            $xBufResult = setFormfrontend('edidkabupatenayah', 'Kabupaten*', form_dropdown('edidkabupatenayah', $query, '', ' class="form-control" required="required" id="edidkabupatenayah" onchange="kabupatenortuayahbysiswa()" '));
        }
        $this->json_data['combokabupaten'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function kecamatanbykabupatenayah() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $this->load->model('modelkecamatan');
        if (!($xidkabupaten == 'undefined')) {
            $xidkabupatenprovinsi = $xidkabupaten;
        } else {
            $xidkabupatenprovinsi = $xidprovinsi;
        }
        $query = $this->modelkecamatan->getListkecamatanbykabupaten((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkecamatansekolah', 'Kecamatan*', form_dropdown('edidkecamatanayah', $query, '', ' class="form-control" required="required" id="edidkecamatanayah" '));
        $this->json_data['combokecamatan'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function kabupatenbyprovinsiibu() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $query = $this->modelkabupaten->getListkabupatenbyprovinsi((int) $xidprovinsi);
        $xBufResult = '';
        if (!empty($query)) {
            $xBufResult = setFormfrontend('edidkabupatenibu', 'Kabupaten*', form_dropdown('edidkabupatenibu', $query, '', ' class="form-control" required="required" id="edidkabupatenibu" onchange="kabupatenortuibubysiswa()" '));
        }
        $this->json_data['combokabupaten'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function kecamatanbykabupatenibu() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $this->load->model('modelkecamatan');
        if (!($xidkabupaten == 'undefined')) {
            $xidkabupatenprovinsi = $xidkabupaten;
        } else {
            $xidkabupatenprovinsi = $xidprovinsi;
        }
        $query = $this->modelkecamatan->getListkecamatanbykabupaten((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkecamatanibu', 'Kecamatan*', form_dropdown('edidkecamatanibu', $query, '', ' class="form-control" required="required" id="edidkecamatanibu" '));
        $this->json_data['combokecamatan'] = $xBufResult;

        echo json_encode($this->json_data);
    }

    function kabupatenbyprovinsiwali() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $query = $this->modelkabupaten->getListkabupatenbyprovinsi((int) $xidprovinsi);
        $xBufResult = '';
        if (!empty($query)) {
            $xBufResult = setFormfrontend('edidkabupatenwali', 'Kabupaten*', form_dropdown('edidkabupatenwali', $query, '', ' class="form-control" required="required" id="edidkabupatenwali" onchange="kabupatenwalibysiswa()" '));
        }
        $this->json_data['combokabupaten'] = $xBufResult;
        echo json_encode($this->json_data);
    }

    function kecamatanbykabupatenwali() {
        $this->load->helper('json');
        $this->load->helper('common');
        $this->load->helper('form');
        $xidprovinsi = $_POST['edidprovinsi'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $this->load->model('modelkecamatan');
        if (!($xidkabupaten == 'undefined')) {
            $xidkabupatenprovinsi = $xidkabupaten;
        } else {
            $xidkabupatenprovinsi = $xidprovinsi;
        }
        $query = $this->modelkecamatan->getListkecamatanbykabupaten((int) $xidkabupatenprovinsi);
        $xBufResult = '';
        $xBufResult = setFormfrontend('edidkecamatanwali', 'Kecamatan*', form_dropdown('edidkecamatanwali', $query, '', ' class="form-control" required="required" id="edidkecamatanwali" '));
        $this->json_data['combokecamatan'] = $xBufResult;

        echo json_encode($this->json_data);
    }

//    function noinvoice() {
//        $this->load->helper('json');
//        $tahunajar = $this->session->userdata('tahunajar');
//        $idgelombang = $this->session->userdata('idgelombang');
//        $idkategoridaftar = $this->session->userdata('idkategoridaftar');
//        $this->load->model('modelitembayar');
//        $rownomor = $this->modelitembayar->getLastIndexitembayar();
//        $intnomor = substr(@$rownomor->noinvoice, -5) + 1;
//        $noinvoice = 'INV' . $idgelombang . $this->number_pad($intnomor, 5);
//
//        return $noinvoice;
//    }

    function number_pad($number, $n) {
        return str_pad((int) $number, $n, "0", STR_PAD_LEFT);
    }

    /*
     * Form add  End
     */

    /*
     * Start Save/simpan
     */

//    function simpansiswa() {
//        $this->load->helper('json');
//        $this->load->helper('common');
//        $this->load->model('modelbahasa');
//        $this->load->model('modelasalsekolah');
//        if (!empty($_POST['edidx'])) {
//            $xidx = $_POST['edidx'];
//        } else {
//            $xidx = '0';
//        }
//        $xnamalenkap = $_POST['ednamalenkap'];
//        $xnamapanggilan = $_POST['ednamapanggilan'];
//        $xjeniskelamin = $_POST['edjeniskelamin'];
//        $xnokk = $_POST['ednokk'];
//        $xnoakta = $_POST['ednoaktakelahiran'];
//        $xnik = $_POST['ednik'];
//        $xnisn = $_POST['ednisn'];
//        $xnopip = $_POST['ednopip'];
//        $xtempatlahir = $_POST['edtempatlahir'];
//        $xtanggallahir = $_POST['edtanggallahir'];
//        $xagama = $_POST['edagama'];
//        $xnamagereja = $_POST['ednamagereja'];
//        $xnamabaptis = $_POST['ednamabaptis'];
//        $xgolongandarah = $_POST['edgolongandarah'];
//        $xkewaganegaraan = $_POST['edkewaganegaraan'];
//        $xalamatkk = $_POST['edalamatkk'];
//        $xalamatdomisli = $_POST['edalamatdomisli'];
//        $xidkelurahanan = $_POST['edidkelurahan'];
//        $xidkecamatan = $_POST['edidkecamatan'];
//        $xidkabupaten = $_POST['edidkabupaten'];
//        $xidprovinisi = $_POST['edidprovinisi'];
//        $xkodepos = $_POST['edkodepos'];
//        $xanakke = $_POST['edanakke'];
//        $xjumlahsaudarakandung = $_POST['edjumlahsaudarakandung'];
//        $xjumlahsaudaraangkat = $_POST['edjumlahsaudaraangkat'];
//        $xjumlahsaudaratiri = $_POST['edjumlahsaudaratiri'];
//        $xcalonadalahanak = $_POST['edcalonadalahanak'];
//        $xjaraktempattinggalkesma = $_POST['edjaraktempattinggalkesma'];
//        $xtinggibadan = $_POST['edtinggibadan'];
//        $xberatbadan = $_POST['edberatbadan'];
//        $xbahasaseharihari = $_POST['edbahasaseharihari'];
//        if ($xbahasaseharihari == '-') {
//            /*
//             * Input bahasa jika yang dipilih adalah bahasa lain
//             */
//            $xbahasaseharihari = $_POST['edbahasasehariharilain'];
//            $this->modelbahasa->setInsertbahasa('', $xbahasaseharihari);
//        }
//        $xidasalsekolah = $_POST['edidasalsekolah'];
//        /*
//         * Input sekolah jika yang dipilih adalah sekolah lain
//         */
//        if ($xidasalsekolah == '-') {
//            $xidprovinsisekolah = $_POST['edidprovinsisekolah'];
//            $xidkabupatensekolah = $_POST['edidkabupatensekolah'];
//            $xidkecamatansekolah = $_POST['edidkecamatansekolah'];
//            $xidasalsekolahlain = $_POST['edidasalsekolahlain'];
//            $this->modelasalsekolah->setInsertasalsekolah('', '', $xidasalsekolahlain, '', $xidkecamatansekolah, $xidkabupatensekolah, $xidprovinsisekolah);
//            $datasekolahasal = $this->modelasalsekolah->getLastIndexasalsekolah();
//            $xidasalsekolah = $datasekolahasal->idx;
//        }
//        $xtahunlulus = $_POST['edtahunlulus'];
//        $xpernahtinggalkelas = $_POST['edpernahtinggalkelas'];
//        $xnohp = $_POST['ednohp'];
//        $xnotelp = $_POST['ednotelprumah'];
//        $xemail = $_POST['edemail'];
//        $xpassword = str_replace(' ', '', $_POST['edpassword']);
//
//        if ($_POST['edidhobby'] != '0') {
//            $xidhobby = $_POST['edidhobby'];
//        } else {
//            $xidhobby = '0';
//        }
//        $xhobby = $_POST['edhobby'];
//
//        if ($_POST['edidkesehatan'] != '0') {
//            $xidkesehatan = $_POST['edidkesehatan'];
//        } else {
//            $xidkesehatan = '0';
//        }
//        $xitemkesehatan = $_POST['editemkesehatan'];
//        $xketerangan = $_POST['edketerangan'];
//        $xfile = $_POST['edphoto'];
//        $now = new DateTime();
//        $tglnow = $now->format('Y-m-d');
//        $newfilename = 'Photo_profile' . $tglnow . '-' . $xidx;
//        $xphoto = $this->renamefileupload($xfile, $newfilename);
//        $this->basemodel->imageresize(150, 150, $xphoto);
//        $this->load->model('modelsiswa');
//        $idpegawai = $this->session->userdata('idpegawai');
//        $xgelombang = $this->session->userdata('idgelombang');
//        $tahun = $this->session->userdata('idtahunajar');
//        $tgl = '';
//        if (!empty($xtanggallahir)) {
//            $tgl = mysqltodate($xtanggallahir);
//        }
////        $this->json_data['gelombang'] = $xidx;
//        $this->json_data['data'] = $xgelombang;
//        if (!empty($xgelombang)) {
//            if ($xidx != '0') {
//                $xStr = $this->modelsiswa->setUpdatesiswa($xidx, $xnamalenkap, $xnamapanggilan, $xjeniskelamin, $xnokk, $xnopip, $xnoakta, $xnik, $xnisn, $xtempatlahir, $tgl, $xagama, $xnamagereja, $xnamabaptis, $xgolongandarah, $xkewaganegaraan, $xalamatkk, $xalamatdomisli, $xidkelurahanan, $xidkecamatan, $xidkabupaten, $xidprovinisi, $xkodepos, $xanakke, $xjumlahsaudarakandung, $xjumlahsaudaraangkat, $xjumlahsaudaratiri, $xcalonadalahanak, $xjaraktempattinggalkesma, $xtinggibadan, $xberatbadan, $xbahasaseharihari, $xidasalsekolah, $xtahunlulus, $xpernahtinggalkelas, $xgelombang, $tahun, $xnohp, $xnotelp, $xemail, $xpassword, $xphoto);
//                $this->json_data['data'] = true;
//                $this->simpanhobbysiswa($xidhobby, $xidx, $xhobby);
//                $this->simpankesehatansiswa($xidkesehatan, $xidx, $xitemkesehatan, $xketerangan);
//            } else {
//                $xStr = $this->modelsiswa->setInsertsiswa($xidx, $xnamalenkap, $xnamapanggilan, $xjeniskelamin, $xnokk, $xnopip, $xnoakta, $xnik, $xnisn, $xtempatlahir, $tgl, $xagama, $xnamagereja, $xnamabaptis, $xgolongandarah, $xkewaganegaraan, $xalamatkk, $xalamatdomisli, $xidkelurahanan, $xidkecamatan, $xidkabupaten, $xidprovinisi, $xkodepos, $xanakke, $xjumlahsaudarakandung, $xjumlahsaudaraangkat, $xjumlahsaudaratiri, $xcalonadalahanak, $xjaraktempattinggalkesma, $xtinggibadan, $xberatbadan, $xbahasaseharihari, $xidasalsekolah, $xtahunlulus, $xpernahtinggalkelas, $xgelombang, $tahun, $xnohp, $xnotelp, $xemail, $xpassword, $xphoto);
//                $this->simpanhobbysiswa($xidhobby, $xidx, $xhobby);
//                $this->simpankesehatansiswa($xidkesehatan, $xidx, $xitemkesehatan, $xketerangan);
//            }
//        }
//        echo json_encode($this->json_data);
//    }
//
//    function simpanhobbysiswa($xidhobby, $xidx, $xhobby) {
//        $this->load->helper('json');
//
//        $this->load->model('modelhobby');
//        $idsiswa = $this->session->userdata('idsiswa');
////        if (!empty($idpegawai)) {
//        if ($xidhobby != '0' && $xidhobby != '') {
//            $xStr = $this->modelhobby->setUpdatehobby($xidhobby, $xidx, $xhobby, '', '');
//        } else {
//            $xStr = $this->modelhobby->setInserthobby($xidhobby, $xidx, $xhobby, '', '');
//        }
////        }
//        return true;
//    }
//
//    function simpankesehatansiswa($xidkesehatan, $xidx, $xitemkesehatan, $xketerangan) {
//        $this->load->helper('json');
//
//        $this->load->model('modelkesehatan');
//        if ($xidkesehatan != '0' && $xidkesehatan != '') {
//            $xStr = $this->modelkesehatan->setUpdatekesehatan($xidkesehatan, $xidx, $xitemkesehatan, '', $xketerangan);
//        } else {
//            $xStr = $this->modelkesehatan->setInsertkesehatan($xidkesehatan, $xidx, $xitemkesehatan, '', $xketerangan);
//        }
//
//        return true;
//    }
//
//    function simpanrapot() {
//        $this->load->helper('json');
//        $this->load->helper('common');
//        if (!empty($_POST['edidx'])) {
//            $xidx = $_POST['edidx'];
//        } else {
//            $xidx = '0';
//        }
//        $xidpendaftaran = $_POST['edidpendaftaran'];
//        $xkodependaftaran = $_POST['edkodependaftaran'];
//        $xnorapot = $_POST['ednorapot'];
//        $xtahunrapot = $_POST['edtahunrapot'];
//        $xtotalnilai = $_POST['edtotalnilai'];
//        $xratarata = $_POST['edratarata'];
//        $xislock = 'Y';
//
//        $this->load->model('modelrapot');
//        $this->json_data['pesan'] = " tidak Berhasil Update/insrt";
//        $idsiswa = $this->session->userdata('idsiswa');
//        if (!empty($idsiswa)) {
//            if ($xidx != '0') {
//                $xStr = $this->modelrapot->setUpdaterapot($xidx, $xkodependaftaran, $xnorapot, $xtahunrapot, $xtotalnilai, $xratarata, $xislock);
//                $this->simpannilairapot($xidx, $xidpendaftaran);
//                $this->json_data['pesan'] = "Berhasil Insert";
//                $this->json_data['location'] = site_url() . '/konfirmasifinishingdata';
//            } else {
//                $xStr = $this->modelrapot->setInsertrapot($xidx, $xkodependaftaran, $xnorapot, $xtahunrapot, $xtotalnilai, $xratarata, $xislock);
//                $xidrapot = $this->modelrapot->getLastindexrapot();
//                $this->simpannilairapot($xidrapot->idx, $xidpendaftaran);
//                $this->json_data['pesan'] = "Berhasil Insert";
//                $this->json_data['location'] = site_url() . '/konfirmasifinishingdata';
//            }
//            //create tagihan UNP SMP reginapacis
//            $this->createtagihansppsmpreginapacis($idsiswa);
//        }
//        echo json_encode($this->json_data);
//    }
//
//    function simpannilairapot($xidrapot, $idpendaftaran) {
//        $this->load->helper('json');
//        $this->load->model('modelnilairapot');
//        $this->load->model('modelmatapelajaran');
//        $this->load->model('modelkelastingkat');
//        $this->load->model('modelsemester');
////        $xidgelombang = $_POST['edidgelombang'];
////        $xidkelas = $_POST['edidkelas' . $idpendaftaran];
////        $xidsemester = $_POST['edidsemester' . $idpendaftaran];
//        $matapelajaran = $this->modelmatapelajaran->getListmatapelajaranrapot(0, 20);
//        $kelas = $this->modelkelastingkat->getListkelastingkatrapotjalurnotest(0, 20);
////        $kelas = $this->modelkelastingkat->getListkelastingkat(0, 20);
//        $semester = $this->modelsemester->getListsemester(0, 20);
//        $idsiswa = $this->session->userdata('idsiswa');
//        if (!empty($idsiswa)) {
//            $xStr = $this->modelnilairapot->deletenilaibyidrapotfrontend($xidrapot);
//
//            foreach ($kelas->result() as $rowkelas) {
//                foreach ($semester->result() as $rowsemester) {
//                    foreach ($matapelajaran->result() as $row) {
//                        $xnilai = $_POST['ednilai_' . $idpendaftaran . '_' . $row->idx . $rowkelas->idx . $rowsemester->idx];
//                        $xStr = $this->modelnilairapot->setInsertnilairapot('', $xidrapot, $row->idx, $xnilai, $rowkelas->idx, $rowsemester->idx, '', '');
//                    }
//                }
//            }
//        }
//        return TRUE;
//    }
//
//    function simpanpendaftaran() {
//        $idtahunajar = $this->session->userdata('idtahunajar');
//        $idgelombang = $this->session->userdata('idgelombang');
//
//        $this->load->helper('json');
//        if (!empty($_POST['edidx'])) {
//            $xidx = $_POST['edidx'];
//        } else {
//            $xidx = '0';
//        }
//
////        $xkoependaftaran = $_POST['edkoependaftaran'];
//        $xkoependaftaran = $this->nopendaftaran();
//        $xidsiswa = $_POST['edidsiswa'];
//        $xidgelombang = $idgelombang;
//        $xidtahunajar = $idtahunajar;
//        $xtanggaldaftar = $_POST['edtanggaldaftar'];
//        $xidjurusan = $_POST['edidjurusan'];
//        $xisditerima = 'N';
//        $xidstatuspendaftaran = '1';
//        $xisbayar = 'N';
//        $xislock = 'Y';
//        $xidkategoridaftar = $_POST['edidkategoridaftar'];
//
//        $this->load->model('modelpendaftaran');
//        $this->load->model('modelitembayar');
//        $this->load->model('modelsettingbiaya');
//        $qsetting = $this->modelsettingbiaya->getListsettingbiayabyidjeis(0, 20, 1);
//        /*
//         * 1 = pendaftaran
//         * 2 = smp reginapacis
//         * 3 = non smp reginapacis jalur non test
//         * 4 = non smp reginapacis jalur test
//         * 5 = SPP
//         */
//        $idpegawai = $this->session->userdata('idpegawai');
//        $idsiswa = $this->session->userdata('idsiswa');
//        if (!empty($idsiswa)) {
//            if ($xidx != '0') {
//                $this->load->model('modelpilihan');
//                $xQuery = $this->modelpilihan->getListpilihan(0, 10);
//
//                foreach ($xQuery->result() as $row) {
//                    if (!empty($_POST['edidjurusan_' . $row->idx])) {
//                        $xidjurusan = $_POST['edidjurusan_' . $row->idx];
//
//                        $xStr = $this->modelpendaftaran->setUpdatependaftaran($xidx, $xkoependaftaran, $xidsiswa, $xidgelombang, $xidtahunajar, $xtanggaldaftar, $xidjurusan, $xisditerima, $xidstatuspendaftaran, $xisbayar, $xislock, $row->idx);
//                    }
//                }
//            } else {
//                $this->load->model('modelpilihan');
//                $xQuery = $this->modelpilihan->getListpilihan(0, 10);
//
//                foreach ($xQuery->result() as $row) {
//                    if (!empty($_POST['edidjurusan_' . $row->idx])) {
//                        $xidjurusan = $_POST['edidjurusan_' . $row->idx];
//                        $xStr = $this->modelpendaftaran->setInsertpendaftaran($xidx, $xkoependaftaran, $xidsiswa, $xidgelombang, $xidtahunajar, $xtanggaldaftar, $xidjurusan, $xisditerima, $xidstatuspendaftaran, $xisbayar, $xislock, $row->idx);
//                    }
//                }
////                foreach ($qsetting->result() as $row) {
////                    $xStr = $this->modelitembayar->setInsertitembayar($xidx, $xidsiswa, $this->noinvoice(), $row->namabiaya, $row->nominal, '', 'N', $row->idjenisbiaya);
////                }
//            }
//        }
//        echo json_encode(null);
//    }
//
//    function simpanprestasisiswa() {
//        $this->load->helper('json');
//        $this->load->helper('common');
//
//        $xnamalomba = $_POST['ednamalomba'];
//        $xpenyelenggara = $_POST['edpenyelenggara'];
//        $xtingkat = $_POST['edtingkat'];
//        $xprestasi = $_POST['edprestasi'];
//        $xidsiswa = $_POST['edidsiswa'];
//        $xfoto = $_POST['edfoto'];
//        $xfoto = $this->renamefileupload($xfoto, $xidsiswa . '-' . $xprestasi . '-' . $xtingkat . $xnamalomba);
//        $this->basemodel->imageresize(150, 150, $xfoto);
//
//        $this->load->model('modelprestasi');
//        $xStr = $this->modelprestasi->setInsertprestasi('', $xnamalomba, $xpenyelenggara, $xtingkat, $xprestasi, $xidsiswa, $xfoto);
//        echo json_encode(null);
//    }
//
//    function renamefileupload($xfile, $newfilename, $folderpath = 'resource/uploaded/img/') {
//        $axfilegambar = explode('.', $xfile);
//        $filegambar = $newfilename . '.' . end($axfilegambar);
//        $source = rename($folderpath . $xfile, $folderpath . $newfilename . '.' . end($axfilegambar));
//        return $filegambar;
//    }
//
//    function createtagihansppsmpreginapacis($xidsiswa) {
//        // $this->load->helper('json');
//        $this->load->helper('common');
//        $this->load->model('modelsiswa');
//        $rowsiswa = $this->modelsiswa->getDetailsiswa($xidsiswa);
//
//        // $xidsiswa = $_POST['edidsiswa'];
////        $xpassword = $_POST['edpassword'];
//        $this->load->model('modelitembayar');
//        $this->load->model('modelsettingbiaya');
//        $invoicetunggal = $this->noinvoice();
//        $cekitembayarpendaftraran = $this->modelitembayar->getListitembayarbyidsiswapendaftaran($xidsiswa, 2)->num_rows();
//        $qsetting = $this->modelsettingbiaya->getListsettingbiayabyidjeis(0, 20, 2);
//        if ($cekitembayarpendaftraran <= 0) {
//            if ($rowsiswa->idasalsekolah == '1') {
//                $detailbiaya = $this->modelsettingbiaya->getDetailsettingbiaya(2);
//            }
//            if ($rowsiswa->idasalsekolah != '1') {
//                $detailbiaya = $this->modelsettingbiaya->getDetailsettingbiaya(3);
//            }
//            $xStr = $this->modelitembayar->setInsertitembayar('', $xidsiswa, $invoicetunggal, $detailbiaya->namabiaya, $detailbiaya->nominal, '', 'N', 2);
//
//            foreach ($qsetting->result() as $row) {
//
//                if ($row->idx != '2' && $row->idx != '3') {
//                    $xStr = $this->modelitembayar->setInsertitembayar('', $xidsiswa, $invoicetunggal, $row->namabiaya, $row->nominal, '', 'N', 2);
//                }
//            }
//        }
//
//        // echo json_encode($this->jsno_data);
//    }
//
//    /*
//     * End Save/simpan
//     */
//    /*
//     * Start Upload Scan
//     */
//
//    function bayarbeliform() {
//        $xidsiswa = $_POST['edidsiswa'];
//        $xiditembayar = $_POST['edidjenisupload'];
//        $xnominal = $_POST['ednominal'];
//        $this->load->helper('json');
//        $this->load->helper('form');
//        $this->load->helper('common');
//        $this->load->model('modeljenisbiaya');
//        $this->load->model('modelsiswa');
//        $this->load->model('modelbank');
//
//        $datasiswa = $this->modelsiswa->getDetailsiswa($xidsiswa);
//        $xBufResult = '';
//        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0">';
//        $xBufResult .= '<input type="hidden" name="edidsiswa" id="edidsiswa" value="' . $xidsiswa . '">';
//        $xBufResult .= setFormfrontend('idsiswa', 'Nama Siswa', form_input_frontend_(getArrayObj('ednamasiswa', $datasiswa->namalenkap, '100%'), '', ' class="curency form-control" placeholder="totalbayar" disabled '));
//        $xBufResult .= setFormfrontend('ednominal', 'Nominal', form_input_frontend_(getArrayObj('edtotalbayar', number_format($xnominal, 2, ",", "."), '100%'), '', ' class="curency form-control" placeholder="totalbayar" disabled '));
////        $xBufResult .= setFormfrontend('idjenisupload', 'Jenis Upload', form_input_frontend_(getArrayObj('edidjenisupload', $xnominal, '100%'), '', ' class="curency form-control" placeholder="totalbayar" disabled '));
//        $xBufResult .= setFormfrontend('idjenisbiaya', 'Jenis Biaya', form_dropdown('edidjenisbiaya', $this->modeljenisbiaya->getArraylistjenisbiaya(), $xiditembayar, ' id="edidjenisbiaya" class="curency form-control" disabled'));
//        $xBufResult .= setFormfrontend('edtanggaltransfer', 'Tanggal Transfer *', form_input_frontend_(getArrayObj('edtanggaltransfer', '', '100%'), '', ' class="curency form-control tanggalkonfirmasi" placeholder="DD-MM-YYYY"  required="required"'));
//        $xBufResult .= setFormfrontend('ednamapemilikrekening', 'Nama Rek Pengirim *', form_input_frontend_(getArrayObj('ednamapemilikrekening', '', '100%'), '', ' class="curency form-control" placeholder="Nama pemilik rek pengirim"  required="required"'));
//        $xBufResult .= setFormfrontend('edidbank', 'Tujuan Bank Sekolah', form_dropdown('edidbank', $this->modelbank->getArraylistbank(), $xiditembayar, ' id="edidbank" class="curency form-control" disabled="disabled" required="required"'));
////        $xBufResult .= form_input_frontend_(getArrayObj('edtotalbayar', $xnominal, '100%'), '', ' class="curency form-control" placeholder="totalbayar" ') . '<div class="spacer"></div>';
//        $xBufResult .= setFormfrontend('Foto', 'Foto/Scan Bukti Transfer *', '<div id="uploadarea" style="width:150px;height:150px">' . form_input_frontend_(getArrayObj('edurl', @$rowcalonsiswa->photo, '100'), '', 'alt="Unggah"  class="form-control" required="required"') . '</div><br><br><br>Scan Bukti Transfer') . '</div>'
//                . ' <div class="form-group row mt-5">
//            <div class="col text-center ">
//                <button type="submit" class="btn btn-lg btn-ursuline px-5" onclick="dosimpanbayarpembelianform();">Save</button>
//            </div>
//        </div> <script>
// $(document).ready(function () {
//
//
//        var currentTimeAndDate = new Date();
//        var Date30 = new Date();
//        var date = new Date();
//        Date30.setDate(Date30.getDate() - 30);
//
//
//
//        var dd = date.getDate();
//        var mm = date.getMonth();
//        var yy = date.getYear();
//
//        var hh = date.getHours();
//        var mnt = date.getMinutes();
//
//        var dd30 = Date30.getDate();
//        var mm30 = Date30.getMonth();
//        var yy30 = Date30.getYear();
//
//        yy = (yy < 1000) ? yy + 1900 : yy;
//        yy30 = (yy30 < 1000) ? yy30 + 1900 : yy30;
//
//
//        $(document).on("focus", ".tanggalkonfirmasi", function () {
//            $(".tanggalkonfirmasi").datepicker({
//                showOtherMonths: true,
//                selectOtherMonths: true,
//                showButtonPanel: true,
//                changeMonth: true,
//                changeYear: true,
//                yearRange: "2000:2020",
//                dateFormat: "dd-mm-yy"
//            });
//        });
//
//
//        $(".tanggalkonfirmasi").val(strpad(dd) + "-" + strpad(mm + 1) + "-" + yy);
//
//    });
//</script>';
////        $xBufResult .= form_button('btBayar', 'Bayar', 'onclick="dosimpanpembayaran(\'' . $xiditembayar . '\');"');
//        $this->json_data['data'] = $xBufResult;
//        echo json_encode($this->json_data);
//    }
//
//    function simpanpembayaran() {
//        $this->load->helper('json');
//        $this->load->helper('common');
//        if (!empty($_POST['edidx'])) {
//            $xidx = $_POST['edidx'];
//        } else {
//            $xidx = '0';
//        }
//        $this->load->model('modelverifikasipembayaran');
//        $xidsiswa = $_POST['edidsiswa'];
//        $xkodependaftaran = $_POST['edkodependaftaran'];
//        $xnamapemilikrekening = $_POST['ednamapemilikrekening'];
//        $xtanggaltransfer = '';
//        if ($xtanggaltransfer != '' || $xtanggaltransfer != 'undefined') {
//            $xtanggaltransferawal = $_POST['edtanggaltransfer'];
//            $xtanggaltransfer = mysqltodate($xtanggaltransferawal);
//        }
//
//        $now = new DateTime();
//        $tglnow = $now->format('Y-m-d');
//        $xfile = $_POST['edfile'];
//        $xidbank = $_POST['edidbank'];
//        $xidjenisbiaya = $_POST['edidjenisbiaya'];
//        $filegambar = $tglnow . '-' . $xidsiswa . 'verifikasipembayaran';
//        $xfoto = $this->renamefileupload($xfile, $filegambar);
//        $this->basemodel->imageresize(150, 150, $xfoto);
//
//        $idpegawai = $this->session->userdata('idsiswa');
//        if (!empty($idpegawai)) {
//            if ($xidx != '0') {
//                $xStr = $this->modelverifikasipembayaran->setUpdateverifikasipembayaran($xidx, $xidsiswa, $xkodependaftaran, $xnamapemilikrekening, $xtanggaltransfer, '', $xfoto, $xidbank, $xidjenisbiaya);
//            } else {
//                $xStr = $this->modelverifikasipembayaran->setInsertverifikasipembayaran($xidx, $xidsiswa, $xkodependaftaran, $xnamapemilikrekening, $xtanggaltransfer, '', $xfoto, $xidbank, $xidjenisbiaya);
//            }
//        }
//
//        echo json_encode(null);
//    }
//
//    /*
//     * End Upload Scan
//     */
//
//    function nopendaftaran() {
//        $this->load->helper('json');
//        $tahunajar = $this->session->userdata('tahunajar');
//        $idgelombang = $this->session->userdata('idgelombang');
//        $idkategoridaftar = $this->session->userdata('idkategoridaftar');
//        $this->load->model('modelpendaftaran');
//        $rownomor = $this->modelpendaftaran->getLastIndexpendaftaran();
//        $intnomor = substr(@$rownomor->koependaftaran, -5) + 1;
//        $nopendaftaran = $tahunajar . $idgelombang . $this->number_pad($intnomor, 5);
//        $ceknodaftar = $this->modelpendaftaran->getDetailpendaftaranbynopendaftaran($nopendaftaran);
////        if (!empty($ceknodaftar->idx)) {
////            $this->nopendaftaran();
////        }
//        return $nopendaftaran;
//    }

}

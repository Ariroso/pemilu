<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : pemilih   *  By Diar */

class Ctrpemilih extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformpemilih('0', $xAwal);
    }

    function createformpemilih($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxadmin.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxpemilih.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormpemilih($xidx), '', '', $xAddJs, '', 'pemilih');
    }

    function setDetailFormpemilih($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrpemilih/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelpendidikan');
        $this->load->model('modelpekerjaan');
        $this->load->model('modeljabatan');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modeltahun');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidanggota" id="edidanggota" value="0" />';

        $xBufResult .= setForm('nokk', 'nokk', form_input_(getArrayObj('ednokk', '', '200'), '', ' placeholder="nokk" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('nik', 'nik', form_input_(getArrayObj('ednik', '', '200'), '', ' placeholder="nik" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('nama', 'nama', form_input_(getArrayObj('ednama', '', '200'), '', ' placeholder="nama" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('tempatlahir', 'tempatlahir', form_input_(getArrayObj('edtempatlahir', '', '200'), '', ' placeholder="tempatlahir" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('tgllahir', 'tgllahir', form_input_(getArrayObj('edtgllahir', '', '200'), '', ' class="form-control tanggal" placeholder="tgllahir" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('perkawinan', 'perkawinan', form_dropdown_('edperkawinan', getArrayperkawinan(), '', ' id="edperkawinan" placeholder="perkawinan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('jeniskelamin', 'jeniskelamin', form_dropdown_('edjeniskelamin', getArraygender(), '', ' id="edjeniskelamin" placeholder="jeniskelamin" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpendidikan', 'idpendidikan', form_dropdown_('edidpendidikan', $this->modelpendidikan->getArraylistpendidikan(), '', ' id="edidpendidikan" placeholder="idpendidikan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idpekerjaan', 'idpekerjaan', form_dropdown_('edidpekerjaan', $this->modelpekerjaan->getArraylistpekerjaan(), '', ' id="edidpekerjaan" placeholder="idpekerjaan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idjabatan', 'idjabatan', form_dropdown_('edidjabatan', $this->modeljabatan->getArraylistjabatan(), '', ' placeholder="idjabatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('alamat', 'alamat', form_input_(getArrayObj('edalamat', '', '400'), '', ' placeholder="alamat" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('rt', 'rt', form_input_(getArrayObj('edrt', '', '200'), '', ' placeholder="rt" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('rw', 'rw', form_input_(getArrayObj('edrw', '', '200'), '', ' placeholder="rw" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('disabilitas', 'disabilitas', form_dropdown_('eddisabilitas', getArrayYaTidak(), '', ' id="eddisabilitas" placeholder="disabilitas" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('keterangan', 'keterangan', form_textarea(getArrayObj('edketerangan', '', '200'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('idanggota', 'idanggota', form_input_(getArrayObj('edidanggota', '', '200'), '', ' placeholder="idanggota" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanpemilih();"') . form_button('btNew', 'new', 'onclick="doClearpemilih();"') . '<div class="spacer"></div><div id="tabledatapemilih">' . $this->getlistpemilih(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistpemilih($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('nokk', '', 'data-field="nokk" data-sortable="true" width=10%') .
                tbaddcellhead('nik', '', 'data-field="nik" data-sortable="true" width=10%') .
                tbaddcellhead('nama', '', 'data-field="nama" data-sortable="true" width=10%') .
                tbaddcellhead('tempatlahir', '', 'data-field="tempatlahir" data-sortable="true" width=10%') .
                tbaddcellhead('tgllahir', '', 'data-field="tgllahir" data-sortable="true" width=10%') .
                tbaddcellhead('perkawinan', '', 'data-field="perkawinan" data-sortable="true" width=10%') .
                tbaddcellhead('jeniskelamin', '', 'data-field="jeniskelamin" data-sortable="true" width=10%') .
                tbaddcellhead('idpendidikan', '', 'data-field="idpendidikan" data-sortable="true" width=10%') .
                tbaddcellhead('idpekerjaan', '', 'data-field="idpekerjaan" data-sortable="true" width=10%') .
                tbaddcellhead('idjabatan', '', 'data-field="idjabatan" data-sortable="true" width=10%') .
                tbaddcellhead('alamat', '', 'data-field="alamat" data-sortable="true" width=10%') .
                tbaddcellhead('rt', '', 'data-field="rt" data-sortable="true" width=10%') .
                tbaddcellhead('rw', '', 'data-field="rw" data-sortable="true" width=10%') .
                tbaddcellhead('disabilitas', '', 'data-field="disabilitas" data-sortable="true" width=10%') .
                tbaddcellhead('keterangan', '', 'data-field="keterangan" data-sortable="true" width=10%') .
                tbaddcellhead('iddapil', '', 'data-field="iddapil" data-sortable="true" width=10%') .
                tbaddcellhead('idtps', '', 'data-field="idtps" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('idanggota', '', 'data-field="idanggota" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modelpemilih');
        $xQuery = $this->modelpemilih->getListpemilih($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditpemilih(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapuspemilih(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->nokk) .
                    tbaddcell($row->nik) .
                    tbaddcell($row->nama) .
                    tbaddcell($row->tempatlahir) .
                    tbaddcell($row->tgllahir) .
                    tbaddcell($row->perkawinan) .
                    tbaddcell($row->jeniskelamin) .
                    tbaddcell($row->idpendidikan) .
                    tbaddcell($row->idpekerjaan) .
                    tbaddcell($row->idjabatan) .
                    tbaddcell($row->alamat) .
                    tbaddcell($row->rt) .
                    tbaddcell($row->rw) .
                    tbaddcell($row->disabilitas) .
                    tbaddcell($row->keterangan) .
                    tbaddcell($row->iddapil) .
                    tbaddcell($row->idtps) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($row->idanggota) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchpemilih(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchpemilih(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchpemilih(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistpemilihAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['nokk'] = "";
        $this->json_data['nik'] = "";
        $this->json_data['nama'] = "";
        $this->json_data['tempatlahir'] = "";
        $this->json_data['tgllahir'] = "";
        $this->json_data['perkawinan'] = "";
        $this->json_data['jeniskelamin'] = "";
        $this->json_data['idpendidikan'] = "";
        $this->json_data['idpekerjaan'] = "";
        $this->json_data['idjabatan'] = "";
        $this->json_data['alamat'] = "";
        $this->json_data['rt'] = "";
        $this->json_data['rw'] = "";
        $this->json_data['disabilitas'] = "";
        $this->json_data['keterangan'] = "";
        $this->json_data['iddapil'] = "";
        $this->json_data['idtps'] = "";
        $this->json_data['idtahun'] = "";
        $this->json_data['idanggota'] = "";

        $response = array();
        $this->load->model('modelpemilih');
        $xQuery = $this->modelpemilih->getListpemilih($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['nokk'] = $row->nokk;
            $this->json_data['nik'] = $row->nik;
            $this->json_data['nama'] = $row->nama;
            $this->json_data['tempatlahir'] = $row->tempatlahir;
            $this->json_data['tgllahir'] = $row->tgllahir;
            $this->json_data['perkawinan'] = $row->perkawinan;
            $this->json_data['jeniskelamin'] = $row->jeniskelamin;
            $this->json_data['idpendidikan'] = $row->idpendidikan;
            $this->json_data['idpekerjaan'] = $row->idpekerjaan;
            $this->json_data['idjabatan'] = $row->idjabatan;
            $this->json_data['alamat'] = $row->alamat;
            $this->json_data['rt'] = $row->rt;
            $this->json_data['rw'] = $row->rw;
            $this->json_data['disabilitas'] = $row->disabilitas;
            $this->json_data['keterangan'] = $row->keterangan;
            $this->json_data['iddapil'] = $row->iddapil;
            $this->json_data['idtps'] = $row->idtps;
            $this->json_data['idtahun'] = $row->idtahun;
            $this->json_data['idanggota'] = $row->idanggota;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpanpemilihAndroid() {
        $xidx = $_POST['edidx'];
        $xnokk = $_POST['ednokk'];
        $xnik = $_POST['ednik'];
        $xnama = $_POST['ednama'];
        $xtempatlahir = $_POST['edtempatlahir'];
        $xtgllahir = $_POST['edtgllahir'];
        $xperkawinan = $_POST['edperkawinan'];
        $xjeniskelamin = $_POST['edjeniskelamin'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xalamat = $_POST['edalamat'];
        $xrt = $_POST['edrt'];
        $xrw = $_POST['edrw'];
        $xdisabilitas = $_POST['eddisabilitas'];
        $xketerangan = $_POST['edketerangan'];
        $xiddapil = $_POST['ediddapil'];
        $xidtps = $_POST['edidtps'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->helper('json');
        $this->load->model('modelpemilih');
        $response = array();
        if ($xidx != '0') {
            $this->modelpemilih->setUpdatepemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, $xtgllahir, $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
        } else {
            $this->modelpemilih->setInsertpemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, $xtgllahir, $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
        }
        $row = $this->modelpemilih->getLastIndexpemilih();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['nokk'] = $row->nokk;
        $this->json_data['nik'] = $row->nik;
        $this->json_data['nama'] = $row->nama;
        $this->json_data['tempatlahir'] = $row->tempatlahir;
        $this->json_data['tgllahir'] = $row->tgllahir;
        $this->json_data['perkawinan'] = $row->perkawinan;
        $this->json_data['jeniskelamin'] = $row->jeniskelamin;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['rt'] = $row->rt;
        $this->json_data['rw'] = $row->rw;
        $this->json_data['disabilitas'] = $row->disabilitas;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecpemilih() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelpemilih');
        $row = $this->modelpemilih->getDetailpemilih($xIdEdit);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['nokk'] = $row->nokk;
        $this->json_data['nik'] = $row->nik;
        $this->json_data['nama'] = $row->nama;
        $this->json_data['tempatlahir'] = $row->tempatlahir;
        $this->json_data['tgllahir'] = mysqltodate($row->tgllahir);
        $this->json_data['perkawinan'] = $row->perkawinan;
        $this->json_data['jeniskelamin'] = $row->jeniskelamin;
        $this->json_data['idpendidikan'] = $row->idpendidikan;
        $this->json_data['idpekerjaan'] = $row->idpekerjaan;
        $this->json_data['idjabatan'] = $row->idjabatan;
        $this->json_data['alamat'] = $row->alamat;
        $this->json_data['rt'] = $row->rt;
        $this->json_data['rw'] = $row->rw;
        $this->json_data['disabilitas'] = $row->disabilitas;
        $this->json_data['keterangan'] = $row->keterangan;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        echo json_encode($this->json_data);
    }

    function deletetablepemilih() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelpemilih');
        $this->modelpemilih->setDeletepemilih($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchpemilih() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatapemilih'] = $this->getlistpemilih($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpanpemilih() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xnokk = $_POST['ednokk'];
        $xnik = $_POST['ednik'];
        $xnama = $_POST['ednama'];
        $xtempatlahir = $_POST['edtempatlahir'];
        $xtgllahir = $_POST['edtgllahir'];
        $xperkawinan = $_POST['edperkawinan'];
        $xjeniskelamin = $_POST['edjeniskelamin'];
        $xidpendidikan = $_POST['edidpendidikan'];
        $xidpekerjaan = $_POST['edidpekerjaan'];
        $xidjabatan = $_POST['edidjabatan'];
        $xalamat = $_POST['edalamat'];
        $xrt = $_POST['edrt'];
        $xrw = $_POST['edrw'];
        $xdisabilitas = $_POST['eddisabilitas'];
        $xketerangan = $_POST['edketerangan'];
        $xiddapil = $_POST['ediddapil'];
        $xidtps = $_POST['edidtps'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->model('modelpemilih');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelpemilih->setUpdatepemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, datetomysql($xtgllahir), $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
            } else {
                $xStr = $this->modelpemilih->setInsertpemilih($xidx, $xnokk, $xnik, $xnama, $xtempatlahir, datetomysql($xtgllahir), $xperkawinan, $xjeniskelamin, $xidpendidikan, $xidpekerjaan, $xidjabatan, $xalamat, $xrt, $xrw, $xdisabilitas, $xketerangan, $xiddapil, $xidtps, $xidtahun, $xidanggota);
            }
        }
        echo json_encode(null);
    }

}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ctrview
 *
 * @author scriptmedia
 */
if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
date_default_timezone_set('Asia/Jakarta');
setlocale(LC_TIME, 'id_ID');

class show extends CI_Controller {

    private $xtanggal;
    private $xxgelombang;
    private $xxtahun;

    function __construct() {
        parent::__construct();
        $this->load->model('basemodel');
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('modeltahun');
        $this->xtanggal = date('Y-m-d'); // Hasil: 20-01-2017 05:32:15
        $setth = $this->modeltahun->getDetailtahunnow();
//        $this->xxtahun = $setth->tahun;
//        $this->session->set_userdata('idtahun', $setth->idx);
//        $this->session->set_userdata('tahun', $setth->tahun);
    }

    public function index() {
//        if (!$this->session->userdata('idpegawai')) {
//            exit();
//        }
        $data['maincontent'] = '';
//        $data['maincontent'] = $this->basemodel->moduledetail(120);
        $data['maincontent'] = '';

//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header('', 1);
        $data['slider'] = $this->slider();
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = '';
        $data['footer'] .= $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    public function persyaratan() {
        $data['maincontent'] = $this->basemodel->moduledetail(121);

//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header('', 2);
        $data['slider'] = '';
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    public function persyaratan2() {
        $data['maincontent'] = $this->basemodel->moduledetail(122);

//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header();
        $data['slider'] = '';
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    public function persyaratan3() {
        $data['maincontent'] = $this->basemodel->moduledetail(123);

//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header();
        $data['slider'] = '';
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    public function prosedur() {
        $data['maincontent'] = $this->basemodel->moduledetail(124);

//        $data['sidebar']= $this->sidebar();
        $data['header'] = $this->basemodel->header('', 3);
        $data['slider'] = '';
//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    public function login() {
        if ($this->session->userdata('idanggota'))
            redirect(site_url() . "/myaccount/");
        $data['maincontent'] = $this->load->view('scriptmedia/login', '', TRUE);
        $data['header'] = $this->basemodel->header('', 5);
        $data['slider'] = '';
        $data['footer'] = $this->basemodel->footer();
        $this->load->view('scriptmedia/index', $data);
    }

    public function Forgotpassword() {
//        $pendaftaran = $this->modelgelombang->getpendaftaranbuka($this->xtanggal);
//        $g = $this->modelgelombang->getgelombangnow($xtanggal);
//        $this->load->model('modelsettingtemplatenotif');
//        $this->load->model('modeltemplatenotif');
//        $settingtemplatependaftarantutup = $this->modelsettingtemplatenotif->getDetailsettingtemplate(1);
//        $templatependaftaran = $this->modeltemplatenotif->getDetailtemplatebyidsetting($settingtemplatependaftarantutup->idx);
//
//        if ($pendaftaran) {
        $data['maincontent'] = $this->load->view('scriptmedia/Forgotpassword', '', TRUE);
//        } else {
//            $data['maincontent'] = $templatependaftaran->template;
//        }

        $data['header'] = $this->basemodel->header('<script  language="javascript" type="text/javascript" src="' . base_url() . '/resource/js/jquery/ui/jquery.min.js"> </script> ' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . '/resource/js/jquery/ui/jquery-ui.js"> </script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . '/resource/ajax/ajaxdaftarawal.js"> </script>');
        $data['slider'] = '';
        $data['footer'] = $this->basemodel->footer();
        $this->load->view('scriptmedia/index', $data);
    }

    public function pendaftaran() {
        $gel = $this->session->userdata('idgelombang');
        $pendaftaran = $this->modelgelombang->getpendaftaranbuka($this->xtanggal);
        /* ===
         * Cek jika gelombang sedang tidak dibuka....
         */
        $this->load->model('modelsettingtemplatenotif');
        $this->load->model('modeltemplatenotif');
        $settingtemplatependaftarantutup = $this->modelsettingtemplatenotif->getDetailsettingtemplate(1);
        $templatependaftaran = $this->modeltemplatenotif->getDetailtemplatebyidsetting($settingtemplatependaftarantutup->idx);

        if ($pendaftaran) {
            $data['maincontent'] = $this->load->view('scriptmedia/pendaftaran', '', TRUE);
        } else {
            $data['maincontent'] = $templatependaftaran->template;
        }

        $data['sidebar'] = '';
        $data['header'] = $this->basemodel->header('', 4);

        //$data['header'] = $this->basemodel->header('<link  href="'. base_url().'/resource/js/jquery/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">');
        $data['header'] = $this->basemodel->header(
                // '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/ui/jquery.min.js"> </script> ' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/jquery/ui/jquery-ui.js"> </script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script  language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsiswadaftarawal.js"> </script>');
//        $data['header'] = $this->basemodel->header('<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>'. "\n" .'');
//        $data['header'] = $this->basemodel->header('');
        $data['slider'] = '';

//        $data['footerwidget']= $this->footerwidget();
        $data['footer'] = $this->basemodel->footer();

        $this->load->view('scriptmedia/index', $data);
    }

    function slider() {
        $this->load->model('modelimgslide');
        $this->load->model('basemodel');
        $qslider = $this->modelimgslide->getListimgslide(0, 10);
        $data = [];
        foreach ($qslider->result() as $row) {
            $data['qslider'][] = array(
                'image' => (!empty($row->url)) ? $row->url : '',
                'keterangan' => $row->keterangan,
                'link' => $row->link
            );
        }
        //$data['qslide']=$qslider;
        return $this->load->view("scriptmedia/slider", $data, TRUE);
    }

    function loginsiswa() {
        if (!empty($this->session->userdata('idanggota'))) {
            redirect(site_url() . "/myaccount/");
        }
        $xuser = $_POST['edUser'];
        $xPassword = $_POST['edPassword'];

        $this->load->helper('json');
        $this->load->model('modelanggota');
//        $this->load->model('modelgelombang');
        $this->load->model('modeltahun');
//        $this->load->model('modeltanggalpengumuman');
        $this->json_data['data'] = false;
//        $pendaftaran = $this->modelgelombang->getpendaftaranbuka($this->xtanggal);
//        $xgelombang = $this->session->userdata('idgelombang');
//        $pengumuman = $this->modeltanggalpengumuman->gettanggalpengumumannow();
        $tahun = $this->session->userdata('idtahun');

        $row = $this->modelanggota->getlogin($xuser, $xPassword);
        if ($row) {
            $this->session->set_userdata('idanggota', $row->idx);
            $this->session->set_userdata('iddapil', $row->iddapil);
            $this->session->set_userdata('idtps', $row->idtps);
            $this->session->set_userdata('nama', ucwords(strtolower($row->namalengkap)));
            $this->json_data['data'] = true;
            $this->json_data['location'] = site_url() . "/myaccount/";
        } else {
            $this->json_data['location'] = site_url() . "/show/";
        }
        echo json_encode($this->json_data);
    }

//    function loginguru() {
//        $datalogin['penggunalogin'] = 'guru';
//        $xidx = '';
//        $xpassword = '';
//        $xeuser_pengguna = '';
//        $xpassword = (isset($_POST['edpassword_guru'])) ? $_POST['edpassword_guru'] : '';
//        $xeuser_pengguna = (isset($_POST['eduser_guru'])) ? $_POST['eduser_guru'] : '';
//        $this->load->model('modelguru');
//        $user = $this->modelguru->getloginguru($xeuser_pengguna, $xpassword);
//        $this->json_data['data'] = false;
//        if (!empty($user)) {
//            $this->session->set_userdata('statuslogin', 'guru');
//            $this->session->set_userdata('nama', $user->NamaGuru);
//            $this->session->set_userdata('pengguna', $user->idx);
//            $this->json_data['data'] = true;
//            $this->json_data['location'] = site_url() . "";
//            $datalogin['error'] = $this->session->userdata('nama');
//            redirect(base_url() . 'index.php/ctrmyaccountguru');
//        } else
//            $datalogin['error'] = '';
//
//        $data['maincontent'] = $this->load->view('sma1wates/login', $datalogin, TRUE);
//        $data['sidebar'] = $this->sidebar();
//        $data['header'] = $this->basemodel->header();
//        $data['slider'] = '';
//        $data['footerwidget'] = $this->footerwidget();
//        $data['footer'] = $this->basemodel->footer('');
//        $this->load->view('sma1wates/index', $data);
//    }

    function logout() {
        $this->session->sess_destroy();
        return redirect(site_url());
    }

}

?>

<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : jabatan   *  By Diar */

class Ctrjabatan extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformjabatan('0', $xAwal);
    }

    function createformjabatan($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxjabatan.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormjabatan($xidx), '', '', $xAddJs, '', 'jabatan');
    }

    function setDetailFormjabatan($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrjabatan/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('jabatan', 'jabatan', form_input_(getArrayObj('edjabatan', '', '200'), '', ' placeholder="jabatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('keterangan', 'keterangan', form_textarea_(getArrayObj('edketerangan', '', '200'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpanjabatan();"') . form_button('btNew', 'new', 'onclick="doClearjabatan();"') . '<div class="spacer"></div><div id="tabledatajabatan">' . $this->getlistjabatan(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistjabatan($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('jabatan', '', 'data-field="jabatan" data-sortable="true" width=10%') .
                tbaddcellhead('keterangan', '', 'data-field="keterangan" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modeljabatan');
        $xQuery = $this->modeljabatan->getListjabatan($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditjabatan(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapusjabatan(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->jabatan) .
                    tbaddcell($row->keterangan) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchjabatan(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchjabatan(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchjabatan(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistjabatanAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['jabatan'] = "";
        $this->json_data['keterangan'] = "";

        $response = array();
        $this->load->model('modeljabatan');
        $xQuery = $this->modeljabatan->getListjabatan($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['jabatan'] = $row->jabatan;
            $this->json_data['keterangan'] = $row->keterangan;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpanjabatanAndroid() {
        $xidx = $_POST['edidx'];
        $xjabatan = $_POST['edjabatan'];
        $xketerangan = $_POST['edketerangan'];

        $this->load->helper('json');
        $this->load->model('modeljabatan');
        $response = array();
        if ($xidx != '0') {
            $this->modeljabatan->setUpdatejabatan($xidx, $xjabatan, $xketerangan);
        } else {
            $this->modeljabatan->setInsertjabatan($xidx, $xjabatan, $xketerangan);
        }
        $row = $this->modeljabatan->getLastIndexjabatan();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jabatan'] = $row->jabatan;
        $this->json_data['keterangan'] = $row->keterangan;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecjabatan() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modeljabatan');
        $row = $this->modeljabatan->getDetailjabatan($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['jabatan'] = $row->jabatan;
        $this->json_data['keterangan'] = $row->keterangan;

        echo json_encode($this->json_data);
    }

    function deletetablejabatan() {
        $edidx = $_POST['edidx'];
        $this->load->model('modeljabatan');
        $this->modeljabatan->setDeletejabatan($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchjabatan() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatajabatan'] = $this->getlistjabatan($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpanjabatan() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xjabatan = $_POST['edjabatan'];
        $xketerangan = $_POST['edketerangan'];

        $this->load->model('modeljabatan');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modeljabatan->setUpdatejabatan($xidx, $xjabatan, $xketerangan);
            } else {
                $xStr = $this->modeljabatan->setInsertjabatan($xidx, $xjabatan, $xketerangan);
            }
        }
        echo json_encode(null);
    }

}

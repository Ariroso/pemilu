<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : setting   *  By Diar */

class Ctrsetting extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformsetting('0', $xAwal);
    }

    function createformsetting($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                link_tag('resource/css/admin/upload/css/upload.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.knob.js"></script>' . "\n" .
//                        '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.iframe-transport.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.fileupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/myupload.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsetting.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormsetting($xidx), '', '', $xAddJs, '', 'setting');
    }

    function setDetailFormsetting($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrsetting/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modelkecamatan');
        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';

        $xBufResult .= setForm('Nama', 'Nama Sekolah', form_input_(getArrayObj('edNama', '', '200'), '', ' placeholder="Nama" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('Alamat', 'Alamat', form_input_(getArrayObj('edAlamat', '', '400'), '', ' placeholder="Alamat" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('email', 'email', form_input_(getArrayObj('edemail', '', '200'), '', ' placeholder="email" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('portemail', 'portemail', form_input_(getArrayObj('edportemail', '', '50'), '', ' placeholder="portemail" '), 'port smtp default 465') . '<div class="spacer"></div>';

        $xBufResult .= setForm('Telpon', 'Telpon', form_input_(getArrayObj('edTelpon', '', '200'), '', ' placeholder="Telpon" '), 'Dimulai dengan kode negara 62') . '<div class="spacer"></div>';

        $xBufResult .= setForm('Whatsapp', 'Whatsapp', form_input_(getArrayObj('edWhatsapp', '', '200'), '', ' placeholder="Whatsapp" '), 'Dimulai dengan kode negara 62') . '<div class="spacer"></div>';
        $xBufResult .= '<div id="kecamatan">' . setForm('edkecamatan', '</b>Kecamatan', form_dropdown('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), '', 'id="edidkecamatan"')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('idkecamatan', 'idkecamatan', form_input_(getArrayObj('edidkecamatan', '', '200'), '', ' placeholder="idkecamatan" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('Bio', 'Bio', form_textarea_(getArrayObj('edBio', '', '200'), '', ' placeholder="Bio" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('logo', 'logo', form_input_(getArrayObj('edlogo', '', '200'), '', ' placeholder="logo" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('Logo', 'Logo', '<div id="uploadarea" style="width:150px;height:150px">' . form_input_(getArrayObj('edurl', '', '100'), '', 'alt="Upload Logo"') . '</div>');
        $xBufResult .= setForm('fb', 'fb', form_input_(getArrayObj('edfb', '', '200'), '', ' placeholder="fb" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('ig', 'ig', form_input_(getArrayObj('edig', '', '200'), '', ' placeholder="ig" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('tw', 'tw', form_input_(getArrayObj('edtw', '', '200'), '', ' placeholder="tw" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('G', 'G', form_input_(getArrayObj('edG', '', '200'), '', ' placeholder="G" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('youtube', 'youtube', form_input_(getArrayObj('edyoutube', '', '200'), '', ' placeholder="youtube" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpansetting();"') . form_button('btNew', 'new', 'onclick="doClearsetting();"') . '<div class="spacer"></div><div id="tabledatasetting">' . $this->getlistsetting(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistsetting($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('Nama', '', 'data-field="Nama" data-sortable="true" width=10%') .
                tbaddcellhead('Alamat', '', 'data-field="Alamat" data-sortable="true" width=10%') .
                tbaddcellhead('email', '', 'data-field="email" data-sortable="true" width=10%') .
                tbaddcellhead('portemail', '', 'data-field="portemail" data-sortable="true" width=10%') .
                tbaddcellhead('Telpon', '', 'data-field="Telpon" data-sortable="true" width=10%') .
                tbaddcellhead('Whatsapp', '', 'data-field="Whatsapp" data-sortable="true" width=10%') .
                tbaddcellhead('idkecamatan', '', 'data-field="idkecamatan" data-sortable="true" width=10%') .
                tbaddcellhead('Bio', '', 'data-field="Bio" data-sortable="true" width=10%') .
                tbaddcellhead('logo', '', 'data-field="logo" data-sortable="true" width=10%') .
                tbaddcellhead('fb', '', 'data-field="fb" data-sortable="true" width=10%') .
                tbaddcellhead('ig', '', 'data-field="ig" data-sortable="true" width=10%') .
                tbaddcellhead('tw', '', 'data-field="tw" data-sortable="true" width=10%') .
                tbaddcellhead('G', '', 'data-field="G" data-sortable="true" width=10%') .
                tbaddcellhead('youtube', '', 'data-field="youtube" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;', 'width:10%;text-align:center;'), '', TRUE);
        $this->load->model('modelsetting');
        $xQuery = $this->modelsetting->getListsetting($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditsetting(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapussetting(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->Nama) .
                    tbaddcell($row->Alamat) .
                    tbaddcell($row->email) .
                    tbaddcell($row->portemail) .
                    tbaddcell($row->Telpon) .
                    tbaddcell($row->Whatsapp) .
                    tbaddcell($row->idkecamatan) .
                    tbaddcell($row->Bio) .
                    tbaddcell($row->logo) .
                    tbaddcell($row->fb) .
                    tbaddcell($row->ig) .
                    tbaddcell($row->tw) .
                    tbaddcell($row->G) .
                    tbaddcell($row->youtube) .
                    tbaddcell($xButtonEdit));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchsetting(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchsetting(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchsetting(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistsettingAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['Nama'] = "";
        $this->json_data['Alamat'] = "";
        $this->json_data['email'] = "";
        $this->json_data['portemail'] = "";
        $this->json_data['Telpon'] = "";
        $this->json_data['Whatsapp'] = "";
        $this->json_data['idkecamatan'] = "";
        $this->json_data['Bio'] = "";
        $this->json_data['logo'] = "";
        $this->json_data['fb'] = "";
        $this->json_data['ig'] = "";
        $this->json_data['tw'] = "";
        $this->json_data['G'] = "";
        $this->json_data['youtube'] = "";

        $response = array();
        $this->load->model('modelsetting');
        $xQuery = $this->modelsetting->getListsetting($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['Nama'] = $row->Nama;
            $this->json_data['Alamat'] = $row->Alamat;
            $this->json_data['email'] = $row->email;
            $this->json_data['portemail'] = $row->portemail;
            $this->json_data['Telpon'] = $row->Telpon;
            $this->json_data['Whatsapp'] = $row->Whatsapp;
            $this->json_data['idkecamatan'] = $row->idkecamatan;
            $this->json_data['Bio'] = $row->Bio;
            $this->json_data['logo'] = $row->logo;
            $this->json_data['fb'] = $row->fb;
            $this->json_data['ig'] = $row->ig;
            $this->json_data['tw'] = $row->tw;
            $this->json_data['G'] = $row->G;
            $this->json_data['youtube'] = $row->youtube;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpansettingAndroid() {
        $xidx = $_POST['edidx'];
        $xNama = $_POST['edNama'];
        $xAlamat = $_POST['edAlamat'];
        $xemail = $_POST['edemail'];
        $xportemail = $_POST['edportemail'];
        $xTelpon = $_POST['edTelpon'];
        $xWhatsapp = $_POST['edWhatsapp'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xBio = $_POST['edBio'];
        $xlogo = $_POST['edlogo'];
        $xfb = $_POST['edfb'];
        $xig = $_POST['edig'];
        $xtw = $_POST['edtw'];
        $xG = $_POST['edG'];
        $xyoutube = $_POST['edyoutube'];

        $this->load->helper('json');
        $this->load->model('modelsetting');
        $response = array();
        if ($xidx != '0') {
            $this->modelsetting->setUpdatesetting($xidx, $xNama, $xAlamat, $xemail, $xportemail, $xTelpon, $xWhatsapp, $xidkecamatan, $xBio, $xlogo, $xfb, $xig, $xtw, $xG, $xyoutube);
        } else {
            $this->modelsetting->setInsertsetting($xidx, $xNama, $xAlamat, $xemail, $xportemail, $xTelpon, $xWhatsapp, $xidkecamatan, $xBio, $xlogo, $xfb, $xig, $xtw, $xG, $xyoutube);
        }
        $row = $this->modelsetting->getLastIndexsetting();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['Nama'] = $row->Nama;
        $this->json_data['Alamat'] = $row->Alamat;
        $this->json_data['email'] = $row->email;
        $this->json_data['portemail'] = $row->portemail;
        $this->json_data['Telpon'] = $row->Telpon;
        $this->json_data['Whatsapp'] = $row->Whatsapp;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['Bio'] = $row->Bio;
        $this->json_data['logo'] = $row->logo;
        $this->json_data['fb'] = $row->fb;
        $this->json_data['ig'] = $row->ig;
        $this->json_data['tw'] = $row->tw;
        $this->json_data['G'] = $row->G;
        $this->json_data['youtube'] = $row->youtube;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecsetting() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelsetting');
        $row = $this->modelsetting->getDetailsetting($xIdEdit);
        $this->load->helper('json');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['Nama'] = $row->Nama;
        $this->json_data['Alamat'] = $row->Alamat;
        $this->json_data['email'] = $row->email;
        $this->json_data['portemail'] = $row->portemail;
        $this->json_data['Telpon'] = $row->Telpon;
        $this->json_data['Whatsapp'] = $row->Whatsapp;
        $this->json_data['idkecamatan'] = $row->idkecamatan;
        $this->json_data['Bio'] = $row->Bio;
        $this->json_data['logo'] = $row->logo;
        $this->json_data['fb'] = $row->fb;
        $this->json_data['ig'] = $row->ig;
        $this->json_data['tw'] = $row->tw;
        $this->json_data['G'] = $row->G;
        $this->json_data['youtube'] = $row->youtube;

        echo json_encode($this->json_data);
    }

    function deletetablesetting() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelsetting');
        $this->modelsetting->setDeletesetting($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchsetting() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatasetting'] = $this->getlistsetting($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpansetting() {
        $this->load->helper('json');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xNama = $_POST['edNama'];
        $xAlamat = $_POST['edAlamat'];
        $xemail = $_POST['edemail'];
        $xportemail = $_POST['edportemail'];
        $xTelpon = $_POST['edTelpon'];
        $xWhatsapp = $_POST['edWhatsapp'];
        $xidkecamatan = $_POST['edidkecamatan'];
        $xBio = $_POST['edBio'];
        $xlogo = $_POST['edlogo'];
        $xfb = $_POST['edfb'];
        $xig = $_POST['edig'];
        $xtw = $_POST['edtw'];
        $xG = $_POST['edG'];
        $xyoutube = $_POST['edyoutube'];

        $this->load->model('modelsetting');
        $idpegawai = $this->session->userdata('idpegawai');
        if (!empty($idpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelsetting->setUpdatesetting($xidx, $xNama, $xAlamat, $xemail, $xportemail, $xTelpon, $xWhatsapp, $xidkecamatan, $xBio, $xlogo, $xfb, $xig, $xtw, $xG, $xyoutube);
//            } else {
//                $xStr = $this->modelsetting->setInsertsetting($xidx, $xNama, $xAlamat, $xemail, $xportemail, $xTelpon, $xWhatsapp, $xidkecamatan, $xBio, $xlogo, $xfb, $xig, $xtw, $xG, $xyoutube);
            }
        }
        echo json_encode(null);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('Tidak Diperkenankan mengakses langsung');
/* Class  Control : suaracalon   *  By Diar */

class Ctrsuaracalon extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($xAwal = 0, $xSearch = '') {
        $idpegawai = $this->session->userdata('idpegawai');
        if (empty($idpegawai)) {
            redirect(site_url(), '');
        }
        if ($xAwal <= -1) {
            $xAwal = 0;
        }
        $this->session->set_userdata('awal', $xAwal);
        $this->session->set_userdata('limit', 100);
        $this->createformsuaracalon('0', $xAwal);
    }

    function createformsuaracalon($xidx, $xAwal = 0, $xSearch = '') {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('modelgetmenu');
        $xAddJs = link_tag('resource/admin/vendor/toaster/toastr.css') . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/toaster/toastr.min.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/js/common/fileupload/jquery.ui.widget.js"></script>' . "\n" .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxadmin.js"></script>' .
                '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/ajax/ajaxsuaracalon.js"></script>';
        echo $this->modelgetmenu->SetViewAdmin($this->setDetailFormsuaracalon($xidx), '', '', $xAddJs, '', 'suaracalon');
    }

    function setDetailFormsuaracalon($xidx) {
        $this->load->helper('form');
        $xBufResult = '';
        $xBufResult = '<div id="stylized" class="myform">' . form_open_multipart('ctrsuaracalon/inserttable', array('id' => 'form', 'name' => 'form'));
        $this->load->helper('common');
        $this->load->model('modeldapil');
        $this->load->model('modeltps');
        $this->load->model('modelpartai');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkabupaten');
        $this->load->model('modelprovinsi');
        $this->load->model('modelkecamatan');
        $this->load->model('modelkelurahan');
        $this->load->model('modeltahun');

        $xBufResult .= '<input type="hidden" name="edidx" id="edidx" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidcalon" id="edidcalon" value="0" />';
        $xBufResult .= '<input type="hidden" name="edidanggota" id="edidanggota" value="0" />';

        $xBufResult .= setForm('idcalon', 'idcalon', form_input_(getArrayObj('ednamacalon', '', '200'), '', ' placeholder="idcalon" ')) . '<div class="spacer"></div>';


        $xBufResult .= setForm('jumlahsuara', 'jumlahsuara', form_input_(getArrayObj('edjumlahsuara', '', '200'), '', ' placeholder="jumlahsuara" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('edidprovinsi', 'Provinsi', form_dropdown_('edidprovinsi', $this->modelprovinsi->getArraylistprovinsi(), '', ' class="form-control" id="edidprovinsi" onchange="provinsi();" required="required"'), '');
        $kabupaten = setForm('edkabupaten', 'kabupaten', form_dropdown_('edidkabupaten', $this->modelkabupaten->getArraylistkabupaten(), '', 'id="edidkabupaten" onchange="kabupaten()"')) . '<div class="spacer"></div>';
        $kecamatan = setForm('edkecamatan', 'kecamatan', form_dropdown_('edidkecamatan', $this->modelkecamatan->getArraylistkecamatan(), '', 'id="edidkecamatan" onchange="kelurahan()"')) . '<div class="spacer"></div>';
        $kelurahan = setForm('edidkelurahan', 'kelurahan', form_dropdown_('edidkelurahan', $this->modelkelurahan->getArraylistkelurahan(), '', 'id="edidkelurahan" ')) . '<div class="spacer"></div>';
        $xBufResult .= '<div id="kabupaten">' . $kabupaten . '</div>';
        $xBufResult .= '<div id="kecamatan">' . $kecamatan . '</div>';
        $xBufResult .= '<div id="kelurahan">' . $kelurahan . '</div>';

        $xBufResult .= setForm('tanggal', 'tanggal', form_input_(getArrayObj('edtanggal', '', '200'), '', ' class="form-control tanggal" placeholder="tanggal" ')) . '<div class="spacer"></div>';


        $xBufResult .= setForm('idpartai', 'idpartai', form_dropdown_('edidpartai', $this->modelpartai->getArrayListpartai(), '', ' id="edidpartai" placeholder="idpartai" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('iddapil', 'iddapil', form_dropdown_('ediddapil', $this->modeldapil->getArrayListdapil(), '', ' id="ediddapil" placeholder="iddapil" ')) . '<div class="spacer"></div>';

        $xBufResult .= setForm('idtps', 'idtps', form_dropdown_('edidtps', $this->modeltps->getArrayListtps(), '', ' id="edidtps" placeholder="idtps" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('keterangan', 'keterangan', form_textarea(getArrayObj('edketerangan', '', '400'), '', ' placeholder="keterangan" ')) . '<div class="spacer"></div>';
        $xBufResult .= setForm('urut', 'urut', form_input_(getArrayObj('edurut', '', '200'), '', ' placeholder="urut" ')) . '<div class="spacer"></div>';

//        $xBufResult .= setForm('idtahun', 'idtahun', form_dropdown_('edidtahun', $this->modeltahun->getArrayListtahun(), '', ' id="edidtahun" placeholder="idtahun" ')) . '<div class="spacer"></div>';
//        $xBufResult .= setForm('idanggota', 'idanggota', form_input_(getArrayObj('edidanggota', '', '200'), '', ' placeholder="idanggota" ')) . '<div class="spacer"></div>';

        $xBufResult .= '<div class="garis"></div>' . form_button('btSimpan', 'simpan', 'onclick="dosimpansuaracalon();"') . form_button('btNew', 'new', 'onclick="doClearsuaracalon();"') . '<div class="spacer"></div><div id="tabledatasuaracalon">' . $this->getlistsuaracalon(0, '') . '</div><div class="spacer"></div>';
        return $xBufResult;
    }

    function getlistsuaracalon($xAwal, $xSearch) {
        $xLimit = $this->session->userdata('limit');
        $this->load->helper('form');
        $this->load->helper('common');
        $xbufResult1 = tbaddrow(tbaddcellhead('idx', '', 'data-field="idx" data-sortable="true" width=10%') .
                tbaddcellhead('idcalon', '', 'data-field="idcalon" data-sortable="true" width=10%') .
                tbaddcellhead('urut', '', 'data-field="urut" data-sortable="true" width=10%') .
                tbaddcellhead('jumlahsuara', '', 'data-field="jumlahsuara" data-sortable="true" width=10%') .
                tbaddcellhead('idtps', '', 'data-field="idtps" data-sortable="true" width=10%') .
                tbaddcellhead('iddapil', '', 'data-field="iddapil" data-sortable="true" width=10%') .
                tbaddcellhead('idpartai', '', 'data-field="idpartai" data-sortable="true" width=10%') .
                tbaddcellhead('idkelurahan', '', 'data-field="idkelurahan" data-sortable="true" width=10%') .
                tbaddcellhead('idkabupaten', '', 'data-field="idkabupaten" data-sortable="true" width=10%') .
                tbaddcellhead('idprovinsi', '', 'data-field="idprovinsi" data-sortable="true" width=10%') .
                tbaddcellhead('tanggal', '', 'data-field="tanggal" data-sortable="true" width=10%') .
                tbaddcellhead('idtahun', '', 'data-field="idtahun" data-sortable="true" width=10%') .
                tbaddcellhead('idanggota', '', 'data-field="idanggota" data-sortable="true" width=10%') .
                tbaddcellhead('Action', 'padding:5px;width:10%;text-align:center;', 'col-md-2'), '', TRUE);
        $this->load->model('modelsuaracalon');
        $xQuery = $this->modelsuaracalon->getListsuaracalon($xAwal, $xLimit, $xSearch);
        $xbufResult = '<thead>' . $xbufResult1 . '</thead>';
        $xbufResult .= '<tbody>';
        foreach ($xQuery->result() as $row) {
            $xButtonEdit = '<i class="fas fa-edit btn" aria-hidden="true"  onclick = "doeditsuaracalon(\'' . $row->idx . '\');" ></i>';
            $xButtonHapus = '<i class="fa fa-trash btn" aria-hidden="true" onclick = "dohapussuaracalon(\'' . $row->idx . '\');"></i>';
            $xbufResult .= tbaddrow(tbaddcell($row->idx) .
                    tbaddcell($row->idcalon) .
                    tbaddcell($row->urut) .
                    tbaddcell($row->jumlahsuara) .
                    tbaddcell($row->idtps) .
                    tbaddcell($row->iddapil) .
                    tbaddcell($row->idpartai) .
                    tbaddcell($row->idkelurahan) .
                    tbaddcell($row->idkabupaten) .
                    tbaddcell($row->idprovinsi) .
                    tbaddcell($row->tanggal) .
                    tbaddcell($row->idtahun) .
                    tbaddcell($row->idanggota) .
                    tbaddcell($xButtonEdit . $xButtonHapus));
        }
        $xInput = form_input_(getArrayObj('edSearch', '', ' '));
        $xButtonSearch = '<span class="input-group-btn">
                                                <button class="btn btn-default" type="button" onclick = "dosearchsuaracalon(0);"><i class="fa fa-search"></i>
                                                </button>
                                            </span>';
        $xButtonPrev = '<img src="' . base_url() . 'resource/imgbtn/b_prevpage.png" style="border:none;width:20px;" onclick = "dosearchsuaracalon(' . ($xAwal - $xLimit) . ');"/>';
        $xButtonhalaman = '<button id="edHalaman" class="btn btn-default" disabled>' . $xAwal . ' to ' . $xLimit . '</button>';
        $xButtonNext = '<img src="' . base_url() . 'resource/imgbtn/b_nextpage.png" style="border:none;width:20px;" onclick = "dosearchsuaracalon(' . ($xAwal + $xLimit) . ');" />';
        $xbuffoottable = '<div class="foottable"><div class="col-md-6">' . setForm('', '', $xInput . $xButtonSearch, '', '') . '</div>' .
                '<div class="col-md-6">' . $xButtonPrev . $xButtonhalaman . $xButtonNext . '</div></div>';

        $xbufResult = tablegrid($xbufResult . '</tbody>', '', 'id="table" data-toggle="table" data-url="" data-show-columns="true" data-show-refresh="true" data-show-toggle="true" data-query-params="queryParams" data-pagination="true"') . $xbuffoottable;
        $xbufResult .= '<script language="javascript" type="text/javascript" src="' . base_url() . 'resource/admin/vendor/bootstrap-table/bootstrap-table.js"></script>';

        return '<div class="tabledata table-responsive"  style="width:100%;left:-12px;">' . $xbufResult . '</div>' .
                '<div id="showmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                    <div   class="modal-content">
                    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="dialogtitle">Title Dialog</h4>
      </div>
      <div id="dialogdata" class="modal-body">Dialog Data</div></div></div></div>';
    }

    function getlistsuaracalonAndroid() {
        $this->load->helper('json');
        $xSearch = $_POST['search'];
        $xAwal = $_POST['start'];
        $xLimit = $_POST['limit'];
        $this->load->helper('form');
        $this->load->helper('common');
        $this->json_data['idx'] = "";
        $this->json_data['idcalon'] = "";
        $this->json_data['urut'] = "";
        $this->json_data['jumlahsuara'] = "";
        $this->json_data['idtps'] = "";
        $this->json_data['iddapil'] = "";
        $this->json_data['idpartai'] = "";
        $this->json_data['idkelurahan'] = "";
        $this->json_data['idkabupaten'] = "";
        $this->json_data['idprovinsi'] = "";
        $this->json_data['tanggal'] = "";
        $this->json_data['idtahun'] = "";
        $this->json_data['idanggota'] = "";

        $response = array();
        $this->load->model('modelsuaracalon');
        $xQuery = $this->modelsuaracalon->getListsuaracalon($xAwal, $xLimit, $xSearch);
        foreach ($xQuery->result() as $row) {
            $this->json_data['idx'] = $row->idx;
            $this->json_data['idcalon'] = $row->idcalon;
            $this->json_data['urut'] = $row->urut;
            $this->json_data['jumlahsuara'] = $row->jumlahsuara;
            $this->json_data['idtps'] = $row->idtps;
            $this->json_data['iddapil'] = $row->iddapil;
            $this->json_data['idpartai'] = $row->idpartai;
            $this->json_data['idkelurahan'] = $row->idkelurahan;
            $this->json_data['idkabupaten'] = $row->idkabupaten;
            $this->json_data['idprovinsi'] = $row->idprovinsi;
            $this->json_data['tanggal'] = $row->tanggal;
            $this->json_data['idtahun'] = $row->idtahun;
            $this->json_data['idanggota'] = $row->idanggota;

            array_push($response, $this->json_data);
        }
        if (empty($response)) {
            array_push($response, $this->json_data);
        }
        echo json_encode($response);
    }

    function simpansuaracalonAndroid() {
        $xidx = $_POST['edidx'];
        $xidcalon = $_POST['edidcalon'];
        $xurut = $_POST['edurut'];
        $xjumlahsuara = $_POST['edjumlahsuara'];
        $xidtps = $_POST['edidtps'];
        $xiddapil = $_POST['ediddapil'];
        $xidpartai = $_POST['edidpartai'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xtanggal = $_POST['edtanggal'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->helper('json');
        $this->load->model('modelsuaracalon');
        $response = array();
        if ($xidx != '0') {
            $this->modelsuaracalon->setUpdatesuaracalon($xidx, $xidcalon, $xurut, $xjumlahsuara, $xidtps, $xiddapil, $xidpartai, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xtanggal, $xidtahun, $xidanggota);
        } else {
            $this->modelsuaracalon->setInsertsuaracalon($xidx, $xidcalon, $xurut, $xjumlahsuara, $xidtps, $xiddapil, $xidpartai, $xidkelurahan, $xidkabupaten, $xidprovinsi, $xtanggal, $xidtahun, $xidanggota);
        }
        $row = $this->modelsuaracalon->getLastIndexsuaracalon();
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idcalon'] = $row->idcalon;
        $this->json_data['urut'] = $row->urut;
        $this->json_data['jumlahsuara'] = $row->jumlahsuara;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['tanggal'] = $row->tanggal;
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        $response = array();
        array_push($response, $this->json_data);

        echo json_encode($response);
    }

    function editrecsuaracalon() {
        $xIdEdit = $_POST['edidx'];
        $this->load->model('modelsuaracalon');
        $row = $this->modelsuaracalon->getDetailsuaracalon($xIdEdit);
        $this->load->helper('json');
        $this->load->helper('common');
        $this->json_data['idx'] = $row->idx;
        $this->json_data['idcalon'] = $row->idcalon;
        $this->json_data['urut'] = $row->urut;
        $this->json_data['jumlahsuara'] = $row->jumlahsuara;
        $this->json_data['idtps'] = $row->idtps;
        $this->json_data['iddapil'] = $row->iddapil;
        $this->json_data['idpartai'] = $row->idpartai;
        $this->json_data['idkelurahan'] = $row->idkelurahan;
        $this->json_data['idkabupaten'] = $row->idkabupaten;
        $this->json_data['idprovinsi'] = $row->idprovinsi;
        $this->json_data['tanggal'] = mysqltodate($row->tanggal);
        $this->json_data['idtahun'] = $row->idtahun;
        $this->json_data['idanggota'] = $row->idanggota;

        echo json_encode($this->json_data);
    }

    function deletetablesuaracalon() {
        $edidx = $_POST['edidx'];
        $this->load->model('modelsuaracalon');
        $this->modelsuaracalon->setDeletesuaracalon($edidx);
        $this->load->helper('json');
        echo json_encode(null);
    }

    function searchsuaracalon() {
        $xAwal = $_POST['xAwal'];
        $xSearch = $_POST['xSearch'];
        $this->load->helper('json');
        $xhalaman = @ceil($xAwal / ($xAwal - $this->session->userdata('awal', $xAwal)));
        $xlimit = $this->session->userdata('limit');
        $xHal = 1;
        if ($xAwal <= 0) {
            $xHal = 1;
        } else {
            $xHal = ($xhalaman + 1);
        }
        if ($xhalaman < 0) {
            $xHal = (($xhalaman - 1) * -1);
        }
        if (($xAwal + 0) == -99) {
            $xAwal = $this->session->userdata('awal', $xAwal);
            $xHal = $this->session->userdata('halaman', $xHal);
        }
        if ($xAwal + 0 <= -1) {
            $xAwal = 0;
            $this->session->set_userdata('awal', $xAwal);
        } else {
            $this->session->set_userdata('awal', $xAwal);
        }
        $this->json_data['tabledatasuaracalon'] = $this->getlistsuaracalon($xAwal, $xSearch);
        $this->json_data['halaman'] = $xAwal . ' to ' . ($xlimit * $xHal);
        echo json_encode($this->json_data);
    }

    function simpansuaracalon() {
        $this->load->helper('json');
        $this->load->helper('common');
        if (!empty($_POST['edidx'])) {
            $xidx = $_POST['edidx'];
        } else {
            $xidx = '0';
        }
        $xidcalon = $_POST['edidcalon'];
        $xurut = $_POST['edurut'];
        $xjumlahsuara = $_POST['edjumlahsuara'];
        $xidtps = $_POST['edidtps'];
        $xiddapil = $_POST['ediddapil'];
        $xidpartai = $_POST['edidpartai'];
        $xidkelurahan = $_POST['edidkelurahan'];
        $xidkabupaten = $_POST['edidkabupaten'];
        $xidprovinsi = $_POST['edidprovinsi'];
        $xtanggal = $_POST['edtanggal'];
        $xidtahun = $_POST['edidtahun'];
        $xidanggota = $_POST['edidanggota'];

        $this->load->model('modelsuaracalon');
        $xidpegawai = $this->session->userdata('idpegawai');
        if (!empty($xidpegawai)) {
            if ($xidx != '0') {
                $xStr = $this->modelsuaracalon->setUpdatesuaracalon($xidx, $xidcalon, $xurut, $xjumlahsuara, $xidtps, $xiddapil, $xidpartai, $xidkelurahan, $xidkabupaten, $xidprovinsi, datetomysql($xtanggal), $xidtahun, $xidanggota);
            } else {
                $xStr = $this->modelsuaracalon->setInsertsuaracalon($xidx, $xidcalon, $xurut, $xjumlahsuara, $xidtps, $xiddapil, $xidpartai, $xidkelurahan, $xidkabupaten, $xidprovinsi, datetomysql($xtanggal), $xidtahun, $xidanggota);
            }
        }
        echo json_encode(null);
    }

}

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.31-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table pemilu.anggota
CREATE TABLE IF NOT EXISTS `anggota` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `noktp` bit(50) DEFAULT NULL,
  `namalengkap` varchar(250) DEFAULT NULL,
  `jeniskelamin` enum('L','P') DEFAULT NULL,
  `idpendidikan` int(11) DEFAULT NULL,
  `idpekerjaan` int(11) DEFAULT NULL,
  `idjabatan` int(1) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `notelpon` int(15) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `iddapil` int(3) DEFAULT NULL,
  `idtps` int(3) DEFAULT NULL,
  `idpartai` int(3) DEFAULT NULL,
  `idtahun` int(3) DEFAULT NULL,
  `isaktif` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.calon
CREATE TABLE IF NOT EXISTS `calon` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(250) DEFAULT NULL,
  `nourut` int(5) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `idpendidikan` int(11) DEFAULT NULL,
  `idpekerjaan` int(11) DEFAULT NULL,
  `idjabatan` int(1) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `iddapil` int(11) DEFAULT NULL,
  `idpartai` int(3) DEFAULT NULL,
  `keterangan` text,
  `idtahun` int(1) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.content
CREATE TABLE IF NOT EXISTS `content` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `isiawal` text,
  `isi` text,
  `idbahasa` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  `idkomponen` int(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `idadmin` int(11) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `image1` text,
  `image2` text,
  `image3` text,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.dapil
CREATE TABLE IF NOT EXISTS `dapil` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `dapil` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.imgslide
CREATE TABLE IF NOT EXISTS `imgslide` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `url` text,
  `keterangan` text,
  `tglinsert` datetime DEFAULT NULL,
  `tglupdate` datetime DEFAULT NULL,
  `idpegawai` int(10) DEFAULT NULL,
  `link` text,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.jabatan
CREATE TABLE IF NOT EXISTS `jabatan` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(250) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.jenissuara
CREATE TABLE IF NOT EXISTS `jenissuara` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `jenissuara` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.kabupaten
CREATE TABLE IF NOT EXISTS `kabupaten` (
  `idx` int(11) NOT NULL,
  `kode_kabupaten` varchar(4) DEFAULT NULL,
  `kabupaten` varchar(50) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.kecamatan
CREATE TABLE IF NOT EXISTS `kecamatan` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kecamatan` varchar(10) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=927113 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.kelurahan
CREATE TABLE IF NOT EXISTS `kelurahan` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kelurahan` varchar(10) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=82506 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.komponen
CREATE TABLE IF NOT EXISTS `komponen` (
  `idkomponen` int(10) NOT NULL AUTO_INCREMENT COMMENT 'header,menu,iklan,isi,keterangan filed form',
  `NmKomponen` varchar(40) DEFAULT NULL,
  `isshow` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idkomponen`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.logdelrecord
CREATE TABLE IF NOT EXISTS `logdelrecord` (
  `idx` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `idxhapus` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `nmtable` varchar(100) DEFAULT NULL,
  `tgllog` datetime DEFAULT NULL,
  `ideksekusi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=441 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `idmenu` int(10) NOT NULL AUTO_INCREMENT,
  `nmmenu` varchar(100) DEFAULT NULL,
  `tipemenu` int(1) DEFAULT NULL COMMENT '1 : page singgle 2: Page list',
  `idkomponen` int(10) DEFAULT NULL,
  `iduser` int(10) DEFAULT '0',
  `parentmenu` int(10) DEFAULT NULL,
  `urlci` varchar(100) DEFAULT NULL,
  `urut` int(10) DEFAULT NULL,
  `jmlgambar` int(1) DEFAULT '0',
  `settingform` text,
  `idaplikasi` int(10) DEFAULT NULL,
  `isumum` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`idmenu`)
) ENGINE=MyISAM AUTO_INCREMENT=411 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.partai
CREATE TABLE IF NOT EXISTS `partai` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `partai` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.pekerjaan
CREATE TABLE IF NOT EXISTS `pekerjaan` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(100) NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.pemilih
CREATE TABLE IF NOT EXISTS `pemilih` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `nokk` bit(50) DEFAULT NULL,
  `nik` bit(50) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `perkawinan` enum('B','S','P') DEFAULT NULL,
  `jeniskelamin` enum('L','P') DEFAULT NULL,
  `idpendidikan` int(11) DEFAULT NULL,
  `idpekerjaan` int(11) DEFAULT NULL,
  `idjabatan` int(1) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `rt` int(3) DEFAULT NULL,
  `rw` int(3) DEFAULT NULL,
  `disabilitas` enum('Y','N') DEFAULT NULL,
  `keterangan` text,
  `iddapil` int(11) DEFAULT NULL,
  `idtps` int(11) DEFAULT NULL,
  `idtahun` int(11) DEFAULT NULL,
  `idanggota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.pendidikan
CREATE TABLE IF NOT EXISTS `pendidikan` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `Kd_jenjang` char(1) NOT NULL DEFAULT '',
  `Nm_jenjang` varchar(5) DEFAULT NULL,
  `jenjang_ing` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Kd_jenjang`),
  UNIQUE KEY `idx` (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.provinsi
CREATE TABLE IF NOT EXISTS `provinsi` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `kode_provinsi` varchar(5) DEFAULT NULL,
  `provinsi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  UNIQUE KEY `id_provinsi` (`kode_provinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(200) DEFAULT NULL,
  `Alamat` varchar(250) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `portemail` int(11) DEFAULT NULL,
  `Telpon` varchar(20) DEFAULT NULL,
  `Whatsapp` int(20) DEFAULT NULL,
  `idkecamatan` int(20) DEFAULT NULL,
  `Bio` text,
  `logo` varchar(200) DEFAULT NULL,
  `fb` varchar(200) DEFAULT NULL,
  `ig` varchar(200) DEFAULT NULL,
  `tw` varchar(200) DEFAULT NULL,
  `G` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.suaracalon
CREATE TABLE IF NOT EXISTS `suaracalon` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `idcalon` int(11) DEFAULT NULL,
  `urut` int(11) DEFAULT NULL,
  `jumlahsuara` float DEFAULT NULL,
  `idtps` int(11) DEFAULT NULL,
  `iddapil` int(11) DEFAULT NULL,
  `idpartai` int(11) DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `idtahun` int(1) DEFAULT NULL,
  `idanggota` int(5) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.suaratotal
CREATE TABLE IF NOT EXISTS `suaratotal` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `jumlahsuara` int(11) DEFAULT NULL,
  `idjenissuara` int(1) DEFAULT NULL,
  `idtps` int(11) DEFAULT NULL,
  `keterangan` text,
  `idtahun` text,
  `idanggota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.tahun
CREATE TABLE IF NOT EXISTS `tahun` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.tipemenu
CREATE TABLE IF NOT EXISTS `tipemenu` (
  `idTipeMenu` int(10) NOT NULL AUTO_INCREMENT,
  `NmTipeMenu` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idTipeMenu`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.tps
CREATE TABLE IF NOT EXISTS `tps` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `tps` varchar(150) DEFAULT NULL,
  `urut` int(2) DEFAULT NULL,
  `idketua` int(11) DEFAULT '0',
  `keterangan` text,
  `idkecamatan` int(11) DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `iddapil` int(11) DEFAULT NULL,
  `idtahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.usergroup
CREATE TABLE IF NOT EXISTS `usergroup` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `NmUserGroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.usermenu
CREATE TABLE IF NOT EXISTS `usermenu` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `iduser` int(10) DEFAULT NULL,
  `idmenu` int(10) DEFAULT NULL,
  `idaplikasi` int(10) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=8566 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- Pengeluaran data tidak dipilih.
-- membuang struktur untuk table pemilu.usersistem
CREATE TABLE IF NOT EXISTS `usersistem` (
  `idx` int(10) NOT NULL AUTO_INCREMENT,
  `npp` varchar(20) DEFAULT NULL,
  `Nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL COMMENT 'Organisasi',
  `NoTelpon` varchar(50) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `statuspeg` int(10) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ym` varchar(100) DEFAULT NULL,
  `isaktif` enum('Y','N') DEFAULT 'N',
  `idusergroup` int(10) DEFAULT NULL,
  `idkabupaten` int(10) DEFAULT NULL,
  `idpropinsi` int(10) DEFAULT NULL,
  `imehp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Pengeluaran data tidak dipilih.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
